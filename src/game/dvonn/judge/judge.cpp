/*
 * @file game/dvonn/judge/judge.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <game/dvonn/judge/judge.hpp>
#include <iostream>

using namespace openxum::common;

namespace openxum { namespace dvonn {

void Judge::play(Player& first_player, Player& second_player)
{
    Board::Phase previousPhase = mBoard.phase();

    if (mFirstPlaying) {
        if (mBoard.phase() == Board::PUT_DVONN_PIECE) {
            put_dvonn_piece(first_player, second_player);
        } else if (mBoard.phase() == Board::PUT_PIECE) {
            put_piece(first_player, second_player);
        } else if (mBoard.phase() == Board::MOVE_STACK) {
            move_stack(first_player, second_player);
        }
    } else {
        if (mBoard.phase() == Board::PUT_DVONN_PIECE) {
            put_dvonn_piece(second_player, first_player);
        } else if (mBoard.phase() == Board::PUT_PIECE) {
            put_piece(second_player, first_player);
        } else if (mBoard.phase() == Board::MOVE_STACK) {
            move_stack(second_player, first_player);
        }
    }
    if (not(previousPhase == Board::PUT_PIECE and
            mBoard.phase() == Board::MOVE_STACK)) {
        mFirstPlaying = not mFirstPlaying;
    }

    if (mBoard.phase() == Board::MOVE_STACK) {
        remove_isolated_stacks(first_player, second_player);
    }
}

void Judge::move_stack(Player& current_player, Player& other_player)
{
    Color color = current_player.color();
    Coordinates origin, destination;

    if (current_player.move_stack(origin, destination)) {

        std::cerr << "move stack by " << (color == BLACK ? "black" : "white")
                  << " from " << origin.letter() << origin.number()
                  << " to " << destination.letter() << destination.number()
                  << std::endl;

        other_player.move_oponent_stack(origin, destination);
        mBoard.move_stack(origin, destination);
    } else {
        other_player.move_no_stack();
        mBoard.move_no_stack();

        std::cerr << "PASS " << (color == BLACK ? "black" : "white")
                  << std::endl;
    }
}

void Judge::put_dvonn_piece(Player& current_player, Player& other_player)
{
    Coordinates coordinates = current_player.put_dvonn_piece();

    std::cerr << "put dvonn piece at " << coordinates.letter()
              << coordinates.number() << std::endl;

    other_player.put_dvonn_piece(coordinates);
    mBoard.put_dvonn_piece(coordinates);
}

void Judge::put_piece(Player& current_player, Player& other_player)
{
    Coordinates coordinates = current_player.put_piece();
    Color color = current_player.color();

    std::cerr << "put " << (color == BLACK ? "black" : "white")
              << " piece at " << coordinates.letter()
              << coordinates.number() << std::endl;

    other_player.put_piece(coordinates, color);
    mBoard.put_piece(coordinates, color);
}

void Judge::remove_isolated_stacks(Player& a_player, Player& another_player)
{
    CoordinatesList list = a_player.remove_isolated_stacks();

    if (not list.empty()) {
        another_player.remove_isolated_stacks(list);
        mBoard.remove_stacks(list);
    }
}

}} // namespace openxum dvonn
