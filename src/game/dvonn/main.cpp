/*
 * @file game/dvonn/main.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <stdlib.h>
#include <game/dvonn/judge/judge.hpp>
#include <game/dvonn/players/computer.hpp>

using namespace openxum::common;
using namespace openxum::dvonn;
using namespace openxum;

int main(int argc, char* argv[])
{

    srand(872164);

    Judge judge;
    Player* player_black = new Computer(judge.current_color(),
                                      judge.current_color());
    Player* player_white = new Computer(judge.current_color(),
                                      judge.next_color());

    std::cout << "First player is: "
              << (player_black->color() == BLACK ? "black" : "white" )
              << std::endl;
    std::cout << "Second player is: "
              << (player_white->color() == BLACK ? "black" : "white" )
              << std::endl;

    std::cout << judge.display_board() << std::endl;
    std::cout << "Board is initialized !" << std::endl;

    unsigned int i = 1;

    while (not judge.is_finished()) {

        std::cout << "Turn n°" << i++ << std::endl;

        judge.play(*player_black, *player_white);

        std::cout << judge.display_board() << std::endl;
    }

    std::cout << "Winner is: "
              << (judge.winner_is() == BLACK ? "BLACK" :
                  (judge.winner_is() == WHITE ? "WHITE" : "NONE"))
              << std::endl;

    delete player_black;
    delete player_white;
    return 0;
}
