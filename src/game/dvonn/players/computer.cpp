/*
 * @file game/dvonn/players/computer.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <game/dvonn/players/computer.hpp>
#include <stdlib.h>

#include <iostream>

using namespace openxum::common;

namespace openxum { namespace dvonn {

bool Computer::move_stack(Coordinates& origin, Coordinates& destination)
{
    CoordinatesList stack_list = mBoard.get_possible_moving_stacks(mColor);

    if (not stack_list.empty()) {
        int stack_index = rand() % stack_list.size();

        origin = stack_list[stack_index];

        CoordinatesList destination_list =
            mBoard.get_stack_possible_move(origin);

        if (not destination_list.empty()) {
            int destination_index = rand() % destination_list.size();

            destination = destination_list[destination_index];
            mBoard.move_stack(origin, destination);
            return true;
        } else {
            mBoard.move_no_stack();
            return false;
        }
    } else {
        mBoard.move_no_stack();
        return false;
    }
}

Coordinates Computer::put_dvonn_piece()
{
    CoordinatesList list = mBoard.get_free_intersections();
    int index = rand() % list.size();
    Coordinates coordinates = list[index];

    mBoard.put_dvonn_piece(coordinates);
    return coordinates;
}

Coordinates Computer::put_piece()
{
    CoordinatesList list = mBoard.get_free_intersections();
    int index = rand() % list.size();
    Coordinates coordinates = list[index];

    mBoard.put_piece(coordinates, mColor);
    return coordinates;
}

CoordinatesList Computer::remove_isolated_stacks()
{
    return mBoard.remove_isolated_stacks();
}

}} // namespace openxum dvonn
