/*
 * @file game/dvonn/players/localPlayer.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <game/dvonn/players/localPlayer.hpp>

using namespace openxum::common;

namespace openxum { namespace dvonn {

void LocalPlayer::move_no_stack()
{
    mBoard.move_no_stack();
}

void LocalPlayer::move_oponent_stack(const Coordinates& origin,
                                     const Coordinates& destination)
{
    mBoard.move_stack(origin, destination);
}

void LocalPlayer::put_dvonn_piece(const Coordinates& coordinates)
{
    mBoard.put_dvonn_piece(coordinates);
}

void LocalPlayer::put_piece(const Coordinates& coordinates, Color color)
{
    mBoard.put_piece(coordinates, color);
}

void LocalPlayer::remove_isolated_stacks(CoordinatesList& list)
{
    mBoard.remove_stacks(list);
}

}} // namespace openxum dvonn
