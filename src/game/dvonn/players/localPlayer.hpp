/*
 * @file game/dvonn/players/localPlayer.hpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _DVONN_LOCAL_PLAYER_HPP
#define _DVONN_LOCAL_PLAYER_HPP 1

#include <game/dvonn/players/player.hpp>

namespace openxum { namespace dvonn {

class LocalPlayer : public Player
{
public:
    LocalPlayer(Color color, Color mycolor) :
        Player(mycolor)
    { }

    virtual ~LocalPlayer()
    { }

    Color current_player() const
    { return mBoard.current_color(); }

    std::string display_board() const
    { return mBoard.to_string(); }

    bool is_finished() const
    { return mBoard.is_finished(); }

    virtual void move_no_stack();
    virtual void move_oponent_stack(const Coordinates& origin,
                                    const Coordinates& destination);
    virtual void put_dvonn_piece(const Coordinates& coordinates);
    virtual void put_piece(const Coordinates& coordinates, Color color);
    virtual void remove_isolated_stacks(CoordinatesList& list);

    virtual bool move_stack(Coordinates& origin, Coordinates& destination) = 0;
    virtual Coordinates put_dvonn_piece() = 0;
    virtual Coordinates put_piece() = 0;
    virtual CoordinatesList remove_isolated_stacks() = 0;

protected:
    Board mBoard;
};

}} // namespace openxum dvonn

#endif
