/*
 * @file game/gipf/judge/judge.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <game/gipf/judge/judge.hpp>
#include <iostream>

using namespace openxum::common;

namespace openxum { namespace gipf {

void Judge::play(Player& first_player, Player& second_player)
{
    if (mFirstPlaying) {
        if (mBoard.phase() == Board::PUT_FIRST_PIECE) {
            put_first_piece(first_player, second_player);
        } else if (mBoard.phase() == Board::PUT_PIECE) {
            put_piece(first_player, second_player);
            push_piece(first_player, second_player);
            remove_rows(first_player, second_player);
            remove_rows(second_player, first_player);
        }
    } else {
        if (mBoard.phase() == Board::PUT_FIRST_PIECE) {
            put_first_piece(second_player, first_player);
        } else if (mBoard.phase() == Board::PUT_PIECE) {
            put_piece(second_player, first_player);
            push_piece(second_player, first_player);
            remove_rows(second_player, first_player);
            remove_rows(first_player, second_player);
        }
    }
    mFirstPlaying = not mFirstPlaying;
}

void Judge::push_piece(Player& current_player, Player& other_player)
{
    Color color = current_player.color();
    Coordinates origin = current_player.origin();
    Coordinates destination = current_player.push_piece();

    std::cerr << "push piece by " << (color == BLACK ? "black" : "white")
              << " from " << origin.letter() << origin.number()
              << " to " << destination.letter() << destination.number()
              << std::endl;

    other_player.push_piece(origin, destination);
    mBoard.push_piece(origin, destination, color);
}

void Judge::put_first_piece(Player& current_player, Player& other_player)
{
    Color color = current_player.color();
    Coordinates coordinates = current_player.put_first_piece();

    std::cerr << "put first piece by " << (color == BLACK ? "black" : "white")
              << " at " << coordinates.letter() << coordinates.number()
              << std::endl;

    other_player.put_first_piece(coordinates);
    mBoard.put_first_piece(coordinates, color, mBoard.game_type() != BASIC);
}

void Judge::put_piece(Player& current_player, Player& other_player)
{
    Color color = current_player.color();
    Coordinates coordinates = current_player.put_piece();

    std::cerr << "put piece by " << (color == BLACK ? "black" : "white")
              << " at " << coordinates.letter() << coordinates.number()
              << std::endl;

    other_player.put_piece(coordinates);
    mBoard.put_piece(coordinates, color);
}

void Judge::remove_rows(Player& current_player, Player& other_player)
{
    Color color = current_player.color();
    Rows rows = current_player.remove_rows();

    if (not rows.empty()) {
        std::cerr << "remove rows by " << (color == BLACK ? "black" : "white")
                  << ": ";

        if (rows.size() == 1) {
            std::cerr << " row: " << rows.front().to_string() << std::endl;
        } else {
            std::cerr << " rows:";
            for (Rows::const_iterator it = rows.begin(); it != rows.end();
                 ++it) {
                std::cerr << " " << it->to_string();
            }
            std::cerr << std::endl;
        }

        other_player.remove_rows(rows);
        mBoard.remove_rows(rows, color);
    } else {
        other_player.remove_no_row();
        mBoard.remove_no_row();
    }
}

}} // namespace openxum gipf
