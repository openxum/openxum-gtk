/*
 * @file game/gipf/judge/judge.hpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _GIPF_JUDGE_HPP
#define _GIPF_JUDGE_HPP 1

#include <gipf/board.hpp>
#include <game/gipf/players/player.hpp>

namespace openxum { namespace gipf {

class Judge
{
public:
    Judge(GameType type) : mBoard(type), mFirstPlaying(true)
    { }

    virtual ~Judge()
    { }

    const Board& board() const
    { return mBoard; }
    Board& board()
    { return mBoard; }
    common::Color current_color() const
    { return mBoard.current_color(); }
    common::Color next_color() const
    { return mBoard.current_color() == common::BLACK ?
            common::WHITE : common::BLACK; }
    std::string display_board() const
    { return mBoard.to_string(); }
    bool is_finished() const
    { return mBoard.is_finished(); }
    void play(Player& first_player, Player& second_player);
    common::Color winner_is() const
    { return mBoard.winner_is(); }

private:
    void push_piece(Player& current_player, Player& other_player);
    void put_first_piece(Player& current_player, Player& other_player);
    void put_piece(Player& current_player, Player& other_player);
    void remove_rows(Player& current_player, Player& other_player);

    Board mBoard;
    bool mFirstPlaying;
};

}} // namespace openxum gipf

#endif
