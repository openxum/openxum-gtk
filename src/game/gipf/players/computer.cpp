/*
 * @file game/gipf/players/computer.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <game/gipf/players/computer.hpp>
#include <stdlib.h>

using namespace openxum::common;

namespace openxum { namespace gipf {

Coordinates Computer::push_piece()
{
    CoordinatesList list = mBoard.get_possible_pushing_list(mOrigin);
    CoordinatesList::const_iterator it = list.begin();

    if (not list.empty()) {
        int index = rand() % list.size();

        mBoard.push_piece(mOrigin, list[index], mColor);
        return list[index];
    } else {
        return Coordinates('X', -1);
    }
}

Coordinates Computer::put_first_piece()
{
    CoordinatesList list = mBoard.get_possible_first_putting_list();

    if (not list.empty()) {
        int index = rand() % list.size();

        mBoard.put_first_piece(list[index], mColor,
                               mBoard.game_type() != BASIC);
        return list[index];
    } else {
        return Coordinates('X', -1);
    }
}

Coordinates Computer::put_piece()
{
    CoordinatesList list = mBoard.get_possible_putting_list();

    if (not list.empty()) {
        int index = rand() % list.size();

        mOrigin = list[index];
        mBoard.put_piece(mOrigin, mColor);
    } else {
        mOrigin = Coordinates('X', -1);
    }
    return mOrigin;
}

Rows Computer::remove_rows()
{
    Rows rows;
    SeparatedRows srows = mBoard.get_rows(mColor);

    if (not srows.empty()) {
        if (srows.size() == 1) {
            rows.push_back(remove_row(srows.front()));
        } else {
            SeparatedRows::const_iterator it = srows.begin();

            while (it != srows.end()) {
                rows.push_back(remove_row(*it));
                ++it;
            }
        }
        mBoard.remove_rows(rows, mColor);
    }
    return rows;
}

Row Computer::remove_row(const Rows& rows)
{
    Row selectedRow;

    if (rows.size() == 1) {
        selectedRow = rows.front();
    } else {
        selectedRow = rows[rand() % rows.size()];
    }
    return selectedRow;
}

}} // namespace openxum gipf
