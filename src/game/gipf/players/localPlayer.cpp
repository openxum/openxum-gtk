/*
 * @file game/gipf/players/localPlayer.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <game/gipf/players/localPlayer.hpp>

using namespace openxum::common;

namespace openxum { namespace gipf {

void LocalPlayer::push_piece(const Coordinates& origin,
                             const Coordinates& destination)
{
    mBoard.push_piece(origin, destination, mColor == BLACK ? WHITE : BLACK);
}

void LocalPlayer::put_first_piece(const Coordinates& coordinates)
{
    mBoard.put_first_piece(coordinates, mColor == BLACK ? WHITE : BLACK,
                           mBoard.game_type() != BASIC);
}

void LocalPlayer::put_piece(const Coordinates& coordinates)
{
    mBoard.put_piece(coordinates, mColor == BLACK ? WHITE : BLACK);
}

void LocalPlayer::remove_no_row()
{
}

void LocalPlayer::remove_rows(const Rows& rows)
{
    mBoard.remove_rows(rows, mColor == BLACK ? WHITE : BLACK);
}

}} // namespace openxum gipf
