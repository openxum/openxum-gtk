/*
 * @file game/gipf/players/localPlayer.hpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _GIPF_LOCAL_PLAYER_HPP
#define _GIPF_LOCAL_PLAYER_HPP 1

#include <game/gipf/players/player.hpp>

namespace openxum { namespace gipf {

class LocalPlayer : public Player
{
public:
    LocalPlayer(GameType type, common::Color mycolor) :
        Player(mycolor), mBoard(type)
    { }

    virtual ~LocalPlayer()
    { }

    common::Color current_color() const
    { return mBoard.current_color(); }

    std::string display_board() const
    { return mBoard.to_string(); }

    bool is_finished() const
    { return mBoard.is_finished(); }

    virtual void push_piece(const Coordinates& origin,
                            const Coordinates& destination);
    virtual void put_first_piece(const Coordinates& coordinates);
    virtual void put_piece(const Coordinates& coordinates);
    virtual void remove_no_row();
    virtual void remove_rows(const Rows& rows);

    virtual Coordinates push_piece() = 0;
    virtual Coordinates put_first_piece() = 0;
    virtual Coordinates put_piece() = 0;
    virtual Rows remove_rows() = 0;

protected:
    Board mBoard;
};

}} // namespace openxum gipf

#endif
