/*
 * @file game/gipf/players/player.hpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _GIPF_PLAYER_HPP
#define _GIPF_PLAYER_HPP 1

#include <gipf/coordinates.hpp>
#include <gipf/board.hpp>

namespace openxum { namespace gipf {

class Player
{
public:
    Player(common::Color mycolor) : mColor(mycolor), mOrigin('X', -1)
    { }

    virtual ~Player()
    { }

    common::Color color() const
    { return mColor; }

    const Coordinates& origin() const
    { return mOrigin; }

    // abstract methods
    virtual void push_piece(const Coordinates& origin,
                            const Coordinates& destination) = 0;
    virtual void put_first_piece(const Coordinates& coordinates) = 0;
    virtual void put_piece(const Coordinates& coordinates) = 0;
    virtual void remove_no_row() = 0;
    virtual void remove_rows(const Rows& rows) = 0;

    virtual Coordinates push_piece() = 0;
    virtual Coordinates put_first_piece() = 0;
    virtual Coordinates put_piece() = 0;
    virtual Rows remove_rows() = 0;

protected:
    void set_color(common::Color color)
    { mColor = color; }

    common::Color mColor;
    Coordinates mOrigin;
};

}} // namespace openxum gipf

#endif
