/*
 * @file game/invers/judge/judge.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <game/invers/judge/judge.hpp>
#include <iostream>

using namespace openxum::common;

namespace openxum { namespace invers {

void Judge::play(Player& first_player, Player& second_player)
{
    if (mFirstPlaying) {

        std::cout << "Player "
                  << (first_player.color() == RED ? "red" : "yellow")
                  << std::endl;

        put_tile(first_player, second_player);
    } else {

        std::cout << "Player "
                  << (second_player.color() == RED ? "red" : "yellow")
                  << std::endl;

        put_tile(second_player, first_player);
    }
    mFirstPlaying = not mFirstPlaying;
}

void Judge::put_tile(Player& first_player, Player& second_player)
{
    Color current_player = first_player.color();
    Position position;
    Color color;

    Coordinates coordinates = first_player.put_tile(position, color);

    if (coordinates.letter() != 'X' or coordinates.number() != 0) {

        std::cout << "put "
                  << ((color == RED) ? "red":"yellow")
                  << " tile at ";
        if (position == TOP or position == BOTTOM) {
            std::cout << coordinates.letter() << " by "
                      << ((position == TOP) ? "top" : "bottom");
        } else {
            std::cout << coordinates.number() << " by "
                      << ((position == LEFT) ? "left" : "right");
        }
        std::cout << std::endl;

        second_player.put_tile(coordinates, position, color, current_player);
        mBoard.put_tile(coordinates, position, color, current_player);
    } else {
        std::cout << "PASS" << std::endl;
    }
}

}} // namespace openxum invers
