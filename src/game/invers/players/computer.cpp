/*
 * @file game/invers/players/computer.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <game/invers/players/computer.hpp>
#include <stdlib.h>

using namespace openxum::common;

namespace openxum { namespace invers {

Coordinates Computer::put_tile(Position& position, Color& color)
{
    Coordinates coordinates;

    if (mColor == RED) {
        if (mBoard.red_tile_number() > 0) {
            color = RED;
        } else {
            color = YELLOW;
        }
    } else {
        if (mBoard.yellow_tile_number() > 0) {
            color = YELLOW;
        } else {
            color = RED;
        }
    }

    CoordinatesList top;
    CoordinatesList bottom;
    CoordinatesList left;
    CoordinatesList right;

    mBoard.get_possible_position(top, bottom, left, right, mColor);

    int size = top.size() + bottom.size() + left.size() + right.size();

    if (size > 0) {
        int index = rand() % size;

        if (index < top.size()) {
            position = TOP;
            coordinates = top[index];
        } else if (index < top.size() + bottom.size()) {
            position = BOTTOM;
            coordinates = bottom[index - top.size()];
        } else if (index < top.size() + bottom.size() + left.size()) {
            position = LEFT;
            coordinates = left[index - top.size() - bottom.size()];
        } else {
            position = RIGHT;
            coordinates = right[index - top.size() - bottom.size() -
                                left.size()];
        }
        mBoard.put_tile(coordinates, position, color, mColor);
    } else {
        coordinates = Coordinates('X', 0);
    }
    return coordinates;
}

}} // namespace openxum invers
