/*
 * @file game/invers/players/localPlayer.hpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _INVERS_LOCAL_PLAYER_HPP
#define _INVERS_LOCAL_PLAYER_HPP 1

#include <game/invers/players/player.hpp>

namespace openxum { namespace invers {

class LocalPlayer : public Player
{
public:
    LocalPlayer(Type type, common::Color color, common::Color mycolor) :
        Player(mycolor), mBoard(type, color)
    { }

    virtual ~LocalPlayer()
    { }

    common::Color current_player() const
    { return mBoard.current_player(); }

    std::string display_board() const
    { return mBoard.to_string(); }

    bool is_finished() const
    { return mBoard.is_finished(); }

    virtual void put_tile(const Coordinates& coordinates, Position position,
                          common::Color color, common::Color player);

    virtual Coordinates put_tile(Position& position, common::Color& color) = 0;

protected:
    Board mBoard;
};

}} // namespace openxum invers

#endif
