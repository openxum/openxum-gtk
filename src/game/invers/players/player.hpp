/*
 * @file game/invers/players/player.hpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _INVERS_PLAYER_HPP
#define _INVERS_PLAYER_HPP 1

#include <invers/coordinates.hpp>
#include <invers/intersection.hpp>
#include <invers/board.hpp>

namespace openxum { namespace invers {

class Player
{
public:
    Player(common::Color color) : mColor(color)
    { }

    virtual ~Player()
    { }

    common::Color color() const
    { return mColor; }

    // abstract methods
    virtual void put_tile(const Coordinates& coordinates, Position position,
                          common::Color color, common::Color player) = 0;

    virtual Coordinates put_tile(Position& position, common::Color& color) = 0;

protected:
    void set_color(common::Color color)
    { mColor = color; }

    common::Color mColor;
};

}} // namespace openxum invers

#endif
