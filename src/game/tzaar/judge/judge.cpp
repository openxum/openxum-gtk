/*
 * @file game/tzaar/judge/judge.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <game/tzaar/judge/judge.hpp>
#include <iostream>

using namespace openxum::common;

namespace openxum { namespace tzaar {

void Judge::play(Player& first_player, Player& second_player)
{
    if (mFirstPlaying) {
        if (board().phase() == Board::FIRST_MOVE) {
            first_move(first_player, second_player);
        } else if (board().phase() == Board::CAPTURE) {
            capture(first_player, second_player);
            choose(first_player, second_player);
            if (board().phase() == Board::SECOND_CAPTURE) {
                capture(first_player, second_player);
            } else if (board().phase() == Board::MAKE_STRONGER) {
                make_stack(first_player, second_player);
            } else if (board().phase() == Board::PASS) {
                pass(first_player, second_player);
            }
        }
    } else {
        if (board().phase() == Board::CAPTURE) {
            capture(second_player, first_player);
            choose(second_player, first_player);
            if (board().phase() == Board::SECOND_CAPTURE) {
                capture(second_player, first_player);
            } else if (board().phase() == Board::MAKE_STRONGER) {
                make_stack(second_player, first_player);
            } else if (board().phase() == Board::PASS) {
                pass(second_player, first_player);
            }
        }
    }
    mFirstPlaying = not mFirstPlaying;
}

void Judge::capture(Player& current_player, Player& other_player)
{
    Color color = current_player.color();
    Coordinates origin, destination;

    destination = current_player.capture(origin);
    if (destination.is_valid() and origin.is_valid()) {

        std::cerr << "capture " << (color == BLACK ? "white" : "black")
                  << " stack by " << (color == BLACK ? "black" : "white")
                  << " from " << origin.letter() << origin.number()
                  << " to " << destination.letter() << destination.number()
                  << std::endl;

        other_player.capture(origin, destination);
        mBoard.capture(origin, destination);
    } else {
        other_player.no_capture();
        mBoard.no_capture();
    }
}

void Judge::choose(Player& current_player, Player& other_player)
{
    Color color = current_player.color();
    Board::Phase choice = current_player.choose();

    std::cerr << (color == BLACK ? "black" : "white")
              << " player chooses to "
              << (choice == Board::SECOND_CAPTURE ? "capture" :
                  (choice == Board::MAKE_STRONGER ? "make a stronger stack" :
                   "pass")) << std::endl;

    other_player.choose(choice);
    mBoard.choose(choice);
}

void Judge::first_move(Player& current_player, Player& other_player)
{
    Color color = current_player.color();
    Coordinates origin, destination;

    destination = current_player.first_move(origin);

    std::cerr << "move piece by " << (color == BLACK ? "black" : "white")
              << " from " << origin.letter() << origin.number()
              << " to " << destination.letter() << destination.number()
              << std::endl;

    other_player.first_move(origin, destination);
    mBoard.first_move(origin, destination);
}

void Judge::make_stack(Player& current_player, Player& other_player)
{
    Color color = current_player.color();
    Coordinates origin, destination;

    destination = current_player.make_stack(origin);
    if (destination.is_valid() and origin.is_valid()) {

        std::cerr << "make stack by " << (color == BLACK ? "black" : "white")
                  << " from " << origin.letter() << origin.number()
                  << " to " << destination.letter() << destination.number()
                  << std::endl;

        other_player.make_stack(origin, destination);
        mBoard.make_stack(origin, destination);
    } else {
        other_player.pass();
        mBoard.pass();
    }
}

void Judge::pass(Player& current_player, Player& other_player)
{
    current_player.pass();
    other_player.pass();
    mBoard.pass();
}

}} // namespace openxum tzaar
