/*
 * @file game/tzaar/judge/judge.hpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _TZAAR_JUDGE_HPP
#define _TZAAR_JUDGE_HPP 1

#include <tzaar/board.hpp>
#include <game/tzaar/players/player.hpp>

namespace openxum { namespace tzaar {

class Judge
{
public:
    Judge(GameType type) : mBoard(type, common::WHITE), mFirstPlaying(true)
    { }

    virtual ~Judge()
    { }

    const Board& board() const
    { return mBoard; }
    Board& board()
    { return mBoard; }
    common::Color current_color() const
    { return mBoard.current_color(); }
    common::Color next_color() const
    { return mBoard.current_color() == common::BLACK ?
            common::WHITE : common::BLACK; }
    std::string display_board() const
    { return mBoard.to_string(); }
    bool is_finished() const
    { return mBoard.is_finished(); }
    void play(Player& first_player, Player& second_player);
    common::Color winner_is() const
    { return mBoard.winner_is(); }

private:
    void capture(Player& current_player, Player& other_player);
    void choose(Player& current_player, Player& other_player);
    void first_move(Player& current_player, Player& other_player);
    void make_stack(Player& current_player, Player& other_player);
    void pass(Player& current_player, Player& other_player);

    Board mBoard;
    bool mFirstPlaying;
};

}} // namespace openxum tzaar

#endif
