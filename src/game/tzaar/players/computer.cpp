/*
 * @file game/tzaar/players/computer.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <game/tzaar/players/computer.hpp>
#include <algorithm>
#include <stdlib.h>

#include <iostream>

using namespace openxum::common;

namespace openxum { namespace tzaar {

Coordinates Computer::capture(Coordinates& origin)
{
    bool found = false;
    CoordinatesList list = mBoard.get_no_free_intersections(mColor);
    Coordinates destination;

    while (not found and not list.empty()) {
        int index = rand() % list.size();

        origin = list[index];
        CoordinatesList destination_list =
            mBoard.get_distant_neighbors(origin,
                                         mColor == BLACK ? WHITE : BLACK);

        if (not destination_list.empty()) {
            int destination_index = rand() % destination_list.size();

            destination = destination_list[destination_index];
            mBoard.capture(origin, destination);
            found = true;
        } else {
            CoordinatesList::iterator it = std::find(list.begin(), list.end(),
                                                     origin);
            list.erase(it);
        }
    }
    if (list.empty()) {
        return Coordinates('X', -1);
    } else {
        return destination;
    }
}

Board::Phase Computer::choose()
{
    if (mBoard.can_capture(mColor)) {
        if (mBoard.can_make_stack(mColor)) {
            int i = rand() % 3;

            if (i == 0) {
                return Board::SECOND_CAPTURE;
            } else if (i == 1) {
                return Board::MAKE_STRONGER;
            } else {
                return Board::PASS;
            }
        } else {
            int i = rand() % 2;

            if (i == 0) {
                return Board::SECOND_CAPTURE;
            } else {
                return Board::PASS;
            }
        }
    } else {
        if (mBoard.can_make_stack(mColor)) {
            int i = rand() % 2;

            if (i == 0) {
                return Board::MAKE_STRONGER;
            } else {
                return Board::PASS;
            }
        } else {
            return Board::PASS;
        }
    }
}

Coordinates Computer::first_move(Coordinates& origin)
{
    bool found = false;
    CoordinatesList list = mBoard.get_no_free_intersections(mColor);
    Coordinates destination;

    while (not found) {
        int index = rand() % list.size();

        origin = list[index];
        CoordinatesList destination_list =
            mBoard.get_neighbors(origin, mColor == BLACK ? WHITE : BLACK);

        // CoordinatesList::const_iterator it = destination_list.begin();
        // std::cout << origin.letter() << origin.number() << " -> { ";
        // while (it != destination_list.end()) {
        //     std::cout << it->letter() << it->number() << " ";
        //     ++it;
        // }
        // std::cout << "}" << std::endl;

        if (not destination_list.empty()) {
            int destination_index = rand() % destination_list.size();

            destination = destination_list[destination_index];
            mBoard.first_move(origin, destination);
            found = true;
        }
    }
    return destination;
}

Coordinates Computer::make_stack(Coordinates& origin)
{
    bool found = false;
    CoordinatesList list = mBoard.get_no_free_intersections(mColor);
    Coordinates destination;

    while (not found and not list.empty()) {
        int index = rand() % list.size();

        origin = list[index];
        CoordinatesList destination_list =
            mBoard.get_distant_neighbors(origin, mColor, false);

        // CoordinatesList::const_iterator it = destination_list.begin();
        // std::cout << origin.letter() << origin.number() << " -> { ";
        // while (it != destination_list.end()) {
        //     std::cout << it->letter() << it->number() << " ";
        //     ++it;
        // }
        // std::cout << "}" << std::endl;

        if (not destination_list.empty()) {
            int destination_index = rand() % destination_list.size();

            destination = destination_list[destination_index];
            mBoard.make_stack(origin, destination);
            found = true;
        } else {
            CoordinatesList::iterator it = std::find(list.begin(), list.end(),
                                                     origin);
            list.erase(it);
        }
    }
    if (list.empty()) {
        return Coordinates('X', -1);
    } else {
        return destination;
    }
}

}} // namespace openxum tzaar
