/*
 * @file game/tzaar/players/localPlayer.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <game/tzaar/players/localPlayer.hpp>

using namespace openxum::common;

namespace openxum { namespace tzaar {

void LocalPlayer::capture(const Coordinates& origin,
                          const Coordinates& destination)
{
    mBoard.capture(origin, destination);
}

void LocalPlayer::choose(Board::Phase choice)
{
    mBoard.choose(choice);
}

void LocalPlayer::first_move(const Coordinates& origin,
                             const Coordinates& destination)
{
    mBoard.first_move(origin, destination);
}

void LocalPlayer::make_stack(const Coordinates& origin,
                             const Coordinates& destination)
{
    mBoard.make_stack(origin, destination);
}

void LocalPlayer::no_capture()
{
    mBoard.no_capture();
}

void LocalPlayer::pass()
{
    mBoard.pass();
}

}} // namespace openxum tzaar
