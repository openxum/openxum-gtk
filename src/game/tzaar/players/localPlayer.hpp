/*
 * @file game/tzaar/players/localPlayer.hpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _TZAAR_LOCAL_PLAYER_HPP
#define _TZAAR_LOCAL_PLAYER_HPP 1

#include <game/tzaar/players/player.hpp>

namespace openxum { namespace tzaar {

class LocalPlayer : public Player
{
public:
    LocalPlayer(GameType type, common::Color color, common::Color mycolor) :
        Player(mycolor), mBoard(type, color)
    { }

    virtual ~LocalPlayer()
    { }

    common::Color current_color() const
    { return mBoard.current_color(); }

    std::string display_board() const
    { return mBoard.to_string(); }

    bool is_finished() const
    { return mBoard.is_finished(); }

    virtual void capture(const Coordinates& origin,
        const Coordinates& destination);
    virtual void choose(Board::Phase);
    virtual void first_move(const Coordinates& origin,
        const Coordinates& destination);
    virtual void make_stack(const Coordinates& origin,
        const Coordinates& destination);
    virtual void no_capture();
    virtual void pass();

    virtual Coordinates capture(Coordinates& origin) = 0;
    virtual Board::Phase choose() = 0;
    virtual Coordinates first_move(Coordinates& origin) = 0;
    virtual Coordinates make_stack(Coordinates& origin) = 0;

protected:
    Board mBoard;
};

}} // namespace openxum tzaar

#endif
