/*
 * @file game/tzaar/players/player.hpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _TZAAR_PLAYER_HPP
#define _TZAAR_PLAYER_HPP 1

#include <tzaar/coordinates.hpp>
#include <tzaar/board.hpp>

namespace openxum { namespace tzaar {

class Player
{
public:
    Player(common::Color mycolor) : mColor(mycolor)
    { }

    virtual ~Player()
    { }

    common::Color color() const
    { return mColor; }

    // abstract methods
    virtual void capture(const Coordinates& origin,
        const Coordinates& destination) = 0;
    virtual Board::Phase choose() = 0;
    virtual void first_move(const Coordinates& origin,
        const Coordinates& destination) = 0;
    virtual void make_stack(const Coordinates& origin,
        const Coordinates& destination) = 0;
    virtual void pass() = 0;

    virtual Coordinates capture(Coordinates& origin) = 0;
    virtual void choose(Board::Phase choice) = 0;
    virtual Coordinates first_move(Coordinates& origin) = 0;
    virtual Coordinates make_stack(Coordinates& origin) = 0;
    virtual void no_capture() = 0;

protected:
    void set_color(common::Color color)
    { mColor = color; }

    common::Color mColor;
};

}} // namespace openxum tzaar

#endif
