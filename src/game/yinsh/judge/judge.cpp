/*
 * @file game/yinsh/judge/judge.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <game/yinsh/judge/judge.hpp>
#include <iostream>

using namespace openxum::common;

namespace openxum { namespace yinsh {

void Judge::init(Player& first_player, Player& second_player)
{
    // std::cerr << mBoard.to_string() << std::endl;
    if (mFirstPlaying) {
        put_ring(first_player, second_player);
    } else {
        put_ring(second_player, first_player);
    }
    mFirstPlaying = not mFirstPlaying;
}

void Judge::move_ring(Player& current_player, Player& other_player,
                      const Coordinates& origin)
{
    Coordinates coordinates = current_player.move_ring(origin);

    // std::cerr << "move "
    //           << ((current_player.color() == BLACK) ? "black":"white")
    //           << " ring to " << coordinates.letter()
    //           << coordinates.number() << std::endl;

    other_player.move_ring(origin, coordinates, current_player.color());
    mBoard.move_ring(origin, coordinates);
}

void Judge::play(Player& first_player, Player& second_player)
{
    if (mBoard.current_color() == first_player.color()) {
        if (mBoard.phase() == Board::PUT_RING) {
            put_ring(first_player, second_player);
        } else if (mBoard.phase() == Board::REMOVE_ROWS_AFTER or
            mBoard.phase() == Board::REMOVE_ROWS_BEFORE) {
            remove_rows(first_player, second_player);
        } else if (mBoard.phase() == Board::PUT_MARKER) {
            mOrigin = put_marker(first_player, second_player);
        } else if (mBoard.phase() == Board::MOVE_RING) {
            move_ring(first_player, second_player, mOrigin);
        }
    } else {
        if (mBoard.phase() == Board::PUT_RING) {
            put_ring(second_player, first_player);
        } else if (mBoard.phase() == Board::REMOVE_ROWS_AFTER or
            mBoard.phase() == Board::REMOVE_ROWS_BEFORE) {
            remove_rows(second_player, first_player);
        } else if (mBoard.phase() == Board::PUT_MARKER) {
            mOrigin = put_marker(second_player, first_player);
        } else if (mBoard.phase() == Board::MOVE_RING) {
            move_ring(second_player, first_player, mOrigin);
        }
    }
    mFirstPlaying = not mFirstPlaying;
}

Coordinates Judge::put_marker(Player& current_player, Player& other_player)
{
    Color player_color = current_player.color();
    Coordinates coordinates = current_player.put_marker();

    // std::cerr << "put " << ((player_color == BLACK) ? "black":"white")
    //           << " marker at " << coordinates.letter()
    //           << coordinates.number() << std::endl;

    other_player.put_marker(coordinates, player_color);
    mBoard.put_marker(coordinates, player_color);
    return coordinates;
}

void Judge::put_ring(Player& current_player, Player& other_player)
{
    Color player_color = current_player.color();
    Coordinates coordinates = current_player.put_ring();

    other_player.put_ring(coordinates, player_color);
    mBoard.put_ring(coordinates, player_color);
}

Coordinates Judge::remove_rows(Player& current_player, Player& other_player)
{
    Color player_color = current_player.color();
    Rows rows;

    if (current_player.remove_rows(rows)) {
        // std::cerr << "remove " << ((player_color == BLACK) ? "black":"white");
        // if (rows.size() == 1) {
        //     std::cerr << " row: " << rows.front().to_string() << std::endl;
        // } else {
        //     std::cerr << " rows:";
        //     for (Rows::const_iterator it = rows.begin(); it != rows.end();
        //          ++it) {
        //         std::cerr << " " << it->to_string();
        //     }
        //     std::cerr << std::endl;
        // }

        other_player.remove_rows(rows, player_color);
        mBoard.remove_rows(rows, player_color);

        {
            Coordinates coordinates = current_player.remove_ring();

            // std::cerr << "remove "
            //           << ((player_color == BLACK) ? "black":"white")
            //           << " ring at " << coordinates.letter()
            //           << coordinates.number()
            //           << std::endl;

            other_player.remove_ring(coordinates, player_color);
            mBoard.remove_ring(coordinates, player_color);
        }
    } else {
        other_player.remove_no_row(player_color);
        mBoard.remove_no_row();
    }
}

}} // namespace openxum yinsh
