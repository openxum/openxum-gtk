/*
 * @file game/yinsh/players/MCTSPlayer.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <game/yinsh/players/MCTSPlayer.hpp>
#include <game/yinsh/players/evaluator.hpp>

#include <iostream>

using namespace openxum::common;

namespace openxum { namespace yinsh {

Coordinates MCTSPlayer::move_ring(const Coordinates& origin)
{
    mBoard.move_ring(origin, mRingCoordinates);
    return mRingCoordinates;
}

Coordinates MCTSPlayer::put_marker()
{
    Evaluator(mColor, mBoard).put_marker_evaluate(mMarkerCoordinates,
                                                  mRingCoordinates);
    mBoard.put_marker(mMarkerCoordinates, mColor);
    return mMarkerCoordinates;
}

Coordinates MCTSPlayer::put_ring()
{
    Coordinates coordinates;

    Evaluator(mColor, mBoard).put_ring_evaluate(coordinates);
    mBoard.put_ring(coordinates, mColor);
    return coordinates;
}

}} // namespace openxum yinsh
