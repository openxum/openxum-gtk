/*
 * @file game/yinsh/players/MCTSPlayer.hpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _YINSH_MCTSPLAYER_HPP
#define _YINSH_MCTSPLAYER_HPP 1

#include <game/yinsh/players/randomPlayer.hpp>

namespace openxum { namespace yinsh {

class MCTSPlayer : public RandomPlayer
{
public:
    MCTSPlayer(Type type, common::Color color, common::Color mycolor) :
        RandomPlayer(type, color, mycolor)
    { }

    virtual ~MCTSPlayer()
    { }

    virtual Coordinates move_ring(const Coordinates& origin);
    virtual Coordinates put_marker();
    virtual Coordinates put_ring();

private:
    Coordinates mRingCoordinates;
    Coordinates mMarkerCoordinates;

    void thread_worker();
};

}} // namespace openxum yinsh

#endif
