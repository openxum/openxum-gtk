/*
 * @file game/yinsh/players/clientPlayer.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <game/yinsh/players/clientPlayer.hpp>
#include <game/yinsh/players/randomPlayer.hpp>

#include <iostream>

#include <arpa/inet.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>

using namespace openxum::common;

namespace openxum { namespace yinsh {

void ClientPlayer::init()
{
    for (unsigned int i = 0; i < 5; ++i) {
        Color color = NONE;

        if (mColor == mPlayer->current_color()) {
            Coordinates coordinates = put_ring();

            send_ring(mSocket, coordinates, mColor);
            receive_ring(mSocket, coordinates, color);
            put_ring(coordinates, color);
        } else {
            Coordinates coordinates;

            receive_ring(mSocket, coordinates, color);
            put_ring(coordinates, color);
            coordinates = put_ring();
            send_ring(mSocket, coordinates, mColor);
        }
    }
}

void ClientPlayer::play_me()
{
    Coordinates origin;
    Coordinates destination;

    std::cerr << "<<< Me >>>" << std::endl;

    remove_my_rows();
    origin = put_marker();
    send_marker(mSocket, origin, mColor);
    destination = move_ring(origin);
    send_move_ring(mSocket, origin, destination, mColor);
    remove_my_rows();
}

void ClientPlayer::play_other()
{
    Coordinates origin;
    Coordinates destination;
    Color color;

    std::cerr << "<<< Other >>>" << std::endl;

    remove_other_rows();
    receive_marker(mSocket, origin, color);
    put_marker(origin, color);

    receive_move_ring(mSocket, origin, destination, color);
    move_ring(origin, destination, color);
    remove_other_rows();
}

void ClientPlayer::remove_my_rows()
{
    Rows rows;

    if (remove_rows(rows)) {
        send_remove_rows(mSocket, rows, mColor);
        {
            Coordinates coordinates = remove_ring();

            send_remove_ring(mSocket, coordinates, mColor);
        }
    } else {
        send_remove_no_row(mSocket);
    }
}

void ClientPlayer::remove_other_rows()
{
    Rows rows;
    Color color = (mColor == BLACK) ? WHITE : BLACK;

    if (receive_remove_rows(mSocket, rows, color)) {
        remove_rows(rows, color);
        {
            Coordinates coordinates;

            receive_remove_ring(mSocket, coordinates, color);
            remove_ring(coordinates, color);
        }
    } else {
        remove_no_row(color);
    }
}

void ClientPlayer::run()
{
    unsigned int i = 1;

    std::cerr << "========== INIT ==========" << std::endl;

    init();

    std::cerr << "========== PLAY ==========" << std::endl;

    while (not mPlayer->is_finished()) {

        std::cerr << "Turn n°" << i << std::endl;

        if (mColor == mPlayer->current_color()) {
            play_me();
        } else {
            play_other();
        }

        std::cerr << mPlayer->display_board() << std::endl;

        ++i;
    }

    std::cerr << "========== END ==========" << std::endl;
}

void ClientPlayer::wait_other_player()
{
    receive_start(mSocket);
}

/*  - - - - - - - - - - - - - --ooOoo-- - - - - - - - - - - -  */

void ClientPlayer::move_ring(const Coordinates& origin,
                             const Coordinates& destination, Color color)
{
    mPlayer->move_ring(origin, destination, color);
}

void ClientPlayer::put_marker(const Coordinates& coordinates, Color color)
{
    mPlayer->put_marker(coordinates, color);
}

void ClientPlayer::put_ring(const Coordinates& coordinates, Color color)
{
    mPlayer->put_ring(coordinates, color);
}

void ClientPlayer::remove_ring(const Coordinates& coordinates, Color color)
{
    mPlayer->remove_ring(coordinates, color);
}

void ClientPlayer::remove_rows(Rows& rows, Color color)
{
    mPlayer->remove_rows(rows, color);
}

void ClientPlayer::remove_no_row(Color color)
{
    mPlayer->remove_no_row(color);
}

/*  - - - - - - - - - - - - - --ooOoo-- - - - - - - - - - - -  */

Coordinates ClientPlayer::move_ring(const Coordinates& origin)
{
    return mPlayer->move_ring(origin);
}

Coordinates ClientPlayer::put_marker()
{
    return mPlayer->put_marker();
}

Coordinates ClientPlayer::put_ring()
{
    return mPlayer->put_ring();
}

Coordinates ClientPlayer::remove_ring()
{
    return mPlayer->remove_ring();
}

bool ClientPlayer::remove_rows(Rows& rows)
{
    return mPlayer->remove_rows(rows);
}

/*  - - - - - - - - - - - - - --ooOoo-- - - - - - - - - - - -  */

// void ClientPlayer::open_connection()
// {
//     pid_t pid = getpid();

//     std::cerr << "starting on port " << mPort << "... ";

//     struct sockaddr_in server_address;

//     mSocket = socket(AF_INET, SOCK_STREAM, 0);
//     memset(&server_address, '\0', sizeof(server_address));
//     server_address.sin_family = AF_INET;
//     server_address.sin_port = htons(mPort);
//     inet_aton(mIP.c_str(), &server_address.sin_addr);

//     int ret = connect(mSocket, (struct sockaddr *) &server_address,
//                       sizeof(server_address));

//     struct sockaddr_in adr_client;
//     socklen_t adr_client_size;

//     if (ret == -1) {
//         std::cerr << std::endl;
//         throw SocketError("Error in (request_connection)");
//     } else {
//         std::cerr << "OK"  << std::endl;
//     }

//     adr_client_size = sizeof(adr_client);
//     ret = getsockname(mSocket, (struct sockaddr *) &adr_client,
//                       &adr_client_size);
//     if (ret == -1) {
//         throw SocketError("Error in (getsockname)");
//     }

//     std::cerr << "(" << pid << ") Requesting a conection to (ip="
//               << inet_ntoa(server_address.sin_addr)
//               << ", port=" << ntohs(server_address.sin_port)
//               << ") from (ip=" << inet_ntoa(adr_client.sin_addr)
//               << ", port=" << ntohs(adr_client.sin_port) << ")"
//               << std::endl;
// }

/*  - - - - - - - - - - - - - --ooOoo-- - - - - - - - - - - -  */

void ClientPlayer::finish()
{
    // close_connection(mSocket);
}

void ClientPlayer::receive_type_colors(Type& type, Color& color, Color& mycolor)
{
    char msg[4]; // [type][color][mycolor][B/R][B/W][B/W]

    // socket_receive(mSocket, msg, 3);
    if (msg[0] == 'B') {
        type = BLITZ;
    } else if (msg[0] == 'R') {
        type = REGULAR;
    } else {
        throw InvalidType("starting client player: invalid type (B/R)");
    }
    if (msg[1] == 'B') {
        color = BLACK;
    } else if (msg[1] == 'W') {
        color = WHITE;
    } else {
        throw InvalidColor("starting client player: invalid color (B/W)");
    }
    if (msg[2] == 'B') {
        mycolor = BLACK;
    } else if (msg[2] == 'W') {
        mycolor = WHITE;
    } else {
        throw InvalidColor("starting client player: invalid color (B/W)");
    }
}

// void ClientPlayer::start()
// {
//     Type type;
//     Color color;
//     Color mycolor;

//     open_connection();
//     receive_type_colors(type, color, mycolor);
//     set_color(mycolor);
//     mPlayer = new Computer(type, color, mycolor);
// }

}} // namespace openxum yinsh
