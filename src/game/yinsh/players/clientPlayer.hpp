/*
 * @file game/yinsh/players/clientPlayer.hpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _CLIENT_PLAYER_HPP
#define _CLIENT_PLAYER_HPP 1

#include <game/yinsh/players/randomPlayer.hpp>
#include <game/yinsh/players/localPlayer.hpp>
#include <game/yinsh/players/remotePlayer.hpp>

namespace openxum { namespace yinsh {

class ClientPlayer : public RemotePlayer
{
public:
    ClientPlayer(Type type, common::Color color, common::Color mycolor) :
        RemotePlayer(mycolor), mPlayer(new RandomPlayer(type, color, mycolor))
    {
        connect();
    }

    virtual ~ClientPlayer()
    {
        finish();
        delete mPlayer;
    }

    void run();
    void wait_other_player();

    virtual void move_ring(const Coordinates& origin,
                           const Coordinates& destination, common::Color color);
    virtual void put_marker(const Coordinates& coordinates,
                            common::Color color);
    virtual void put_ring(const Coordinates& coordinates,
                          common::Color color);
    virtual void remove_ring(const Coordinates& coordinates,
                             common::Color color);
    virtual void remove_rows(Rows& rows, common::Color color);
    virtual void remove_no_row(common::Color color);

    virtual Coordinates move_ring(const Coordinates& origin);
    virtual Coordinates put_marker();
    virtual Coordinates put_ring();
    virtual Coordinates remove_ring();
    virtual bool remove_rows(Rows& rows);

private:
    void finish();
    void init();
    void play_me();
    void play_other();
    void remove_my_rows();
    void remove_other_rows();

    // void open_connection();
    void receive_type_colors(Type& type, common::Color& color,
                             common::Color& mycolor);
    void remove_row(const Row& row, Coordinates& begin, Coordinates& end);
    // void start();

    int mSocket;
    std::string mIP;
    int mPort;

    LocalPlayer* mPlayer;
};

}} // namespace openxum yinsh

#endif
