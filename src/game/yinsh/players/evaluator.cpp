/*
 * @file game/yinsh/players/evaluator.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <game/yinsh/players/evaluator.hpp>
#include <game/yinsh/players/randomPlayer.hpp>

#include <gui/settings.hpp>

#include <stdlib.h>
#include <iostream>

#define DEPTH_NUMBER 0

using namespace openxum::common;

namespace openxum { namespace yinsh {

double Evaluator::evaluate(const Board& board)
{
    double value = 0;
    int sampleNumber = Settings::settings().getYinshLevelNumber() * 10;

    for (int i = 1; i <= sampleNumber; ++i) {
        Judge judge(board.type(), board.current_color());
        RandomPlayer* player_one = new RandomPlayer(board.type(),
                                                    judge.current_color(),
                                                    judge.current_color());
        RandomPlayer* player_two = new RandomPlayer(board.type(),
                                                    judge.current_color(),
                                                    judge.next_color());

        judge.set_board(board);
        player_one->set_board(board);
        player_two->set_board(board);
        while (not judge.is_finished()) {
            judge.play(*player_one, *player_two);
        }
        delete player_one;
        delete player_two;
        value += judge.winner_is() == mColor ? 100 : -1000;
    }
    return value / sampleNumber;
}

double Evaluator::moving_ring_evaluate(const Board& board,
                                       const Coordinates& origin,
                                       Coordinates& ring_coordinates)
{
    PossibleMovingList list =
        board.get_possible_moving_list(origin, mColor);
    double max = -1000;

    for (PossibleMovingList::const_iterator it = list.begin();
         it != list.end(); ++it) {
        double value;
        Board new_board(board);

        new_board.move_ring(origin, (*it)->coordinates());
        value = evaluate(new_board);
        if (value > max) {
            ring_coordinates = (*it)->coordinates();
            max = value;
        }
    }
    return max;
}

void Evaluator::put_marker_evaluate(Coordinates& marker_coordinates,
                                    Coordinates& ring_coordinates)
{
    CoordinatesList list = mBoard.get_placed_ring_coordinates(mColor);
    double max = -1000;

    for (CoordinatesList::const_iterator it = list.begin();
         it != list.end(); ++it) {
        double value;
        Coordinates coordinates;
        Board board(mBoard);

        board.put_marker(*it, mColor);
        value = moving_ring_evaluate(board, *it, coordinates);
        if (value > max) {
            marker_coordinates = *it;
            ring_coordinates = coordinates;
            max = value;
        }
    }
}

void Evaluator::put_ring_evaluate(Coordinates& ring_coordinates)
{
    const CoordinatesList& list = mBoard.get_placed_ring_coordinates(
        mColor == BLACK ? WHITE : BLACK);

    if (list.empty()) {
        ring_coordinates = list[rand() % list.size()];
    } else {
        do {
            int index = rand() % list.size();
            Coordinates coordinates = list[index];
            int delta_letter = rand() % 3 - 1;
            int delta_number = rand() % 3 - 1;

            ring_coordinates = Coordinates(coordinates.letter() + delta_letter,
                                           coordinates.number() + delta_number);
        } while (not mBoard.exist_intersection(ring_coordinates.letter(),
                                               ring_coordinates.number()) or
                 mBoard.intersection_state(
                     ring_coordinates.letter(),
                     ring_coordinates.number()) != VACANT);
    }
}

}} // namespace openxum yinsh
