/*
 * @file game/yinsh/players/evaluator.hpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _YINSH_EVALUATOR_HPP
#define _YINSH_EVALUATOR_HPP 1

#include <yinsh/board.hpp>
#include <game/yinsh/judge/judge.hpp>
#include <game/yinsh/players/player.hpp>

namespace openxum { namespace yinsh {

class Evaluator
{
public:
    Evaluator(common::Color color, const Board& board) :
        mColor(color), mBoard(board)
    { }

    virtual ~Evaluator()
    { }

    void put_marker_evaluate(Coordinates& marker_coordinates,
                             Coordinates& ring_coordinates);
    void put_ring_evaluate(Coordinates& ring_coordinates);

private:
    double evaluate(const Board& board);
    double moving_ring_evaluate(const Board& board,
                                const Coordinates& origin,
                                Coordinates& ring_coordinates);

    common::Color mColor;
    const Board& mBoard;
};

}} // namespace openxum yinsh

#endif
