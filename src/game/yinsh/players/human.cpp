/*
 * @file game/yinsh/players/human.cpp
 *
 * This file is part of OpemXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <game/yinsh/players/human.hpp>
#include <iostream>
#include <stdlib.h>

using namespace openxum::common;

namespace openxum { namespace yinsh {

void Human::display_rows(const Rows& rows)
{
    Rows::const_iterator itr = rows.begin();
    int i = 0;

    std::cout << "Rows:" << std::endl;
    while (itr != rows.end()) {
        std::cout << i << ":  " << itr->to_string() << std::endl;
        ++itr;
        ++i;
    }
}

void Human::display_separated_rows(const SeparatedRows& srows)
{
    SeparatedRows::const_iterator it = srows.begin();

    std::cout << "Separated rows:" << std::endl;
    while (it != srows.end()) {
        display_rows(*it);
        ++it;
    }
}

Coordinates Human::move_ring(const Coordinates& origin)
{
    char destination_letter;
    int destination_number;
    bool ok = false;

    while (not ok) {
        bool fail = false;

        try {
            std::string position;

            std::cout << "Ring moves to: ";
            std::cin >> position;
            convert(position, destination_letter, destination_number);
            mBoard.move_ring(origin, Coordinates(destination_letter,
                                                 destination_number));
        }
        catch (...) {
            fail = true;
        }
        ok = not fail;
    }
    return Coordinates(destination_letter, destination_number);
}

Coordinates Human::put_marker()
{
    char origin_letter;
    int origin_number;
    bool ok = false;

    while (not ok) {
        bool fail = false;

        try {
            std::string position;

            std::cout << "Marker position: ";
            std::cin >> position;
            convert(position, origin_letter, origin_number);
            mBoard.put_marker(Coordinates(origin_letter, origin_number),
                              mColor);
        }
        catch (...) {
            fail = true;
        }
        ok = not fail;
    }
    return Coordinates(origin_letter, origin_number);
}

Coordinates Human::put_ring()
{
    std::string position;
    char letter;
    int number;

    std::cout << mBoard.to_string() << std::endl;

    std::cout << "Color: "
              << (mColor == BLACK ? "BLACK" : "WHITE")
              << std::endl;
    std::cout << "Ring position: ";
    std::cin >> position;

    convert(position, letter, number);
    mBoard.put_ring(Coordinates(letter, number), mColor);
    return Coordinates(letter, number);
}

Coordinates Human::remove_ring()
{
    char letter;
    int number;
    bool ok = false;

    while (not ok) {
        bool fail = false;

        try {
            std::string position;

            std::cout << "Ring to remove: ";
            std::cin >> position;

            convert(position, letter, number);
            mBoard.remove_ring(Coordinates(letter, number), mColor);
        }
        catch (...) {
            fail = true;
        }
        ok = not fail;
    }
    return Coordinates(letter, number);
}

bool Human::remove_rows(Rows& rows)
{
    bool found = false;
    SeparatedRows srows = mBoard.get_rows(mColor);

    if (not srows.empty()) {
        found = true;
        display_separated_rows(srows);
        if (srows.size() == 1) {
            rows.push_back(remove_row(srows.front()));
        } else {
            SeparatedRows::const_iterator it = srows.begin();

            while (it != srows.end()) {
                rows.push_back(remove_row(*it));
                ++it;
            }
        }
        mBoard.remove_rows(rows, mColor);
        remove_ring();
    }
    return found;
}

/*  - - - - - - - - - - - - - --ooOoo-- - - - - - - - - - - -  */

void Human::convert(const std::string& coordinates, char& letter, int& number)
{
    char _number[3];

    letter = coordinates[0];
    if (coordinates.size() == 2) {
        _number[0] = coordinates[1];
        _number[1] = 0;
    } else if (coordinates.size() == 3) {
        _number[0] = coordinates[1];
        _number[1] = coordinates[2];
    } else {
        throw InvalidFormat("invalid format of coordinates");
    }
    _number[2] = 0;
    number = atoi(_number);
}

Row Human::remove_row(const Rows& rows)
{
    Row selectedRow;
    Row removedRow;

    if (rows.size() == 1) {
        selectedRow = rows.front();
    } else {
        bool ok = false;
        int index = -1;

        display_rows(rows);
        while (not ok) {
            std::cout << "Select a row (0.." << (rows.size() - 1) <<  "):";
            std::cin >> index;

            ok = (index >= 0 and index <= rows.size() - 1);
        }
        selectedRow = rows[index];
    }

    if (selectedRow.size() == 5) {
        std::cout << "Remove : "
                  << selectedRow.front().letter()
                  << selectedRow.front().number()
                  << " -> " << selectedRow.back().letter()
                  << selectedRow.back().number()
                  << std::endl;
        removedRow = selectedRow;
    } else {
        bool ok = false;

        while (not ok) {
            std::string _origin;
            char origin_letter;
            int origin_number;

            std::cout << "First marker: ";
            std::cin >> _origin;
            convert(_origin, origin_letter, origin_number);

            std::string _destination;
            char destination_letter;
            int destination_number;

            std::cout << "Last marker: ";
            std::cin >> _destination;
            convert(_destination, destination_letter,
                    destination_number);

            Coordinates origin(origin_letter, origin_number);
            Coordinates destination(destination_letter,
                                    destination_number);

            if (origin.distance(destination) == 5) {

                std::cout << "Remove : "
                          << origin_letter
                          << origin_number
                          << " -> " << destination_letter
                          << destination_number
                          << std::endl;

                removedRow = Row(origin, destination);
                ok = true;
            }
        }
    }
    return removedRow;
}

}} // namespace openxum yinsh
