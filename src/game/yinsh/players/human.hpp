/*
 * @file game/yinsh/players/human.hpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _YINSH_HUMAN_HPP
#define _YINSH_HUMAN_HPP 1

#include <yinsh/coordinates.hpp>
#include <yinsh/intersection.hpp>
#include <game/yinsh/players/localPlayer.hpp>
#include <yinsh/board.hpp>

namespace openxum { namespace yinsh {

class InvalidFormat : public std::runtime_error
{
public:
    InvalidFormat(const std::string& argv = std::string()) :
        std::runtime_error(argv)
    { }
};

class Human : public LocalPlayer
{
public:
    Human(Type type, common::Color color, common::Color mycolor) :
        LocalPlayer(type, color, mycolor)
    { }

    virtual ~Human()
    { }

    virtual Coordinates move_ring(const Coordinates& origin);
    virtual Coordinates put_marker();
    virtual Coordinates put_ring();
    virtual Coordinates remove_ring();
    virtual bool remove_rows(Rows& rows);

private:
    void convert(const std::string& coordinates, char& letter, int& number);
    void display_rows(const Rows& rows);
    void display_separated_rows(const SeparatedRows& rows);
    Row remove_row(const Rows& rows);
};

}} // namespace openxum yinsh

#endif
