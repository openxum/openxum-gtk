/*
 * @file game/yinsh/players/localPlayer.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <game/yinsh/players/localPlayer.hpp>

using namespace openxum::common;

namespace openxum { namespace yinsh {

void LocalPlayer::move_ring(const Coordinates& origin,
                            const Coordinates& destination, Color color)
{
    mBoard.move_ring(origin, destination);
}

void LocalPlayer::put_marker(const Coordinates& coordinates, Color color)
{
    mBoard.put_marker(coordinates, color);
}

void LocalPlayer::put_ring(const Coordinates& coordinates, Color color)
{
    mBoard.put_ring(coordinates, color);
}

void LocalPlayer::remove_no_row(common::Color /* color */)
{
    mBoard.remove_no_row();
}

void LocalPlayer::remove_ring(const Coordinates& coordinates, Color color)
{
    mBoard.remove_ring(coordinates, color);
}

void LocalPlayer::remove_rows(Rows& rows, Color color)
{
    mBoard.remove_rows(rows, color);
}

}} // namespace openxum yinsh
