/*
 * @file game/yinsh/players/localPlayer.hpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _YINSH_LOCAL_PLAYER_HPP
#define _YINSH_LOCAL_PLAYER_HPP 1

#include <game/yinsh/players/player.hpp>

namespace openxum { namespace yinsh {

class LocalPlayer : public Player
{
public:
    LocalPlayer(Type type, common::Color color, common::Color mycolor) :
        Player(mycolor), mBoard(type, color)
    { }

    virtual ~LocalPlayer()
    { }

    common::Color current_color() const
    { return mBoard.current_color(); }

    std::string display_board() const
    { return mBoard.to_string(); }

    bool is_finished() const
    { return mBoard.is_finished(); }

    void set_board(const Board& board)
    { mBoard = board; }

    virtual void move_ring(const Coordinates& origin,
                           const Coordinates& destination, common::Color color);
    virtual void put_marker(const Coordinates& coordinates,
                            common::Color color);
    virtual void put_ring(const Coordinates& coordinates, common::Color color);
    virtual void remove_ring(const Coordinates& coordinates,
                             common::Color color);
    virtual void remove_rows(Rows& rows, common::Color color);
    virtual void remove_no_row(common::Color color);

    virtual Coordinates move_ring(const Coordinates& origin) = 0;
    virtual Coordinates put_marker() = 0;
    virtual Coordinates put_ring() = 0;
    virtual Coordinates remove_ring() = 0;
    virtual bool remove_rows(Rows& rows) = 0;

protected:
    Board mBoard;
};

}} // namespace openxum yinsh

#endif
