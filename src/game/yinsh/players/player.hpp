/*
 * @file game/yinsh/players/player.hpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _YINSH_PLAYER_HPP
#define _YINSH_PLAYER_HPP 1

#include <yinsh/coordinates.hpp>
#include <yinsh/board.hpp>

namespace openxum { namespace yinsh {

class Player
{
public:
    Player(common::Color mycolor) : mColor(mycolor)
    { }

    virtual ~Player()
    { }

    common::Color color() const
    { return mColor; }

    // abstract methods
    virtual void move_ring(const openxum::yinsh::Coordinates& origin,
                           const openxum::yinsh::Coordinates& destination,
                           common::Color color) = 0;
    virtual void put_marker(const openxum::yinsh::Coordinates& coordinates,
                            common::Color color) = 0;
    virtual void put_ring(const openxum::yinsh::Coordinates& coordinates,
                          common::Color color) = 0;
    virtual void remove_ring(const openxum::yinsh::Coordinates& coordinates,
                             common::Color color) = 0;
    virtual void remove_rows(Rows& rows, common::Color color) = 0;
    virtual void remove_no_row(common::Color color) = 0;

    virtual openxum::yinsh::Coordinates move_ring(
        const openxum::yinsh::Coordinates& origin) = 0;
    virtual openxum::yinsh::Coordinates put_marker() = 0;
    virtual openxum::yinsh::Coordinates put_ring() = 0;
    virtual openxum::yinsh::Coordinates remove_ring() = 0;
    virtual bool remove_rows(Rows& rows) = 0;

protected:
    void set_color(common::Color color)
    { mColor = color; }

    common::Color mColor;
};

}} // namespace openxum yinsh

#endif
