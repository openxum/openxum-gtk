/*
 * @file game/yinsh/players/randomPlayer.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <game/yinsh/players/randomPlayer.hpp>
#include <stdlib.h>

using namespace openxum::common;

namespace openxum { namespace yinsh {

Coordinates RandomPlayer::move_ring(const Coordinates& origin)
{
    PossibleMovingList list =
        mBoard.get_possible_moving_list(origin, mColor);

    if (not list.empty()) {
        Coordinates ring_coordinates =
            list[rand() % list.size()]->coordinates();

        mBoard.move_ring(origin, ring_coordinates);
        return ring_coordinates;
    } else {
        throw InvalidMoving("yinsh: no position for ring moving");
    }
}

Coordinates RandomPlayer::put_marker()
{
    Coordinates ring_coordinates;
    CoordinatesList list = mBoard.get_placed_ring_coordinates(mColor);
    bool ok = false;

    while (not ok) {
        ring_coordinates = list[rand() % list.size()];
        ok = mBoard.get_possible_moving_list(ring_coordinates, mColor,
                                             false).size() > 0;
    }
    mBoard.put_marker(ring_coordinates, mColor);
    return ring_coordinates;
}

Coordinates RandomPlayer::put_ring()
{
    CoordinatesList list = mBoard.get_free_intersections();
    int index = rand() % list.size();
    Coordinates coordinates = list[index];

    mBoard.put_ring(coordinates, mColor);
    return coordinates;
}

Coordinates RandomPlayer::remove_ring()
{
    int ring_index = rand() %
        mBoard.get_placed_ring_coordinates(mColor).size();
    Coordinates ring_coordinates =
        mBoard.get_placed_ring_coordinates(mColor)[ring_index];

    mBoard.remove_ring(ring_coordinates, mColor);
    return ring_coordinates;
}

bool RandomPlayer::remove_rows(Rows& rows)
{
    bool found = false;
    SeparatedRows srows = mBoard.get_rows(mColor);

    if (not srows.empty()) {
        found = true;
        if (srows.size() == 1) {
            rows.push_back(remove_row(srows.front()));
        } else {
            SeparatedRows::const_iterator it = srows.begin();

            while (it != srows.end()) {
                rows.push_back(remove_row(*it));
                ++it;
            }
        }
        mBoard.remove_rows(rows, mColor);
    } else {
        mBoard.remove_no_row();
    }
    return found;
}

/*  - - - - - - - - - - - - - --ooOoo-- - - - - - - - - - - -  */

Row RandomPlayer::remove_row(const Rows& rows)
{
    Row selectedRow;
    Row removedRow;

    if (rows.size() == 1) {
        selectedRow = rows.front();
    } else {
        selectedRow = rows[rand() % rows.size()];
    }

    if (selectedRow.size() == 5) {
        removedRow = selectedRow;
    } else {
        int index = rand() % (selectedRow.size() - 5);

        removedRow = Row(selectedRow[index], selectedRow[index + 4]);
    }
    return removedRow;
}

}} // namespace openxum yinsh
