/*
 * @file game/yinsh/players/randomPlayer.hpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _YINSH_RANDOM_PLAYER_HPP
#define _YINSH_RANDOM_PLAYER_HPP 1

#include <game/yinsh/players/localPlayer.hpp>

namespace openxum { namespace yinsh {

class RandomPlayer : public LocalPlayer
{
public:
    RandomPlayer(Type type, common::Color color, common::Color mycolor) :
        LocalPlayer(type, color, mycolor)
    { }

    virtual ~RandomPlayer()
    { }

    virtual Coordinates move_ring(const Coordinates& origin);
    virtual Coordinates put_marker();
    virtual Coordinates put_ring();
    virtual Coordinates remove_ring();
    virtual bool remove_rows(Rows& rows);

private:
    Row remove_row(const Rows& rows);
};

}} // namespace openxum yinsh

#endif
