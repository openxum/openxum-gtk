/*
 * @file game/yinsh/players/remotePlayer.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <game/yinsh/players/remotePlayer.hpp>
#include <iostream>

#include <stdlib.h>
#include <string.h>
#include <libxml/parser.h>
#include <libxml/tree.h>

// #include <arpa/inet.h>
// #include <string.h>
// #include <sys/socket.h>
// #include <sys/types.h>

using namespace openxum::common;

namespace openxum { namespace yinsh {

// void RemotePlayer::close_connection(int sock) const
// {
//     int ret;
//     pid_t pid = getpid();

//     ret = close(sock);
//     if (ret == -1) {
//         throw SocketError("Error in (close_connection)");
//     } else {
//         std::cerr << "(" << pid << ") Closing connection" << std::endl;
//     }
// }

// int RemotePlayer::create_socket() const
// {
//     int sock;
//     pid_t pid = getpid();

//     sock = socket(AF_INET, SOCK_STREAM, 0);
//     if (sock < 0) {
//         throw SocketError("Error in (create_socket)");
//     } else {
//         std::cerr << "(" << pid << ") Socket created" << std::endl;
//     }
//     return sock;
// }

// void RemotePlayer::socket_receive(int sock, char *msg, int size) const
// {
//     int len = 0;
//     pid_t pid = getpid();
//     len = read(sock, msg, size);

//     if (len == -1) {
//         throw SocketError("Error in (sock_receive)");
//     } else {
//         msg[len] = 0;
//         std::cerr << "(" << pid << ") Received [" << msg << "]" << std::endl;
//     }
// }

// void RemotePlayer::socket_send(int sock, const char *msg) const
// {
//     int len = 0;
//     pid_t pid = getpid();
//     len = write(sock, msg, strlen(msg));

//     if (len != strlen(msg)) {
//         throw SocketError("Error in (sock_send)");
//     } else {
//         std::cerr << "(" << pid << ") Sending [" << msg << "]" << std::endl;
//     }
// }

struct Buffer
{
  char* buffer;
  size_t size;
};

size_t write_data(void *ptr, size_t size, size_t nmemb, void *data)
{
    size_t realsize = size * nmemb;
    struct Buffer* mem = (struct Buffer*) data;
    mem->buffer = (char*)realloc(mem->buffer, mem->size + realsize + 1);

    if (mem->buffer) {
        memcpy(&(mem->buffer[mem->size]), ptr, realsize );
        mem->size += realsize;
        mem->buffer[ mem->size ] = 0;
    }
    return realsize;
}

bool is_failed(char* response)
{
    bool failed = false;
    xmlDocPtr doc = xmlReadMemory(response, strlen(response),
                                  "noname.xml", NULL, 0);

    xmlNode* root_element = xmlDocGetRootElement(doc);
    if (strcmp((const char*)root_element->name, "status") == 0) {
        if (strcmp((const char*)root_element->properties->children->content,
                   "Failed") == 0) {
            failed = true;
        }
    }
    xmlFreeDoc(doc);
    return failed;
}

bool is_ok(char* response)
{
    bool ok = false;
    xmlDocPtr doc = xmlReadMemory(response, strlen(response),
                                  "noname.xml", NULL, 0);

    xmlNode* root_element = xmlDocGetRootElement(doc);
    if (strcmp((const char*)root_element->name, "status") == 0) {
        if (strcmp((const char*)root_element->properties->children->content,
                   "OK") == 0) {
            ok = true;
        }
    }
    xmlFreeDoc(doc);
    return ok;
}

void RemotePlayer::join_game()
{
    char* response =
        send_request("game=yinsh&action=join&id_waiting=17&login=eric62");

    if (response) {
        if (not is_failed(response)) {
            xmlDocPtr doc = xmlReadMemory(response, strlen(response),
                                      "noname.xml", NULL, 0);

            xmlNode* root_element = xmlDocGetRootElement(doc);

            std::cout << "game: " << std::endl;
            for (xmlNode* cur_node = root_element; cur_node;
                 cur_node = cur_node->next) {
                std::cout << "[ ";
                for (xmlAttr* cur_node2 = cur_node->properties; cur_node2;
                     cur_node2 = cur_node2->next) {
                    std::cout << cur_node2->name
                              << " = "
                              << cur_node2->children->content
                              << " ; ";
                }
                std::cout << "]" << std::endl;
            }
            xmlFreeDoc(doc);
        } else {
            std::cout << "join game failed" << std::endl;
        }
    }
}

void RemotePlayer::list_waiting()
{
    char* response = send_request("game=yinsh&action=list_waiting");

    if (response) {
        xmlDocPtr doc = xmlReadMemory(response, strlen(response),
                                      "noname.xml", NULL, 0);

        xmlNode* root_element = xmlDocGetRootElement(doc);
        xmlNode* a_node = root_element->children;

        for (xmlNode* cur_node = a_node; cur_node;
             cur_node = cur_node->next) {
            std::cout << "[ ";
            for (xmlAttr* cur_node2 = cur_node->properties; cur_node2;
                 cur_node2 = cur_node2->next) {
                std::cout << cur_node2->name
                          << " = "
                          << cur_node2->children->content
                          << " ; ";
            }
            std::cout << "]" << std::endl;
        }
        xmlFreeDoc(doc);
    }
    free(response);
}

void RemotePlayer::connect()
{
    char* response = send_request("action=connect&login=eric62&password=toto");

    if (response) {
        if (is_ok(response)) {
            list_waiting();
            if (not mWaitingList.empty()) {
                join_game();
            }
        } else {
            std::cout << "connection failed" << std::endl;
        }
        free(response);
    }
}

static void print_cookies(CURL *curl)
{
  CURLcode res;
  struct curl_slist *cookies;
  struct curl_slist *nc;
  int i;

  printf("Cookies, curl knows:\n");
  res = curl_easy_getinfo(curl, CURLINFO_COOKIELIST, &cookies);
  if (res != CURLE_OK) {
    fprintf(stderr, "Curl curl_easy_getinfo failed: %s\n", curl_easy_strerror(res));
    exit(1);
  }
  nc = cookies, i = 1;
  while (nc) {
    printf("[%d]: %s\n", i, nc->data);
    nc = nc->next;
    i++;
  }
  if (i == 1) {
    printf("(none)\n");
  }
  curl_slist_free_all(cookies);
}

char* RemotePlayer::send_request(const char* request)
{
    if (mHTTPClient) {
        CURLcode result;
        struct Buffer response;

        response.buffer = 0;
        response.size = 0;

        curl_easy_setopt(mHTTPClient, CURLOPT_COOKIEFILE, "");
        curl_easy_setopt(mHTTPClient, CURLOPT_WRITEFUNCTION, write_data);
        curl_easy_setopt(mHTTPClient, CURLOPT_URL,
                         "http://127.0.0.1/remote/index.php");
        curl_easy_setopt(mHTTPClient, CURLOPT_WRITEDATA, (void*)&response);
        curl_easy_setopt(mHTTPClient, CURLOPT_POSTFIELDS, request);
        result = curl_easy_perform(mHTTPClient);
        if (result == CURLE_OK) {

            // print_cookies(mHTTPClient);

            return response.buffer;
        }
    }
    return 0;
}

void RemotePlayer::decode_color(Color& color, char* msg) const
{
    if (msg[0] == 'B') {
        color = BLACK;
    } else if (msg[0] == 'W') {
        color = WHITE;
    } else if (msg[0] == 'N') {
        color = NONE;
    } else {
        throw SocketError("Invalid color");
    }
}

void RemotePlayer::encode_color(Color color, char* msg) const
{
    if (color == BLACK) {
        msg[0] = 'B';
    } else if (color == WHITE) {
        msg[0] = 'W';
    } else {
        msg[0] = 'N';
    }
    msg[1] = 0;
}

void RemotePlayer::decode_int(int& number, char* msg) const
{
    number = msg[0] - '0';
}

void RemotePlayer::encode_int(int number, char* msg) const
{
    msg[0] = number + '0';
    msg[1] = 0;
}

void RemotePlayer::receive_ring(int sock, Coordinates& coordinates,
                                Color& color) const
{
    // char msg[5];

    // socket_receive(sock, msg, 4);

    // if (strlen(msg) < 3) {
    //     throw SocketError("Invalid message");
    // }

    // decode_color(color, msg);
    // coordinates.decode(msg + 1);
}

void RemotePlayer::send_ring(int sock, const Coordinates& coordinates,
                             Color color) const
{
    // char msg[5]; // [color][coordinates]
    // char msg2[4];

    // encode_color(color, msg);
    // coordinates.encode(msg2);
    // strcat(msg, msg2);
    // socket_send(sock, msg);
}

void RemotePlayer::receive_start(int sock) const
{
    // char msg[2];

    // socket_receive(sock, msg, 1);
}

void RemotePlayer::send_start(int sock) const
{
    // const char *msg = "S";

    // socket_send(sock, msg);
}

void RemotePlayer::receive_marker(int sock, Coordinates& coordinates,
                                  Color& color)
{
    // char msg[5];

    // socket_receive(sock, msg, 4);

    // if (strlen(msg) < 3) {
    //     throw SocketError("Invalid message");
    // }

    // decode_color(color, msg);
    // coordinates.decode(msg + 1);
}

void RemotePlayer::send_marker(int sock, const Coordinates& coordinates,
                               Color color)
{
    // char msg[5]; // [color][coordinates]
    // char msg2[4];

    // encode_color(color, msg);
    // coordinates.encode(msg2);
    // strcat(msg, msg2);
    // socket_send(sock, msg);
}

void RemotePlayer::receive_remove_ring(int sock, Coordinates& coordinates,
                                       Color& color)
{
    // char msg[5];

    // socket_receive(sock, msg, 4);

    // if (strlen(msg) < 3) {
    //     throw SocketError("Invalid message");
    // }

    // decode_color(color, msg);
    // coordinates.decode(msg + 1);
}

void RemotePlayer::send_remove_ring(int sock, const Coordinates& coordinates,
                                    Color color)
{
    // char msg[5]; // [color][coordinates]
    // char msg2[4];

    // encode_color(color, msg);
    // coordinates.encode(msg2);
    // strcat(msg, msg2);
    // socket_send(sock, msg);
}

void RemotePlayer::receive_move_ring(int sock, Coordinates& origin,
                                     Coordinates& destination, Color& color)
{
    // char msg[8];

    // socket_receive(sock, msg, 7);

    // if (strlen(msg) < 5) {
    //     throw SocketError("Invalid message");
    // }

    // decode_color(color, msg);
    // origin.decode(msg + 1);
    // destination.decode(msg + 4);
}

void RemotePlayer::send_move_ring(int sock, const Coordinates& origin,
                                  const Coordinates& destination, Color color)
{
    // char msg[8]; // [color][coordinates][coordinates]
    // char msg_origin[4];
    // char msg_destination[4];

    // encode_color(color, msg);
    // origin.encode(msg_origin);
    // destination.encode(msg_destination);
    // strcat(msg, msg_origin);
    // strcat(msg, msg_destination);
    // socket_send(sock, msg);
}

bool RemotePlayer::receive_remove_rows(int sock, Rows& rows, Color& color)
{
    // char msg_header[3];

    // socket_receive(sock, msg_header, 2);

    // if (strlen(msg_header) < 2) {
    //     throw SocketError("Invalid message");
    // }

    // if (msg_header[0] == 'X') {
    //     return false;
    // } else {
    //     int n;

    //     decode_color(color, msg_header);
    //     decode_int(n, msg_header + 1);

    //     char* msg = new char[n * 6 + 1];

    //     socket_receive(sock, msg, n * 6);
    //     for (int i = 0; i < n; ++i) {
    //         Row row;

    //         row.decode(msg + i * 6);
    //         rows.push_back(row);
    //     }
    //     delete[] msg;
    //     return true;
    // }
}

void RemotePlayer::send_remove_rows(int sock, const Rows& rows, Color color)
{
    // char* msg; // [color][row_number]{[coordinates][coordinates]}
    // char msg_number[2];

    // msg = new char[rows.size() * 6 + 3];
    // encode_color(color, msg);
    // encode_int(rows.size(), msg_number);
    // strcat(msg, msg_number);

    // for (Rows::const_iterator it = rows.begin(); it != rows.end(); ++it) {
    //     char msg_row[7];

    //     it->encode(msg_row);
    //     strcat(msg, msg_row);
    // }

    // socket_send(sock, msg);
    // delete[] msg;
}

void RemotePlayer::send_remove_no_row(int sock)
{
    // const char* msg = "XX";

    // socket_send(sock, msg);
}

}} // namespace openxum yinsh
