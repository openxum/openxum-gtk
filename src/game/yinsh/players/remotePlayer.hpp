/*
 * @file game/yinsh/players/remotePlayer.hpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _REMOTE_PLAYER_HPP
#define _REMOTE_PLAYER_HPP 1

#include <game/yinsh/players/player.hpp>
#include <stdexcept>
#include <curl/curl.h>

namespace openxum { namespace yinsh {

class SocketError : public std::runtime_error
{
public:
    SocketError(const std::string& argv = std::string()) :
        std::runtime_error(argv)
    { }
};

class RemotePlayer : public Player
{
public:
    struct waiting_request
    {
        std::string login;
        int id_waiting;
        int color;
    };

    typedef std::vector < waiting_request > waiting_list;

    RemotePlayer(common::Color mycolor) : Player(mycolor)
    { mHTTPClient = curl_easy_init(); }

    virtual ~RemotePlayer()
    { curl_global_cleanup(); }

    const waiting_list& getWaitingList() const
    { return mWaitingList; }

protected:
    // void close_connection(int sock) const;
    // int create_socket() const;
    // void socket_receive(int sock, char* msg, int size) const;
    // void socket_send(int sock, const char* msg) const;

    void connect();
    void join_game();
    void list_waiting();
    char* send_request(const char* request);

    void decode_color(common::Color& color, char* msg) const;
    void encode_color(common::Color color, char* msg) const;

    void decode_int(int& number, char* msg) const;
    void encode_int(int number, char* msg) const;

    void receive_ring(int sock, Coordinates& coordinates,
                      common::Color& color) const;
    void send_ring(int sock, const Coordinates& coordinates,
                   common::Color color) const;
    void receive_start(int sock) const;
    void send_start(int sock) const;
    void receive_marker(int sock, Coordinates& coordinates,
                        common::Color& color);
    void send_marker(int sock, const Coordinates& coordinates,
                     common::Color color);
    void receive_remove_ring(int sock, Coordinates& coordinates,
                             common::Color& color);
    void send_remove_ring(int sock, const Coordinates& coordinates,
                          common::Color color);
    void receive_move_ring(int sock, Coordinates& origin,
                           Coordinates& destination, common::Color& color);
    void send_move_ring(int sock, const Coordinates& origin,
                        const Coordinates& destination, common::Color color);
    bool receive_remove_rows(int sock, Rows& rows, common::Color& color);
    void send_remove_rows(int sock, const Rows& rows, common::Color color);
    void send_remove_no_row(int sock);

private:
    CURL* mHTTPClient;
    waiting_list mWaitingList;
};

}} // namespace openxum yinsh

#endif
