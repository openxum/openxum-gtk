/*
 * @file game/yinsh/players/serverPlayer.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <game/yinsh/players/serverPlayer.hpp>

#include <iostream>

#include <arpa/inet.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>

using namespace openxum::common;

namespace openxum { namespace yinsh {

void ServerPlayer::move_ring(const Coordinates& origin,
                             const Coordinates& destination,
                             Color color)
{
    std::cerr << "send move_ring on " << mPort << " port: "
              << origin.letter() << origin.number()
              << " -> "
              << destination.letter() << destination.number()
              << std::endl;
    send_move_ring(mExchangeSocket, origin, destination, color);
}

void ServerPlayer::put_marker(const Coordinates& coordinates, Color color)
{
    std::cerr << "send marker on " << mPort << " port: " << color << "/"
              << coordinates.letter() << coordinates.number() << std::endl;
    send_marker(mExchangeSocket, coordinates, color);
}

void ServerPlayer::put_ring(const Coordinates& coordinates, Color color)
{
    std::cerr << "send ring on " << mPort << " port: " << color << "/"
              << coordinates.letter() << coordinates.number() << std::endl;
    send_ring(mExchangeSocket, coordinates, color);
}

void ServerPlayer::remove_ring(const Coordinates& coordinates, Color color)
{
    std::cerr << "send remove_ring on " << mPort << " port: " << color << "/"
              << coordinates.letter() << coordinates.number() << std::endl;
    send_remove_ring(mExchangeSocket, coordinates, color);
}

void ServerPlayer::remove_rows(Rows& rows, Color color)
{
    Rows::const_iterator itr = rows.begin();

    std::cerr << "send remove_rows on " << mPort << " port: " << color << " - ";
    while (itr != rows.end()) {
        std::cerr << itr->to_string() << " ";
        ++itr;
    }
    std::cerr << std::endl;

    send_remove_rows(mExchangeSocket, rows, color);
}

void ServerPlayer::remove_no_row(Color color)
{
    std::cerr << "send remove_no_row on " << mPort << " port: "
              << color << std::endl;
    send_remove_no_row(mExchangeSocket);
}

/*  - - - - - - - - - - - - - --ooOoo-- - - - - - - - - - - -  */

Coordinates ServerPlayer::move_ring(const Coordinates& origin)
{
    Coordinates origin2;
    Coordinates destination;
    Color color;

    receive_move_ring(mExchangeSocket, origin2, destination, color);
    std::cerr << "receive move_ring on " << mPort << " port: "
              << color << " - "
              << origin2.letter() << origin2.number()
              << " -> "
              << destination.letter() << destination.number()
              << std::endl;
    return destination;
}

Coordinates ServerPlayer::put_marker()
{
    Coordinates coordinates;
    Color color;

    receive_marker(mExchangeSocket, coordinates, color);
    std::cerr << "receive marker on " << mPort << " port: "
              << color << "/" << coordinates.letter() << coordinates.number()
              << std::endl;
    return coordinates;
}

Coordinates ServerPlayer::put_ring()
{
    Coordinates coordinates;
    Color color;

    receive_ring(mExchangeSocket, coordinates, color);
    std::cerr << "receive ring on " << mPort << " port: "
              << color << "/" << coordinates.letter() << coordinates.number()
              << std::endl;
    return coordinates;
}

Coordinates ServerPlayer::remove_ring()
{
    Coordinates coordinates;
    Color color;

    receive_remove_ring(mExchangeSocket, coordinates, color);
    std::cerr << "receive remove_ring on " << mPort << " port: "
              << color << "/" << coordinates.letter() << coordinates.number()
              << std::endl;
    return coordinates;
}

bool ServerPlayer::remove_rows(Rows& rows)
{
    Coordinates coordinates;
    Color color;

    if (receive_remove_rows(mExchangeSocket, rows, color)) {
        Rows::const_iterator itr = rows.begin();

        std::cerr << "receive remove_rows on " << mPort << " port: "
                  << color << " - ";
        while (itr != rows.end()) {
            std::cerr << itr->to_string() << " ";
            ++itr;
        }
        std::cerr << std::endl;

        return true;
    } else {
        return false;
    }
}

/*  - - - - - - - - - - - - - --ooOoo-- - - - - - - - - - - -  */

void ServerPlayer::finish()
{
    // close_connection(mSocket);
}

void ServerPlayer::send_start() const
{
    RemotePlayer::send_start(mExchangeSocket);
}

void ServerPlayer::send_type_colors(Type type, Color color, Color mycolor)
{
    char msg[4];

    msg[0] = (type == BLITZ) ? 'B' : 'R';
    msg[1] = (color == BLACK) ? 'B' : 'W';
    msg[2] = (mycolor == BLACK) ? 'B' : 'W';
    msg[3] = 0;

    // socket_send(mExchangeSocket, msg);
}

// void ServerPlayer::start(Type type, Color color, Color mycolor)
// {
//     start_server();
//     send_type_colors(type, color, mycolor);
// }

// void ServerPlayer::start_server()
// {
//     pid_t pid = getpid();

//     std::cerr << "starting on port " << mPort << "... ";

//     struct sockaddr_in server_address;

//     mSocket = socket(AF_INET, SOCK_STREAM, 0);

//     memset(&server_address, '\0', sizeof(server_address));
//     server_address.sin_family = AF_INET;
//     server_address.sin_port = htons(mPort);
//     server_address.sin_addr.s_addr = INADDR_ANY;

//     bind(mSocket, (struct sockaddr *) & server_address,
//          sizeof(struct sockaddr_in));

//     int ret = listen(mSocket, 1);

//     if (ret == -1) {
//         std::cerr << std::endl;
//         throw SocketError("Error in wait_connection (listen)");
//     } else {
//         std::cerr << "OK"  << std::endl;
//     }

//     socklen_t adr_client_size;
//     struct sockaddr_in adr_client;

//     adr_client_size = sizeof(adr_client);
//     mExchangeSocket = accept(mSocket, (struct sockaddr *) & adr_client,
//                              &adr_client_size);

//     if (mExchangeSocket == -1) {
//         throw SocketError("Error in wait_connection (accept)");
//     } else {
//         std::cerr << "(" << pid << ") Connection requested from ip: "
//                   << inet_ntoa(adr_client.sin_addr) << std::endl;
//     }
// }

}} // namespace openxum yinsh
