/*
 * @file game/yinsh/players/serverPlayer.hpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SERVER_PLAYER_HPP
#define _SERVER_PLAYER_HPP 1

#include <game/yinsh/players/remotePlayer.hpp>

namespace openxum { namespace yinsh {

class ServerPlayer : public RemotePlayer
{
public:
    ServerPlayer(Type type, common::Color color, common::Color mycolor) :
        RemotePlayer(mycolor)
    {
        // if (color == mycolor) {
        //     mPort = 8000;
        // } else {
        //     mPort = 8001;
        // }
        // start(type, color, mycolor);

        connect();

    }

    virtual ~ServerPlayer()
    { finish(); }

    virtual void move_ring(const Coordinates& origin,
                           const Coordinates& destination, common::Color color);
    virtual void put_marker(const Coordinates& coordinates,
                            common::Color color);
    virtual void put_ring(const Coordinates& coordinates, common::Color color);
    virtual void remove_ring(const Coordinates& coordinates,
                             common::Color color);
    virtual void remove_rows(Rows& rows, common::Color color);
    virtual void remove_no_row(common::Color color);

    virtual Coordinates move_ring(const Coordinates& origin);
    virtual Coordinates put_marker();
    virtual Coordinates put_ring();
    virtual Coordinates remove_ring();
    virtual bool remove_rows(Rows& rows);

    void send_start() const;

private:
    void finish();
    void send_type_colors(Type type, common::Color color,
                          common::Color mycolor);
    // void start(Type type, common::Color color, common::Color mycolor);
    // void start_server();

    int mSocket;
    int mExchangeSocket;
    int mPort;
};

}} // namespace openxum yinsh

#endif
