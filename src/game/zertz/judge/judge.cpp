/*
 * @file game/zertz/judge/judge.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <game/zertz/judge/judge.hpp>
#include <iostream>

using namespace openxum::common;

namespace openxum { namespace zertz {

void Judge::play(Player& first_player, Player& second_player)
{
    if (mFirstPlaying) {
        if (mBoard.can_capture()) {
            capture(first_player, second_player);
        } else {
            put_marble(first_player, second_player);
            remove_ring(first_player, second_player);
            capture_isolated_marbles(first_player, second_player);
        }
    } else {
        if (mBoard.can_capture()) {
            capture(second_player, first_player);
        } else {
            put_marble(second_player, first_player);
            remove_ring(second_player, first_player);
            capture_isolated_marbles(second_player, first_player);
        }
    }
    mFirstPlaying = not mFirstPlaying;
}

Coordinates Judge::put_marble(Player& current_player, Player& other_player)
{
    Number player_number = current_player.number();
    Color color;
    Coordinates coordinates = current_player.put_marble(color);

    std::cerr << "put "
              << ((color == BLACK) ? "black":((color == GREY ? "grey":"white")))
              << " marble at " << coordinates.letter()
              << coordinates.number() << std::endl;

    other_player.put_marble(coordinates, color, player_number);
    mBoard.put_marble(coordinates, color, player_number);
    return coordinates;
}

void Judge::capture(Player& current_player, Player& other_player)
{
    Number player_number = current_player.number();
    Coordinates origin;
    Coordinates destination;
    CoordinatesList list = current_player.capture(origin, destination);

    other_player.capture(origin, destination, list, player_number);
    mBoard.capture(origin, destination, list, player_number);
}

void Judge::capture_isolated_marbles(Player& current_player,
                                     Player& other_player)
{
    Number player_number = current_player.number();
    CoordinatesList list = mBoard.get_isolated_marbles();

    if (not list.empty()) {
        CoordinatesList::const_iterator it = list.begin();

        std::cout << "capture isolated marbles = { ";
        while (it != list.end()) {
            std::cout << it->letter() << it->number() << " ";
            ++it;
        }
        std::cout << "}" << std::endl;

        current_player.capture_marble_and_ring(list, player_number);
        other_player.capture_marble_and_ring(list, player_number);
        mBoard.capture_marble_and_ring(list, player_number);
    }
}

Coordinates Judge::remove_ring(Player& current_player, Player& other_player)
{
    Number player_number = current_player.number();
    Coordinates coordinates = current_player.remove_ring();

    std::cerr << "remove ring at " << coordinates.letter()
              << coordinates.number() << std::endl;

    other_player.remove_ring(coordinates, player_number);
    mBoard.remove_ring(coordinates, player_number);
    return coordinates;
}

}} // namespace openxum zertz
