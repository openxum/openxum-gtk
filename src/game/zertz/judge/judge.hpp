/*
 * @file game/zertz/judge/judge.hpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _ZERTZ_JUDGE_HPP
#define _ZERTZ_JUDGE_HPP 1

#include <zertz/board.hpp>
#include <game/zertz/players/player.hpp>

namespace openxum { namespace zertz {

class Judge
{
public:
    Judge(Type type, Number number) : mBoard(type, number), mFirstPlaying(true)
    { }

    virtual ~Judge()
    { }

    const Board& board() const
    { return mBoard; }
    Board& board()
    { return mBoard; }
    Number current_player() const
    { return mBoard.current_player(); }
    Number next_player() const
    { return mBoard.current_player() == ONE ? TWO : ONE; }
    std::string display_board() const
    { return mBoard.to_string(); }
    void init(Player& first_player, Player& second_player);
    bool is_finished() const
    { return mBoard.is_finished(); }
    void play(Player& first_player, Player& second_player);
    Number winner_is() const
    { return mBoard.winner_is(); }

    Coordinates put_marble(Player& current_player, Player& other_player);
    void capture(Player& current_player, Player& other_player);
    void capture_isolated_marbles(Player& current_player, Player& other_player);
    Coordinates remove_ring(Player& current_player, Player& other_player);

private:
    Board mBoard;
    bool mFirstPlaying;
};

}} // namespace openxum zertz

#endif
