/*
 * @file main.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <stdlib.h>
#include <game/zertz/judge/judge.hpp>
#include <game/zertz/players/computer.hpp>

using namespace openxum::zertz;

int main(int argc, char* argv[])
{

    srand(872164);

    Type type = REGULAR;
    Judge judge(type, ONE);
    Player* player_one = new Computer(type, judge.current_player(),
                                      judge.current_player());
    Player* player_two = new Computer(type, judge.current_player(),
                                      judge.next_player());

    std::cout << "First player is: "
              << (player_one->number() == ONE ? "one" : "two" )
              << std::endl;
    std::cout << "Second player is: "
              << (player_two->number() == ONE ? "one" : "two" )
              << std::endl;

    std::cout << judge.display_board() << std::endl;
    std::cout << "Board is initialized !" << std::endl;

    unsigned int i = 1;

    while (not judge.is_finished()) {

        std::cout << "Turn n°" << i++ << std::endl;

        judge.play(*player_one, *player_two);

        std::cout << judge.display_board() << std::endl;

        char a;
        std::cin >> a;

    }

    std::cout << "Winner is: "
              << (judge.winner_is() == ONE ? "ONE" :
                  (judge.winner_is() == TWO ? "TWO" : "NONE"))
              << std::endl;

    delete player_one;
    delete player_two;
    return 0;
}
