/*
 * @file game/zertz/players/computer.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <game/zertz/players/computer.hpp>
#include <stdlib.h>

using namespace openxum::common;

namespace openxum { namespace zertz {

Coordinates Computer::put_marble(Color& color)
{
    CoordinatesList list = mBoard.get_free_rings();
    int index = rand() % list.size();
    int number = mBoard.available_black_marble_number() +
        mBoard.available_grey_marble_number() +
        mBoard.available_white_marble_number();
    int index2 = rand() % number + 1;

    if (index2 <= mBoard.available_black_marble_number()) {
        color = BLACK;
    } else if (index2 <= mBoard.available_black_marble_number()+
               mBoard.available_grey_marble_number()) {
        color = GREY;
    } else {
        color = WHITE;
    }
    mBoard.put_marble(list[index], color, mNumber);
    return list[index];
}

CoordinatesList Computer::capture(Coordinates& origin,
                                  Coordinates& destination)
{
    CoordinatesList captured_marbles;
    CoordinatesList list = mBoard.get_can_capture_marbles();
    int index = rand() % list.size();
    Coordinates origin_ = list[index];

    origin = list[index];
    do {
        CoordinatesList capturing_marbles =
            mBoard.get_possible_capturing_marbles(origin);
        int capturing_marble_index = rand() % capturing_marbles.size();
        Coordinates captured_marble_coordinates =
            capturing_marbles[capturing_marble_index];

        captured_marbles.push_back(captured_marble_coordinates);
        destination = mBoard.jump_marble(origin,
                                         captured_marble_coordinates,
                                         mNumber);
        origin = destination;
    } while (mBoard.is_possible_capturing_marbles(origin));
    origin = origin_;
    return captured_marbles;
}

Coordinates Computer::remove_ring()
{
    CoordinatesList list = mBoard.get_possible_removing_rings();
    int index = rand() % list.size();
    Coordinates coordinates = list[index];

    mBoard.remove_ring(coordinates, mNumber);
    return coordinates;
}

}} // namespace openxum zertz
