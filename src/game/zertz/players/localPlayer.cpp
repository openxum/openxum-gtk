/*
 * @file game/zertz/players/localPlayer.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <game/zertz/players/localPlayer.hpp>

using namespace openxum::common;

namespace openxum { namespace zertz {

void LocalPlayer::put_marble(const Coordinates& coordinates, Color color,
                             Number number)
{
    mBoard.put_marble(coordinates, color, number);
}

void LocalPlayer::capture(const Coordinates& origin,
                          const Coordinates& destination,
                          const CoordinatesList& captured,
                          Number number)
{
    mBoard.capture(origin, destination, captured, number);
}

void LocalPlayer::capture_marble_and_ring(const CoordinatesList& captured,
                                          Number number)
{
    mBoard.capture_marble_and_ring(captured, number);
}

void LocalPlayer::remove_ring(const Coordinates& coordinates, Number number)
{
    mBoard.remove_ring(coordinates, number);
}

}} // namespace openxum zertz
