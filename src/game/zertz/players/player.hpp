/*
 * @file game/zertz/players/player.hpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _ZERTZ_PLAYER_HPP
#define _ZERTZ_PLAYER_HPP 1

#include <zertz/coordinates.hpp>
#include <zertz/intersection.hpp>
#include <zertz/board.hpp>

namespace openxum { namespace zertz {

class Player
{
public:
    Player(Number number) : mNumber(number)
    { }

    virtual ~Player()
    { }

    Number number() const
    { return mNumber; }

    // abstract methods
    virtual void put_marble(const Coordinates& coordinates, common::Color color,
                            Number number) = 0;
    virtual void capture(const Coordinates& origin,
                         const Coordinates& destination,
                         const CoordinatesList& captured, Number number) = 0;
    virtual void capture_marble_and_ring(const CoordinatesList& captured,
                                         Number number) = 0;
    virtual void remove_ring(const Coordinates& coordinates, Number number) = 0;

    virtual Coordinates put_marble(common::Color& color) = 0;
    virtual CoordinatesList capture(Coordinates& origin,
                                    Coordinates& destination) = 0;
    virtual Coordinates remove_ring() = 0;

protected:
    void set_number(Number number)
    { mNumber = number; }

    Number mNumber;
};

}} // namespace openxum zertz

#endif
