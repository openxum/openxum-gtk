/*
 * @file gui/about.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gtkmm/aboutdialog.h>
#include <gdkmm/pixbuf.h>
#include <gui/about.hpp>
#include <version.hpp>

namespace openxum {

class About::Pimpl
{
public:
    Pimpl(Glib::RefPtr < Gnome::Glade::Xml > refXml);

    ~Pimpl();

    void onAboutClose(int response);

    Gtk::AboutDialog* dialog() const;

private:
    Gtk::AboutDialog* mAbout;
    sigc::connection mConnectionResponse;
};

About::Pimpl::Pimpl(Glib::RefPtr < Gnome::Glade::Xml > refXml)
{
    refXml->get_widget("DialogAbout", mAbout);

    std::string extra(OPENXUM_EXTRA_VERSION);
    if (extra.empty()) {
        mAbout->set_version(OPENXUM_VERSION);
    } else {
        std::stringstream version;

        version << OPENXUM_VERSION << "-" << extra;
        mAbout->set_version(version.str());
    }

    std::stringstream path;

    path << OPENXUM_PREFIX_DIR << "/" << OPENXUM_SHARE_DIRS << "/logo.png";

    mAbout->set_logo(Gdk::Pixbuf::create_from_file(path.str()));

    mConnectionResponse = mAbout->signal_response().connect(
        sigc::mem_fun(*this, &About::Pimpl::onAboutClose));
}

About::Pimpl::~Pimpl()
{
    mConnectionResponse.disconnect();
}

void About::Pimpl::onAboutClose(int /*response*/)
{
    mAbout->hide();
}

Gtk::AboutDialog* About::Pimpl::dialog() const
{
    return mAbout;
}

About::About(Glib::RefPtr < Gnome::Glade::Xml > refXml)
    : mImpl(new Pimpl(refXml))
{
}

About::~About()
{
    delete mImpl;
}

void About::run()
{
    mImpl->dialog()->run();
}

} // namespace openxum
