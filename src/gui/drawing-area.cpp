/*
 * @file gui/drawing-area.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gui/drawing-area.hpp>
#include <yinsh/board.hpp>
#include <yinsh/coordinates.hpp>
#include <yinsh/intersection.hpp>

namespace openxum {

const int DrawingArea::tolerance = 15;

DrawingArea::DrawingArea(Gui* parent) :
    mParent(parent), mHeight(450), mWidth(450), mBuffer(0), mIsRealized(false),
    mPointerX(-1), mPointerY(-1)
{
    set_events(Gdk::POINTER_MOTION_MASK | Gdk::BUTTON_MOTION_MASK |
               Gdk::BUTTON1_MOTION_MASK | Gdk::BUTTON2_MOTION_MASK |
               Gdk::BUTTON3_MOTION_MASK | Gdk::BUTTON_PRESS_MASK |
               Gdk::BUTTON_RELEASE_MASK);
}

void DrawingArea::computeLetter(int x, int y, char& letter)
{
    int index = (x - mOffset) / mDelta_x;
    int x_ref = mOffset + mDelta_x * index;
    int x_ref_2 = mOffset + mDelta_x * (index + 1);

    letter = 'X';
    if (x < mOffset) {
        letter = 'A';
    } else if (x <= x_ref + mDelta_x / 2 and x >= x_ref and
               x <= x_ref + tolerance) {
        letter = 'A' + index;
    } else if (x > x_ref + mDelta_x / 2 and x >= x_ref_2 - tolerance) {
        letter = 'A' + (index + 1);
    }
}

void DrawingArea::computeNumber(int x, int y, int& number)
{
    int _x, _y;

    computeCoordinates('A', 1, _x, _y);

    // translation to A1 and rotation
    int X = x - _x;
    int Y = y - _y;
    double sin_alpha = 1. / std::sqrt(5);
    double cos_alpha = 2. * sin_alpha;

    int x2 = (X * sin_alpha - Y * cos_alpha) + _x;
    int delta_x2 = mDelta_x * cos_alpha;

    int index = (x2 - mOffset) / delta_x2;
    int x_ref = mOffset + delta_x2 * index;
    int x_ref_2 = mOffset + delta_x2 * (index + 1);

    number = -1;
    if (x2 > 0 and x2 < mOffset) {
        number = 1;
    } else if (x2 <= x_ref + delta_x2 / 2 and x2 >= x_ref and
               x2 <= x_ref + tolerance) {
        number = index + 1;
    } else if (x2 > x_ref + delta_x2 / 2 and x2 >= x_ref_2 - tolerance) {
        number = index + 2;
    }
}

bool DrawingArea::computePointer(int x, int y)
{
    bool change = false;
    char letter;
    int number;

    computeLetter(x, y, letter);
    if (letter != 'X') {
        computeNumber(x, y, number);
        if (number != -1) {
            if (mParent->specific()->exist_intersection(letter, number)) {
                computeCoordinates(letter, number, mPointerX, mPointerY);
                change = true;
            } else {
                mPointerX = mPointerY = -1;
                change = true;
            }
        } else {
            if (mPointerX != -1) {
                mPointerX = mPointerY = -1;
                change = true;
            }
        }
    } else {
        if (mPointerX != -1) {
            mPointerX = mPointerY = -1;
            change = true;
        }
    }
    return change;
}

bool DrawingArea::on_configure_event(GdkEventConfigure* event)
{
    mWidth = event->width;
    mHeight = event->height;
    compute_deltas();
    if (mIsRealized) {
        set_size_request(mWidth, mHeight);
        mBuffer = Gdk::Pixmap::create(mWin, mWidth, mHeight, -1);
        queueRedraw();
    }
    return true;
}

bool DrawingArea::on_expose_event(GdkEventExpose*)
{
    if (mIsRealized) {
        if (!mBuffer) {
            set_size_request(mWidth, mHeight);
            mBuffer = Gdk::Pixmap::create(mWin, mWidth, mHeight, -1);
        }
        if (mBuffer) {
            if (mNeedRedraw) {
                mContext = mBuffer->create_cairo_context();
                draw();
                mNeedRedraw = false;
            }
            mWin->draw_drawable(mWingc, mBuffer, 0, 0, 0, 0, -1, -1);
        }
    }
    return true;
}

void DrawingArea::on_realize()
{
    Gtk::DrawingArea::on_realize();
    mWin = get_window();
    mWingc = Gdk::GC::create(mWin);
    mIsRealized = true;
    queueRedraw();
}

void DrawingArea::queueRedraw()
{
    mNeedRedraw = true;
    get_window()->invalidate(true);
    gdk_window_process_updates(get_window()->gobj(), false);
}

void DrawingArea::show_intersection()
{
    if (mPointerX != -1 and mPointerY != -1) {
        mContext->set_source_rgb(255, 0, 0);
        mContext->set_line_width(1.);
        mContext->arc(mPointerX, mPointerY, 5, 0.0, 2 * M_PI);
        mContext->fill();
        mContext->stroke();
    }
}

} // namespace openxum
