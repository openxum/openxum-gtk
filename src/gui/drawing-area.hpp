/*
 * @file gui/drawing-area.hpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _GUI_DRAWING_AREA_HPP
#define _GUI_DRAWING_AREA_HPP 1

#include <gtkmm.h>
#include <libglademm/xml.h>

#include <gui/gui.hpp>

namespace openxum {

class Gui;

class DrawingArea : public Gtk::DrawingArea
{
public:
    DrawingArea(Gui* parent);
    virtual ~DrawingArea()
    { }

    virtual void compute_deltas() =0;
    virtual void draw() =0;
    bool on_configure_event(GdkEventConfigure* event);
    bool on_expose_event(GdkEventExpose*);
    void on_realize();
    void queueRedraw();

private:
    Glib::RefPtr < Gdk::Pixmap > mBuffer;
    Glib::RefPtr < Gdk::Window > mWin;
    Glib::RefPtr < Gdk::GC > mWingc;
    bool mIsRealized;
    bool mNeedRedraw;

protected:
    virtual void computeCoordinates(char letter, int number, int& x,
                                    int& y) = 0;
    virtual void computeLetter(int x, int y, char& letter);
    virtual void computeNumber(int x, int y, int& number);
    bool computePointer(int x, int y);

    void show_intersection();

    Gui* mParent;

    int mHeight;
    int mWidth;
    Cairo::RefPtr < Cairo::Context > mContext;

    static const int tolerance;

    int mOffset;
    int mDelta_x;
    int mDelta_y;
    int mDelta_xy;
    int mPointerX;
    int mPointerY;
};

} // namespace openxum

#endif
