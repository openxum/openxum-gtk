/*
 * @file gui/dvonn/drawing-area.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gui/dvonn/drawing-area.hpp>
#include <gui/utils/graphics.hpp>
#include <dvonn/board.hpp>
#include <dvonn/coordinates.hpp>
#include <dvonn/intersection.hpp>

using namespace openxum::common;

namespace openxum { namespace dvonn {

const int DrawingArea::tolerance = 15;

DrawingArea::DrawingArea(openxum::Gui* parent) :
    openxum::DrawingArea(parent), mSelectedPiece('X', -1)
{
}

void DrawingArea::computeCoordinates(char letter, int number, int& x, int& y)
{
    x = mOffset + 1.5 * mDelta_x + (letter - 'A') * mDelta_x -
        (number - 1) * 0.5 * mDelta_x;
    y = mHeight / 2 + (number - 1) * mDelta_y - 2.5 * mDelta_y;
}

void DrawingArea::computeLetter(int x, int y, char& letter)
{
    int _x, _y;

    computeCoordinates('A', 1, _x, _y);

    // translation to A1 and rotation
    int X = x - _x;
    int Y = y - _y;
    double cos_alpha = 1. / std::sqrt(5);
    double sin_alpha = 2. * cos_alpha;

    int x2 = (X * sin_alpha + Y * cos_alpha);
    int delta_x2 = mDelta_x * sin_alpha;
    int index = (x2 + tolerance) / delta_x2 + 1;

    letter = 'X';
    if (index > 0 and index < 12) {
        int ref = (index - 1) * delta_x2 + tolerance;

        if (x2 < ref) {
            letter = 'A' + (index - 1);
        }
    }
}

void DrawingArea::computeNumber(int x, int y, int& number)
{
    int index = ((y + tolerance) - mHeight / 2 + 2.5 * mDelta_y) / mDelta_y + 1;

    number = -1;
    if (index > 0 and index < 6) {
        int ref = mHeight / 2 + (index - 1) * mDelta_y - 2.5 * mDelta_y +
            tolerance;

        if (y < ref) {
            number = index;
        }
    }
}

void DrawingArea::compute_deltas()
{
    mDelta_x = (mWidth - 2 * mOffset) / 11.;
    mDelta_y = mDelta_x;
//    mDelta_xy = mDelta_y / 2;
    mOffset = mDelta_x / 2;
}

void DrawingArea::draw()
{
    mContext->set_line_width(1.);

    // background
    mContext->set_source_rgb(1., 1., 1.);
    mContext->rectangle(0, 0, mWidth, mHeight);
    mContext->fill();
    mContext->stroke();

    // grid
    draw_grid();
    draw_coordinates();

    //state
    draw_state();

    // show intersection
    show_intersection();

    if (parent()->phase() == Gui::MOVE_STACK and mSelectedPiece.is_valid()) {
        draw_possible_moving();
    }
}

void DrawingArea::draw_coordinates()
{
    char text[3];

    text[1] = 0;
    text[2] = 0;

    mContext->set_font_size(16);

    // letters
    for (char l = 'A'; l < 'L'; ++l) {
        int _x, _y;

        text[0] = l;
        computeCoordinates(l, dvonn::Board::begin_number[l - 'A'], _x, _y);
        _x += 5;
        _y -= 5;

        mContext->move_to(_x, _y);
        mContext->show_text(text);
    }

    // numbers
    for (int n = 1; n < 6; ++n) {
        int _x, _y;

        if (n < 10) {
            text[0] = '0' + n;
        } else {
            text[0] = '1';
            text[1] = '0' + (n - 10);
        }
        computeCoordinates(dvonn::Board::begin_letter[n - 1],
                           n, _x, _y);
        _x -= 15 + (n > 3 ? 5 : 0);
        _y -= 3;

        mContext->move_to(_x, _y);
        mContext->show_text(text);
    }
    mContext->stroke();
}

void DrawingArea::draw_grid()
{
    if (parent()->phase() == Gui::IDLE) {
        Gdk::Color color("grey");

        mContext->set_source_rgb(color.get_red_p(),
                                 color.get_green_p(),
                                 color.get_blue_p());
    } else {
        mContext->set_source_rgb(0.2, 0.2, 0.2);
    }

    for (char l = 'A'; l < 'L'; ++l) {
        int _x1, _x2, _y1, _y2;

        computeCoordinates(l, dvonn::Board::begin_number[l - 'A'], _x1, _y1);
        computeCoordinates(l, dvonn::Board::end_number[l - 'A'], _x2, _y2);

        mContext->move_to(_x1, _y1);
        mContext->line_to(_x2, _y2);
    }

    for (int n = 1; n < 6; ++n) {
        int _x1, _x2, _y1, _y2;

        computeCoordinates(dvonn::Board::begin_letter[n - 1], n, _x1, _y1);
        computeCoordinates(dvonn::Board::end_letter[n - 1], n, _x2, _y2);

        mContext->move_to(_x1, _y1);
        mContext->line_to(_x2, _y2);
    }

    for (int i = 0; i < 11; ++i) {
        int _x1, _x2, _y1, _y2;

        computeCoordinates(dvonn::Board::begin_diagonal_letter[i],
                           dvonn::Board::begin_diagonal_number[i], _x1, _y1);
        computeCoordinates(dvonn::Board::end_diagonal_letter[i],
                           dvonn::Board::end_diagonal_number[i], _x2, _y2);

        mContext->move_to(_x1, _y1);
        mContext->line_to(_x2, _y2);
    }
    mContext->stroke();
}

void DrawingArea::draw_piece(int x, int y, Color color, bool selected)
{
    if (selected) {
        mContext->set_source_rgb(192, 192, 192);
        mContext->set_line_width(mDelta_x / 5);
        mContext->arc(x, y, mDelta_x / 3, 0.0, 2 * M_PI);
        mContext->stroke();
    } else {
        Graphics::draw_ring(x, y, mDelta_x, color, mContext);
    }
}

void DrawingArea::draw_possible_moving()
{
    try {
        const CoordinatesList& list = parent()->get_stack_possible_move(
            mSelectedPiece);
        CoordinatesList::const_iterator it = list.begin();

        while (it != list.end()) {
            int x, y;

            computeCoordinates(it->letter(), it->number(), x, y);
            mContext->set_source_rgb(0, 0, 255);
            mContext->set_line_width(1.);
            mContext->arc(x, y, 5, 0.0, 2 * M_PI);
            mContext->fill();
            mContext->stroke();
            ++it;
        }
    } catch (std::runtime_error e) {
    }
}

void DrawingArea::draw_state()
{
    const dvonn::Board::Intersections& intersections =
        parent()->intersections();
    dvonn::Board::Intersections::const_iterator it = intersections.begin();

    while (it != intersections.end()) {
        if (it->second.state() == dvonn::NO_VACANT) {
            int _x, _y;
            int n = it->second.size();

            computeCoordinates(it->second.letter(),
                               it->second.number(), _x, _y);
            draw_piece(_x, _y, it->second.color(), false);
            if (n > 1) {
                char text[3];

                text[1] = 0;
                text[2] = 0;

                mContext->set_font_size(12);
                if (it->second.color() == WHITE) {
                    mContext->set_source_rgb(0, 0, 0);
                } else {
                    mContext->set_source_rgb(1., 1., 1.);
                }
                if (n < 10) {
                    text[0] = '0' + n;
                } else {
                    text[0] = '1';
                    text[1] = '0' + (n - 10);
                }
                mContext->move_to(_x - 5,
                                  _y - mDelta_y * (1. / 3 - 1. / 10) - 2);
                mContext->show_text(text);
                mContext->stroke();
            }
            if (n > 1 and it->second.dvonn()) {
                char text[] = "D";

                mContext->set_font_size(12);
                if (it->second.color() == WHITE) {
                    mContext->set_source_rgb(0, 0, 0);
                } else {
                    mContext->set_source_rgb(1., 1., 1.);
                }
                mContext->move_to(_x - 5,
                                  _y + mDelta_y * (1. / 3 + 1. / 10) - 2);
                mContext->show_text(text);
                mContext->stroke();
            }
        }
        ++it;
    }
    if (mSelectedPiece.is_valid()) {
        draw_piece(mPointerPieceX, mPointerPieceY, mSelectedColor, true);
    }
}

bool DrawingArea::on_button_press_event(GdkEventButton* event)
{
    char letter;
    int number;

    if (parent()->phase() == Gui::MOVE_STACK) {
        computeLetter(event->x, event->y, letter);
        if (letter != 'X') {
            computeNumber(event->x, event->y, number);
            if (number != -1) {
                dvonn::State state = parent()->intersection_state(letter,
                                                                  number);

                if (state == dvonn::NO_VACANT) {
                    common::Color color = parent()->intersection_color(letter,
                                                                       number);

                    if (color != RED and parent()->current_color() == color) {
                        mSelectedPiece = dvonn::Coordinates(letter, number);
                        mSelectedColor = color;
                    }
                }
            }
        }
    }
    return true;
}

bool DrawingArea::on_button_release_event(GdkEventButton* event)
{
    char letter;
    int number;

    computeLetter(event->x, event->y, letter);
    computeNumber(event->x, event->y, number);
    if (letter != 'X' and number != -1 and
        parent()->exist_intersection(letter, number)) {
        if (parent()->phase() == Gui::PUT_DVONN_PIECE and
            parent()->intersection_state(letter, number) == dvonn::VACANT) {
            mSelectedCoordinates = dvonn::Coordinates(letter, number);
            parent()->put_dvonn_piece();
        } else if (parent()->phase() == Gui::PUT_PIECE and
                   parent()->intersection_state(letter, number) ==
                   dvonn::VACANT) {
            mSelectedCoordinates = dvonn::Coordinates(letter, number);
            parent()->put_piece();
        } else if (parent()->phase() == Gui::MOVE_STACK) {
            if (mSelectedPiece.is_valid()) {
                if (parent()->verify_moving(mSelectedPiece,
                                            dvonn::Coordinates(letter,
                                                               number))) {
                    mSelectedCoordinates = dvonn::Coordinates(letter, number);
                    parent()->move_stack();
                }
            }
            mSelectedPiece = dvonn::Coordinates('X', 0);
        }
        queueRedraw();
    }
    return true;
}

bool DrawingArea::on_motion_notify_event(GdkEventMotion* event)
{
    if (event->state & GDK_BUTTON1_MASK) {
        if (parent()->phase() == Gui::MOVE_STACK and
            mSelectedPiece.is_valid()) {
            mPointerPieceX = event->x;
            mPointerPieceY = event->y;
            computePointer(event->x, event->y);
            queueRedraw();
        }
    } else if (event->state & GDK_BUTTON2_MASK) {
    } else if (event->state & GDK_BUTTON3_MASK) {
    } else {
        if (parent()->phase() != Gui::IDLE and
            computePointer(event->x, event->y)) {
            queueRedraw();
        }
    }
}

}} // namespace openxum dvonn
