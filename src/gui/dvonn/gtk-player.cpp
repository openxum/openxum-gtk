/*
 * @file gui/dvonn/gtk-player.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gui/dvonn/gtk-player.hpp>

namespace openxum { namespace dvonn {

bool GtkPlayer::move_stack(Coordinates& origin, Coordinates& destination)
{
    origin = mDrawingArea->selectedPiece();
    destination = mDrawingArea->selectedCoordinates();
    mBoard.move_stack(origin, destination);
    return true;
}

Coordinates GtkPlayer::put_dvonn_piece()
{
    mBoard.put_dvonn_piece(mDrawingArea->selectedCoordinates());
    return mDrawingArea->selectedCoordinates();
}

Coordinates GtkPlayer::put_piece()
{
    mBoard.put_piece(mDrawingArea->selectedCoordinates(), mColor);
    return mDrawingArea->selectedCoordinates();
}

CoordinatesList GtkPlayer::remove_isolated_stacks()
{
    return mBoard.remove_isolated_stacks();
}

}} // namespace openxum dvonn
