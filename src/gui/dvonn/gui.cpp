/*
 * @file gui/dvonn/gui.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gui/dvonn/drawing-area.hpp>
#include <gui/dvonn/gui.hpp>
#include <gui/dvonn/gtk-player.hpp>
#include <game/dvonn/players/computer.hpp>

using namespace openxum::common;

namespace openxum { namespace dvonn {

Gui::Gui(openxum::Gui* gui) : SpecificGui(gui),
                              mPhase(PUT_DVONN_PIECE),
                              mPlayer(0), mOtherPlayer(0), mJudge(0)
{
    mJudge = new dvonn::Judge;
    // TODO
    mPlayer = new dvonn::GtkPlayer(
        dynamic_cast < dvonn::DrawingArea* >(drawing_area()), WHITE, WHITE);
    mOtherPlayer = new dvonn::Computer(WHITE, BLACK);
}

Gui::~Gui()
{
    delete mJudge;
    delete mPlayer;
    delete mOtherPlayer;
}

void Gui::finish()
{
    if (mJudge->winner_is() == WHITE) {
        push_message("White player is winner");
    } else {
        push_message("Black player is winner");
    }
    mPhase = IDLE;
}

void Gui::new_game()
{
    if (mPlayer->color() == mJudge->current_color()) {
        mPhase = PUT_DVONN_PIECE;
        push_message("Put dvonn piece");
    } else {
        put_dvonn_piece_other();
    }
}

void Gui::move_stack()
{
    Color color = mJudge->current_color();
    Coordinates origin, destination;

    mPlayer->move_stack(origin, destination);

    {
        std::stringstream message;

        message << "[" << (mJudge->current_color() == BLACK ? "B" : "W")
                << "] Move stack from "
                << origin.letter() << origin.number()
                << " to " << destination.letter()
                << destination.number() << std::endl;
        add_turn(message.str());
    }

    mOtherPlayer->move_oponent_stack(origin, destination);
    board().move_stack(origin, destination);
    mJudge->remove_isolated_stacks(*mPlayer, *mOtherPlayer);

    if (mJudge->is_finished()) {
        finish();
    } else {
        move_stack_other();
    }
}

void Gui::move_stack_other()
{
    bool stop = false;

    while (not stop) {
        Color color = mJudge->current_color();
        Coordinates origin, destination;

        if (mOtherPlayer->move_stack(origin, destination)) {

            {
                std::stringstream message;

                message << "["
                        << (mJudge->current_color() == BLACK ? "B" : "W")
                        << "] Move stack from "
                        << origin.letter() << origin.number()
                        << " to " << destination.letter()
                        << destination.number() << std::endl;
                add_turn(message.str());
            }

            mPlayer->move_oponent_stack(origin, destination);
            board().move_stack(origin, destination);
            mJudge->remove_isolated_stacks(*mPlayer, *mOtherPlayer);
        } else {
            mPlayer->move_no_stack();
            board().move_no_stack();

            {
                std::stringstream message;

                message << "[" << (color == BLACK ? "B" : "W")
                        << "] Pass" << std::endl;
                add_turn(message.str());
            }
        }
        if (mJudge->is_finished()) {
            stop = true;
        } else {
            stop = not board().get_possible_moving_stacks(
                mJudge->current_color()).empty();
        }
    }
    if (mJudge->is_finished()) {
        finish();
    }
}

void Gui::put_piece()
{
    Color color = mJudge->current_color();
    Coordinates coordinates = mPlayer->put_piece();

    {
        std::stringstream message;

        message << "[" << (mJudge->current_color() == BLACK ? "B" : "W")
                << "] Put "
                << (mJudge->current_color() == BLACK ? "black" : "white")
                << " piece at " << coordinates.letter()
                << coordinates.number() << std::endl;
        add_turn(message.str());
    }

    mOtherPlayer->put_piece(coordinates, color);
    board().put_piece(coordinates, color);

    if (board().phase() == Board::PUT_PIECE) {
        put_piece_other();
    } else {
        if (mOtherPlayer->color() == WHITE) {
            move_stack_other();
        }
        mPhase = MOVE_STACK;
    }
}

void Gui::put_piece_other()
{
    Color color = mJudge->current_color();
    Coordinates coordinates = mOtherPlayer->put_piece();

    {
        std::stringstream message;

        message << "[" << (mJudge->current_color() == BLACK ? "B" : "W")
                << "] Put "
                << (mJudge->current_color() == BLACK ? "black" : "white")
                << " piece at " << coordinates.letter()
                << coordinates.number() << std::endl;
        add_turn(message.str());
    }

    mPlayer->put_piece(coordinates, color);
    board().put_piece(coordinates, color);
    if (board().phase() == Board::MOVE_STACK) {
        mPhase = MOVE_STACK;
    }
}

void Gui::put_dvonn_piece()
{
    Coordinates coordinates = mPlayer->put_dvonn_piece();

    {
        std::stringstream message;

        message << "[" << (mJudge->current_color() == BLACK ? "B" : "W")
                << "] Put dvonn piece at " << coordinates.letter()
                << coordinates.number() << std::endl;
        add_turn(message.str());
    }

    mOtherPlayer->put_dvonn_piece(coordinates);
    board().put_dvonn_piece(coordinates);
    if (board().phase() == Board::PUT_DVONN_PIECE) {
        put_dvonn_piece_other();
    } else {
        put_piece_other();
        mPhase = PUT_PIECE;
    }
}

void Gui::put_dvonn_piece_other()
{
    Coordinates coordinates = mOtherPlayer->put_dvonn_piece();

    {
        std::stringstream message;

        message << "[" << (mJudge->current_color() == BLACK ? "B" : "W")
                << "] Put dvonn piece at " << coordinates.letter()
                << coordinates.number() << std::endl;
        add_turn(message.str());
    }

    mPlayer->put_dvonn_piece(coordinates);
    board().put_dvonn_piece(coordinates);
}

}} // namespace openxum dvonn
