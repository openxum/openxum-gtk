/*
 * @file gui/dvonn/gui.hpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _GUI_DVONN_GUI_HPP
#define _GUI_DVONN_GUI_HPP 1

#include <gui/gui.hpp>
#include <dvonn/coordinates.hpp>
#include <game/dvonn/judge/judge.hpp>

namespace openxum { namespace dvonn {

class Gui : public SpecificGui
{
public:
    enum phase_t { IDLE, PUT_DVONN_PIECE, PUT_PIECE, MOVE_STACK };

    Gui(openxum::Gui* gui);
    virtual ~Gui();

    common::Color current_color() const
    { return mJudge->current_color(); }

    virtual bool exist_intersection(char letter, int number)
    { return board().exist_intersection(letter, number); }

    virtual void finish();

    CoordinatesList get_stack_possible_move(const Coordinates& origin) const
    { return board().get_stack_possible_move(origin); }

    const dvonn::Board::Intersections& intersections() const
    { return board().intersections(); }

    dvonn::State intersection_state(char letter, int number) const
    { return board().intersection_state(letter, number); }

    common::Color intersection_color(char letter, int number) const
    { return board().intersection_color(letter, number); }

    void move_stack();
    virtual void new_game();

    virtual int phase() const
    { return mPhase; }

    void put_dvonn_piece();
    void put_piece();

    bool verify_moving(const Coordinates& origin,
                       const Coordinates& destination) const
    { return board().verify_moving(origin, destination); }

private:
    const dvonn::Board& board() const
    { return mJudge->board(); }

    dvonn::Board& board()
    { return mJudge->board(); }

    void move_stack_other();
    void put_dvonn_piece_other();
    void put_piece_other();

    phase_t mPhase;

    dvonn::Player* mPlayer;
    dvonn::Player* mOtherPlayer;
    dvonn::Judge* mJudge;
};

}} // namespace openxum dvonn

#endif
