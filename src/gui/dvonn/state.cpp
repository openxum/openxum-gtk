/*
 * @file gui/dvonn/state.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gui/utils/graphics.hpp>
#include <gui/dvonn/gui.hpp>
#include <gui/dvonn/state.hpp>

using namespace openxum::common;

namespace openxum { namespace dvonn {

        StateView::StateView(openxum::Gui* parent, openxum::common::Color) :
    openxum::StateView(parent)
{
}

void StateView::draw()
{
    context()->set_line_width(1.);

    // background
    context()->set_source_rgb(1., 1., 1.);
    context()->rectangle(0, 0, width(), height());
    context()->fill();
    context()->stroke();
}

}} // namespace openxum dvonn
