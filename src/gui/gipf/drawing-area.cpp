/*
 * @file gui/gipf/drawing-area.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gui/gipf/drawing-area.hpp>
#include <gui/utils/graphics.hpp>
#include <gipf/board.hpp>
#include <gipf/coordinates.hpp>
#include <gipf/intersection.hpp>

using namespace openxum::common;

namespace openxum { namespace gipf {

DrawingArea::DrawingArea(openxum::Gui* parent) : openxum::DrawingArea(parent)
{
}

void DrawingArea::computeCoordinates(char letter, int number,
                                     int& x, int& y)
{
    x = mOffset + mDelta_x * (letter - 'A');
    y = mOffset + 7 * mDelta_y + mDelta_xy * (letter - 'A') -
        (number - 1) * mDelta_y;
}

void DrawingArea::compute_deltas()
{
    mDelta_x = (mHeight - 2 * mOffset) / 9.;
    mDelta_y = mDelta_x;
    mDelta_xy = mDelta_y / 2;
    mOffset = mDelta_x / 2;
}

void DrawingArea::compute_middle(char first_letter, int first_number,
                                 char second_letter, int second_number,
                                 int& x, int& y)
{
    int x1, y1, x2, y2;

    computeCoordinates(first_letter, first_number, x1, y1);
    computeCoordinates(second_letter, second_number, x2, y2);
    x = x1 + (x2 - x1) / 2;
    y = y1 + (y2 - y1) / 2;
}

void DrawingArea::draw()
{
    mContext->set_line_width(1.);

    // background
    draw_background();

    // grid
    draw_grid();
    draw_coordinates();

    //state
    draw_state();

    // show intersection
    show_intersection();

    if (parent()->phase() == Gui::PUT_FIRST_PIECE) {
        show_possible_first_putting();
    } else if (parent()->phase() == Gui::PUT_PIECE) {
        show_possible_putting();
    } else if (parent()->phase() == Gui::PUSH_PIECE) {
        show_possible_pushing();
    } else if (parent()->phase() == Gui::REMOVE_ROWS) {
        show_possible_pushing();
    }
}

void DrawingArea::draw_background()
{
    mContext->set_source_rgb(1., 1., 1.);
    mContext->rectangle(0, 0, mWidth, mHeight);
    mContext->fill();
    mContext->stroke();
}

void DrawingArea::draw_coordinates()
{
    char text[3];

    text[1] = 0;
    text[2] = 0;

    mContext->set_font_size(16);

    // letters
    for (char l = 'A'; l < 'J'; ++l) {
        int _x, _y;

        text[0] = l;
        computeCoordinates(l, gipf::Board::begin_number[l - 'A'], _x, _y);
        _x -= 5;
        _y += 20;

        mContext->move_to(_x, _y);
        mContext->show_text(text);
    }

    // numbers
    for (int n = 1; n < 10; ++n) {
        int _x, _y;

        if (n < 10) {
            text[0] = '0' + n;
        } else {
            text[0] = '1';
            text[1] = '0' + (n - 10);
        }
        computeCoordinates(gipf::Board::begin_letter[n - 1],
                           n, _x, _y);
        _x -= 15 + (n > 9 ? 5 : 0);
        _y -= 3;

        mContext->move_to(_x, _y);
        mContext->show_text(text);
    }
    mContext->stroke();
}

void DrawingArea::draw_grid()
{
    {
        int _x, _y;
        Gdk::Color color("grey");

        mContext->set_source_rgb(color.get_red_p(),
                                 color.get_green_p(),
                                 color.get_blue_p());

        compute_middle('A', 1, 'B', 2, _x, _y);
        mContext->move_to(_x, _y);
        compute_middle('A', 5, 'B', 5, _x, _y);
        mContext->line_to(_x, _y);
        compute_middle('E', 9, 'E', 8, _x, _y);
        mContext->line_to(_x, _y);
        compute_middle('I', 9, 'H', 8, _x, _y);
        mContext->line_to(_x, _y);
        compute_middle('I', 5, 'H', 5, _x, _y);
        mContext->line_to(_x, _y);
        compute_middle('E', 1, 'E', 2, _x, _y);
        mContext->line_to(_x, _y);
        compute_middle('A', 1, 'B', 2, _x, _y);
        mContext->line_to(_x, _y);
        mContext->close_path();
        mContext->stroke_preserve();
        mContext->fill();
    }

    if (parent()->phase() == Gui::IDLE) {
        Gdk::Color color("grey");

        mContext->set_source_rgb(color.get_red_p(),
                                 color.get_green_p(),
                                 color.get_blue_p());
    } else {
        mContext->set_source_rgb(0.2, 0.2, 0.2);
    }

    for (char l = 'A'; l < 'J'; ++l) {
        int _x1, _x2, _y1, _y2;

        computeCoordinates(l, gipf::Board::begin_number[l - 'A'], _x1, _y1);
        computeCoordinates(l, gipf::Board::end_number[l - 'A'], _x2, _y2);

        mContext->move_to(_x1, _y1);
        mContext->line_to(_x2, _y2);
    }

    for (int n = 1; n < 10; ++n) {
        int _x1, _x2, _y1, _y2;

        computeCoordinates(gipf::Board::begin_letter[n - 1],
                           n, _x1, _y1);
        computeCoordinates(gipf::Board::end_letter[n - 1],
                           n, _x2, _y2);

        mContext->move_to(_x1, _y1);
        mContext->line_to(_x2, _y2);
    }

    for (int i = 0; i < 10; ++i) {
        int _x1, _x2, _y1, _y2;

        computeCoordinates(gipf::Board::begin_diagonal_letter[i],
                           gipf::Board::begin_diagonal_number[i], _x1, _y1);
        computeCoordinates(gipf::Board::end_diagonal_letter[i],
                           gipf::Board::end_diagonal_number[i], _x2, _y2);

        mContext->move_to(_x1, _y1);
        mContext->line_to(_x2, _y2);
    }

    mContext->stroke();
}

void DrawingArea::draw_piece(int x, int y, Color color, bool selected)
{
    if (selected) {
        mContext->set_source_rgb(0.8, 0.8, 0.8);
        mContext->set_line_width(mDelta_x / 5);
        mContext->arc(x, y, mDelta_x / 3, 0.0, 2 * M_PI);
        mContext->stroke();
    } else {
        Gdk::Color piece_color(color == common::BLACK ? "black" : "white");
        Gdk::Color circle_color(color == common::WHITE ? "black" : "white");

        mContext->set_source_rgb(piece_color.get_red_p(),
                                piece_color.get_green_p(),
                                piece_color.get_blue_p());
        mContext->arc(x, y, mDelta_x * (1. / 3 + 1. / 10), 0.0, 2 * M_PI);
        mContext->fill();

        mContext->set_source_rgb(0, 0, 0);
        mContext->set_line_width(1.);
        mContext->arc(x, y, mDelta_x * (1. / 3 + 1. / 10), 0.0, 2 * M_PI);
        mContext->stroke();

        // context->set_source_rgb(circle_color.get_red_p(),
        //                         circle_color.get_green_p(),
        //                         circle_color.get_blue_p());
        // context->set_line_width(width * 1. / 10);
        // context->arc(x, y, width * 2 * (1. / 3 + 1. / 10) / 3,
        //              0.0, 2 * M_PI);
        // context->stroke();

        // context->arc(x, y, width * (1. / 3 + 1. / 10) / 3,
        //              0.0, 2 * M_PI);
        // context->fill();
        // context->stroke();
    }
}

void DrawingArea::draw_rows()
{
    if (parent()->phase() == Gui::REMOVE_ROWS) {
        const SeparatedRows& srows = parent()->get_rows(
            parent()->player_color());
        SeparatedRows::const_iterator its = srows.begin();

        while (its != srows.end()) {
            Rows::const_iterator itr = its->begin();

            while (itr != its->end()) {
                const Coordinates& begin = itr->front();
                const Coordinates& end = itr->back();
                int _x1, _y1, _x2, _y2;
                double alpha_1, beta_1;
                double alpha_2, beta_2;

                computeCoordinates(begin.letter(), begin.number(), _x1, _y1);
                computeCoordinates(end.letter(), end.number(), _x2, _y2);

                if (_x1 == _x2) {
                    if (_y1 < _y2) {
                        alpha_1 = M_PI;
                        beta_1 = 0;
                        alpha_2 = 0;
                        beta_2 = M_PI;
                    } else {
                        alpha_1 = 0;
                        beta_1 = M_PI;
                        alpha_2 = M_PI;
                        beta_2 = 0;
                    }
                } else {
                    double omega_1 = std::acos(1. / std::sqrt(5));

                    if (_x1 < _x2) {
                        if (_y1 < _y2) {
                            alpha_1 = M_PI - omega_1;
                            beta_1 = 3 * M_PI / 2 + omega_1 / 2;
                            alpha_2 = 3 * M_PI / 2 + omega_1 / 2;
                            beta_2 = M_PI - omega_1;
                        } else {
                            alpha_1 = omega_1;
                            beta_1 = M_PI + omega_1;
                            alpha_2 = M_PI + omega_1;
                            beta_2 = omega_1;
                        }
                    }
                }

                std::valarray < double > dashes(2);
                dashes[0] = 2.0;
                dashes[1] = 2.0;

                mContext->set_dash (dashes, 0.0);
                mContext->set_source_rgb(0, 255, 0);
                mContext->set_line_width(4.);
                mContext->arc(_x1, _y1, mDelta_x / 3 + 5, alpha_1, beta_1);
                mContext->line_to(_x2 + (mDelta_x / 3 + 5) * std::cos(alpha_2),
                    _y2 + (mDelta_x / 3 + 5) * std::sin(alpha_2));
                mContext->arc(_x2, _y2, mDelta_x / 3 + 5, alpha_2, beta_2);
                mContext->line_to(_x1 + (mDelta_x / 3 + 5) * std::cos(alpha_1),
                    _y1 + (mDelta_x / 3 + 5) * std::sin(alpha_1));
                mContext->stroke();

                ++itr;
            }
            ++its;
        }

        Row::const_iterator it = mSelectedRow.begin();

        while (it != mSelectedRow.end()) {
            int x,y;

            computeCoordinates(it->letter(), it->number(), x, y);
            mContext->set_source_rgb(0, 0, 255);
            mContext->set_line_width(1.);
            mContext->arc(x, y, mDelta_x * (1. / 3 - 1. / 10) - 1,
                          0.0, 2 * M_PI);
            mContext->fill();
            mContext->stroke();
            ++it;
        }
    }
}

void DrawingArea::draw_state()
{
    const gipf::Board::Intersections& intersections = parent()->intersections();
    gipf::Board::Intersections::const_iterator it = intersections.begin();

    while (it != intersections.end()) {
        int _x, _y;

        computeCoordinates(it->second.letter(), it->second.number(), _x, _y);
        switch (it->second.state()) {
        case gipf::VACANT: break;
        case gipf::BLACK_PIECE:
            draw_piece(_x, _y, BLACK);
            break;
        case gipf::WHITE_PIECE:
            draw_piece(_x, _y, WHITE);
            break;
        case gipf::BLACK_GIPF:
//            draw_piece(_x, _y, BLACK);
            break;
        case gipf::WHITE_GIPF:
//            draw_piece(_x, _y, WHITE);
            break;
        }
        ++it;
    }
    draw_rows();
}

bool DrawingArea::on_button_release_event(GdkEventButton* event)
{
    char letter;
    int number;

    computeLetter(event->x, event->y, letter);
    computeNumber(event->x, event->y, number);
    if (letter != 'X' and number != -1 and
        parent()->exist_intersection(letter, number)) {
        if (parent()->phase() == Gui::PUT_FIRST_PIECE and
            parent()->verify_first_putting(letter, number)) {
            mSelectedCoordinates = gipf::Coordinates(letter, number);
            parent()->put_first_piece();
        } else if (parent()->phase() == Gui::PUT_PIECE and
                   parent()->verify_putting(letter, number)) {
            mSelectedCoordinates = gipf::Coordinates(letter, number);
            parent()->put_piece();
        } else if (parent()->phase() == Gui::PUSH_PIECE and
                   parent()->verify_pushing(letter, number)) {
            mSelectedCoordinates = gipf::Coordinates(letter, number);
            parent()->push_piece();
        }
        else if (parent()->phase() == Gui::REMOVE_ROWS) {
            mSelectedCoordinates = gipf::Coordinates(letter, number);
            parent()->remove_rows();
            mSelectedRow.clear();
        }
        queueRedraw();
    }
    return true;
}

bool DrawingArea::on_motion_notify_event(GdkEventMotion* event)
{
    if (event->state & GDK_BUTTON1_MASK) {
        if (parent()->phase() == Gui::REMOVE_ROWS) {
            char letter;
            int number;

            computeLetter(event->x, event->y, letter);
            computeNumber(event->x, event->y, number);
            if (letter != 'X' and number != -1 and
                std::find(mSelectedRow.begin(), mSelectedRow.end(),
                          gipf::Coordinates(letter,
                                            number)) == mSelectedRow.end()) {
                mSelectedRow.push_back(gipf::Coordinates(letter, number));
            }
            queueRedraw();
        }
    } else if (event->state & GDK_BUTTON2_MASK) {
    } else if (event->state & GDK_BUTTON3_MASK) {
    } else {
        if (parent()->phase() != Gui::IDLE and
            computePointer(event->x, event->y)) {
            queueRedraw();
        }
    }
}

void DrawingArea::show_possible_first_putting()
{
    CoordinatesList list = parent()->get_possible_first_putting_list();
    CoordinatesList::const_iterator it = list.begin();

    while (it != list.end()) {
        int x, y;

        computeCoordinates(it->letter(), it->number(), x, y);
        mContext->set_source_rgb(0, 0, 255);
        mContext->set_line_width(1.);
        mContext->arc(x, y, 5, 0.0, 2 * M_PI);
        mContext->fill();
        mContext->stroke();
        ++it;
    }
}

void DrawingArea::show_possible_pushing()
{
    CoordinatesList list = parent()->get_possible_pushing_list(
        parent()->origin());
    CoordinatesList::const_iterator it = list.begin();

    while (it != list.end()) {
        int x, y;

        computeCoordinates(it->letter(), it->number(), x, y);
        mContext->set_source_rgb(0, 0, 255);
        mContext->set_line_width(1.);
        mContext->arc(x, y, 5, 0.0, 2 * M_PI);
        mContext->fill();
        mContext->stroke();
        ++it;
    }
}

void DrawingArea::show_possible_putting()
{
    CoordinatesList list = parent()->get_possible_putting_list();
    CoordinatesList::const_iterator it = list.begin();

    while (it != list.end()) {
        int x, y;

        computeCoordinates(it->letter(), it->number(), x, y);
        mContext->set_source_rgb(0, 0, 255);
        mContext->set_line_width(1.);
        mContext->arc(x, y, 5, 0.0, 2 * M_PI);
        mContext->fill();
        mContext->stroke();
        ++it;
    }
}

}} // namespace openxum gipf
