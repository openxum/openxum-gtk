/*
 * @file gui/gipf/drawing-area.hpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _GUI_GIPF_DRAWING_AREA_HPP
#define _GUI_GIPF_DRAWING_AREA_HPP 1

#include <gtkmm.h>
#include <libglademm/xml.h>

#include <gui/gui.hpp>
#include <gui/gipf/gui.hpp>
#include <gipf/intersection.hpp>
#include <gipf/row.hpp>

namespace openxum { namespace gipf {

class DrawingArea : public openxum::DrawingArea
{
public:
    DrawingArea(openxum::Gui* parent);
    virtual ~DrawingArea()
    { }

    virtual void compute_deltas();
    virtual void draw();

    bool on_button_release_event(GdkEventButton* event);
    bool on_motion_notify_event(GdkEventMotion* event);

    Coordinates selectedCoordinates() const
    { return mSelectedCoordinates; }

    Row selectedRow() const
    { return mSelectedRow; }

private:
    virtual void computeCoordinates(char letter, int number, int& x, int& y);
    void compute_middle(char first_letter, int first_number,
                        char second_letter, int second_number,
                        int& x, int& y);
    void draw_background();
    void draw_coordinates();
    void draw_grid();
    void draw_piece(int x, int y, common::Color color, bool selected = false);
    void draw_rows();
    void draw_state();

    void show_possible_first_putting();
    void show_possible_pushing();
    void show_possible_putting();

    gipf::Gui* parent() const
    { return dynamic_cast < gipf::Gui* >(mParent->specific()); }

    Coordinates mSelectedCoordinates;
    Row mSelectedRow;

    int mPointerRingX;
    int mPointerRingY;
};

}} // namespace openxum gipf

#endif
