/*
 * @file gui/gipf/gtk-player.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gui/gipf/gtk-player.hpp>

namespace openxum { namespace gipf {

Coordinates GtkPlayer::push_piece()
{
    mBoard.push_piece(mOrigin, mDrawingArea->selectedCoordinates(), mColor);
    return mDrawingArea->selectedCoordinates();
}

Coordinates GtkPlayer::put_first_piece()
{
    mBoard.put_first_piece(mDrawingArea->selectedCoordinates(), mColor, false);
    return mDrawingArea->selectedCoordinates();
}

Coordinates GtkPlayer::put_piece()
{
    mOrigin = mDrawingArea->selectedCoordinates();
    mBoard.put_piece(mDrawingArea->selectedCoordinates(), mColor);
    return mDrawingArea->selectedCoordinates();
}

Rows GtkPlayer::remove_rows()
{
    Rows rows;
    const SeparatedRows& srows = mBoard.get_rows(mColor);
    SeparatedRows::const_iterator its = srows.begin();
    bool found = false;

    while (not found and its != srows.end()) {
        Rows::const_iterator itr = its->begin();

        while (not found and itr != its->end()) {
            if (itr->belong(mDrawingArea->selectedCoordinates())) {
                found = true;
                if (itr->size() >= 4) {
                    rows.push_back(*itr);
                } else if (mDrawingArea->selectedRow().size() >= 4) {
                    rows.push_back(mDrawingArea->selectedRow());
                }
            } else {
                ++itr;
            }
        }
        ++its;
    }
    mBoard.remove_rows(rows, mColor);
    return rows;
}

}} // namespace openxum gipf
