/*
 * @file gui/gipf/gui.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gui/gipf/drawing-area.hpp>
#include <gui/gipf/gtk-player.hpp>
#include <gui/gipf/gui.hpp>

#include <game/gipf/players/computer.hpp>

using namespace openxum::common;

namespace openxum { namespace gipf {

Gui::Gui(openxum::Gui* gui) : SpecificGui(gui),
                              mPhase(IDLE),
                              mPlayer(0), mOtherPlayer(0), mJudge(0)
{
    mJudge = new gipf::Judge(BASIC);
//TODO: preferences
    mPlayer = new gipf::GtkPlayer(
        dynamic_cast < gipf::DrawingArea* >(drawing_area()), BASIC,
        WHITE);
    mOtherPlayer = new gipf::Computer(BASIC, BLACK);
}

Gui::~Gui()
{
    delete mJudge;
    delete mPlayer;
    delete mOtherPlayer;
}

void Gui::finish()
{
    if (mJudge->winner_is() == BLACK) {
        push_message("Black is winner");
    } else {
        push_message("White is winner");
    }
    mPhase = IDLE;
}

void Gui::new_game()
{
    if (mPlayer->color() != mJudge->current_color()) {
        put_first_piece_other();
    } else {
        mPhase = PUT_FIRST_PIECE;
        push_message("Put first piece");
    }
}

void Gui::put_first_piece()
{
    Color color = mPlayer->color();
    Coordinates coordinates = mPlayer->put_first_piece();

    {
        std::stringstream message;

        message << "[" << (mJudge->current_color() == BLACK ? "B" : "W")
                << "] Put first piece at " << coordinates.letter()
                << coordinates.number() << std::endl;
        add_turn(message.str());
    }

    mOtherPlayer->put_first_piece(coordinates);
    board().put_first_piece(coordinates, color, board().game_type() != BASIC);
    if (board().phase() == Board::PUT_FIRST_PIECE) {
        put_first_piece_other();
    } else {
        put_piece_other();
        mPhase = PUT_PIECE;
        push_message("Put a piece");
    }
    redrawState();
}

void Gui::put_first_piece_other()
{
    Color color = mOtherPlayer->color();
    Coordinates coordinates = mOtherPlayer->put_first_piece();

    {
        std::stringstream message;

        message << "[" << (mJudge->current_color() == BLACK ? "B" : "W")
                << "] Put first piece at " << coordinates.letter()
                << coordinates.number() << std::endl;
        add_turn(message.str());
    }

    mPlayer->put_first_piece(coordinates);
    board().put_first_piece(coordinates, color, board().game_type() != BASIC);
    if (board().phase() == Board::PUT_PIECE) {
        mPhase = PUT_PIECE;
        push_message("Put a piece");
    }
}

void Gui::push_piece()
{
    Color color = mPlayer->color();
    Coordinates origin = mPlayer->origin();
    Coordinates destination = mPlayer->push_piece();

    {
        std::stringstream message;

        message << "[" << (mJudge->current_color() == BLACK ? "B" : "W")
                << "] Push piece from " << origin.letter()
                << origin.number() << " to "
                << destination.letter() << destination.number()
                << std::endl;
        add_turn(message.str());
    }

    mOtherPlayer->push_piece(origin, destination);
    board().push_piece(origin, destination, color);
    if (mJudge->is_finished()) {
        finish();
    } else {
        if (board().get_rows(color).empty()) {
            mPlayer->remove_no_row();
            if (board().get_rows(color == BLACK ? WHITE : BLACK).empty()) {
                mOtherPlayer->remove_no_row();
                mPhase = PUT_PIECE;
                put_piece_other();
            } else {
                remove_rows_other();
                mPhase = PUT_PIECE;
                push_message("Put a piece");
            }
        } else {
            mPhase = REMOVE_ROWS;
            push_message("Remove rows");
        }
    }
    redrawState();
}

void Gui::push_piece_other()
{
    Color color = mOtherPlayer->color();
    Coordinates origin = mOtherPlayer->origin();
    Coordinates destination = mOtherPlayer->push_piece();

    {
        std::stringstream message;

        message << "[" << (mJudge->current_color() == BLACK ? "B" : "W")
                << "] Push piece from " << origin.letter()
                << origin.number() << " to "
                << destination.letter() << destination.number()
                << std::endl;
        add_turn(message.str());
    }

    mPlayer->push_piece(origin, destination);
    board().push_piece(origin, destination, color);
    if (mJudge->is_finished()) {
        finish();
    } else {
        if (board().get_rows(color).empty()) {
            mOtherPlayer->remove_no_row();
            if (board().get_rows(color == BLACK ? WHITE : BLACK).empty()) {
                mPlayer->remove_no_row();
                mPhase = PUT_PIECE;
                push_message("Put a piece");
            } else {
                mPhase = REMOVE_ROWS;
                push_message("Remove rows");
            }
        } else {
            remove_rows_other();
            if (board().get_rows(color == BLACK ? WHITE : BLACK).empty()) {
                mPlayer->remove_no_row();
                mPhase = PUT_PIECE;
                push_message("Put a piece");
            } else {
                mPhase = REMOVE_ROWS;
                push_message("Remove rows");
            }
        }
    }
}

void Gui::put_piece()
{
    Color color = mPlayer->color();
    Coordinates coordinates = mPlayer->put_piece();

    {
        std::stringstream message;

        message << "[" << (mJudge->current_color() == BLACK ? "B" : "W")
                << "] Put piece at " << coordinates.letter()
                << coordinates.number() << std::endl;
        add_turn(message.str());
    }

    mOtherPlayer->put_piece(coordinates);
    board().put_piece(coordinates, color);
    mPhase = PUSH_PIECE;
    push_message("Push the piece");
    redrawState();
}

void Gui::put_piece_other()
{
    Color color = mOtherPlayer->color();
    Coordinates coordinates = mOtherPlayer->put_piece();

    {
        std::stringstream message;

        message << "[" << (mJudge->current_color() == BLACK ? "B" : "W")
                << "] Put piece at " << coordinates.letter()
                << coordinates.number() << std::endl;
        add_turn(message.str());
    }

    mPlayer->put_piece(coordinates);
    board().put_piece(coordinates, color);
    mPhase = PUSH_PIECE;
    push_piece_other();
}

void Gui::remove_rows_other()
{
    Color color = mOtherPlayer->color();
    Rows rows = mOtherPlayer->remove_rows();

    if (not rows.empty()) {
        Rows::const_iterator itr = rows.begin();

        while (itr != rows.end()) {
            std::stringstream message;

            message << "["
                    << (color == BLACK ? "B" : "W")
                    << "] Remove " << itr->to_string() << std::endl;
            add_turn(message.str());
            ++itr;
        }

        mPlayer->remove_rows(rows);
        board().remove_rows(rows, color);
        if (mJudge->is_finished()) {
            finish();
        }
    } else {
        mPlayer->remove_no_row();
        board().remove_no_row();
    }
}

void Gui::remove_rows()
{
    Color color = mPlayer->color();
    const SeparatedRows& srows = board().get_rows(color);

    mSelectedRows = mPlayer->remove_rows();
    if (mSelectedRows.size() == srows.size()) {
        Rows::const_iterator itr = mSelectedRows.begin();

        while (itr != mSelectedRows.end()) {
            std::stringstream message;

            message << "["
                    << (color == BLACK ? "B" : "W")
                    << "] Remove " << itr->to_string() << std::endl;
            add_turn(message.str());
            ++itr;
        }
        mOtherPlayer->remove_rows(mSelectedRows);
        board().remove_rows(mSelectedRows, color);
        mSelectedRows.clear();

        if (mJudge->is_finished()) {
            finish();
        } else {
            if (board().get_rows(color == BLACK ? WHITE : BLACK).empty()) {
                mOtherPlayer->remove_no_row();
                mPhase = PUT_PIECE;
                put_piece_other();
            } else {
                remove_rows_other();
            }
        }
    }
    redrawState();
}

bool Gui::verify_first_putting(char letter, int number) const
{
    CoordinatesList list = board().get_possible_first_putting_list();

    return std::find(list.begin(), list.end(), Coordinates(letter, number)) !=
        list.end();
}

bool Gui::verify_pushing(char letter, int number) const
{
    CoordinatesList list = board().get_possible_pushing_list(mPlayer->origin());

    return std::find(list.begin(), list.end(), Coordinates(letter, number)) !=
        list.end();
}

bool Gui::verify_putting(char letter, int number) const
{
    CoordinatesList list = board().get_possible_putting_list();

    return std::find(list.begin(), list.end(), Coordinates(letter, number)) !=
        list.end();
}

}} // namespace openxum gipf
