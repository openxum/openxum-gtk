/*
 * @file gui/gipf/gui.hpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _GUI_GIPF_GUI_HPP
#define _GUI_GIPF_GUI_HPP 1

#include <gui/gui.hpp>
#include <gipf/coordinates.hpp>
#include <game/gipf/judge/judge.hpp>

namespace openxum { namespace gipf {

class Gui : public SpecificGui
{
public:
    enum phase_t { IDLE, PUT_FIRST_PIECE, PUT_PIECE, PUSH_PIECE, REMOVE_ROWS };

    Gui(openxum::Gui* gui);
    virtual ~Gui();

    int capturedPieceNumber(common::Color color) const
    { return (color == BLACK) ? board().blackCapturedPieceNumber() :
            board().whiteCapturedPieceNumber(); }

    common::Color current_color() const
    { return mJudge->current_color(); }

    virtual bool exist_intersection(char letter, int number)
    { return board().exist_intersection(letter, number); }

    virtual void finish();

    virtual int phase() const
    { return mPhase; }

    CoordinatesList get_possible_first_putting_list()
    { return board().get_possible_first_putting_list(); }

    CoordinatesList get_possible_pushing_list(const Coordinates& coordinates)
    { return board().get_possible_pushing_list(coordinates); }

    CoordinatesList get_possible_putting_list()
    { return board().get_possible_putting_list(); }

    gipf::SeparatedRows get_rows(common::Color color) const
    { return board().get_rows(color); }

    gipf::State intersection_state(char letter, int number) const
    { return board().intersection_state(letter, number); }

    const gipf::Board::Intersections& intersections() const
    { return board().intersections(); }

    virtual void new_game();

    const Coordinates& origin() const
    { return mPlayer->origin(); }

    int pieceNumberInReserve(common::Color color) const
    { return (color == BLACK) ? board().blackPieceNumber() :
            board().whitePieceNumber(); }

    common::Color player_color() const
    { return mPlayer->color(); }

    void push_piece();
    void put_first_piece();
    void put_piece();
    void remove_rows();

    bool verify_first_putting(char letter, int number) const;
    bool verify_pushing(char letter, int number) const;
    bool verify_putting(char letter, int number) const;

private:
    const gipf::Board& board() const
    { return mJudge->board(); }

    gipf::Board& board()
    { return mJudge->board(); }

    void push_piece_other();
    void put_first_piece_other();
    void put_piece_other();
    void remove_rows_other();

    phase_t mPhase;

    gipf::Player* mPlayer;
    gipf::Player* mOtherPlayer;
    gipf::Judge* mJudge;

    Rows mSelectedRows;
};

}} // namespace openxum gipf

#endif
