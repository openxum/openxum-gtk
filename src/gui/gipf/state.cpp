/*
 * @file gui/gipf/state.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gui/gipf/gui.hpp>
#include <gui/gipf/state.hpp>
#include <gui/utils/graphics.hpp>

using namespace openxum::common;

namespace openxum { namespace gipf {

StateView::StateView(openxum::Gui* parent, Color color) :
    openxum::StateView(parent), mColor(color)
{
}

void StateView::draw()
{
    gipf::Gui* p = get_parent();
    Color otherColor = mColor == BLACK ? WHITE : BLACK;
    int reserve = p->pieceNumberInReserve(mColor);
    int captured = p->capturedPieceNumber(otherColor);
    char text[3];

    context()->set_line_width(1.);

    // background
    context()->set_source_rgb(1., 1., 1.);
    context()->rectangle(0, 0, width(), height());
    context()->fill();
    context()->stroke();

    context()->set_font_size(10);
    context()->set_source_rgb(0., 0., 0.);
    if (reserve > 0) {
        for (unsigned int i = 1; i <= reserve; ++i) {
            context()->rectangle(i * 10, 10, 8, 10);
            if (mColor == BLACK) {
                context()->fill();
            }
        }
        if (reserve < 10) {
            text[0] = '0' + reserve;
            text[1] = 0;
        } else {
            text[0] = '1';
            text[1] = '0' + (reserve - 10);
            text[2] = 0;
        }
        context()->move_to(20 + reserve * 10, 20);
        context()->show_text(text);
        context()->stroke();
    }

    if (captured > 0) {
        for (unsigned int i = 1; i <= captured; ++i) {
            context()->rectangle(i * 10, 25, 8, 10);
            if (otherColor == BLACK) {
                context()->fill();
            }
        }
        if (captured < 10) {
            text[0] = '0' + captured;
            text[1] = 0;
        } else {
            text[0] = '1';
            text[1] = '0' + (captured - 10);
            text[2] = 0;
        }
        context()->move_to(20 + captured * 10, 35);
        context()->show_text(text);
        context()->stroke();
    }
}

}} // namespace openxum gipf
