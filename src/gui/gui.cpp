/*
 * @file gui/gui.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gui/about.hpp>
#include <gui/gui.hpp>
#include <gui/new-game-box.hpp>
#include <gui/preferences-box.hpp>

// dvonn
#include <gui/dvonn/drawing-area.hpp>
#include <gui/dvonn/gui.hpp>
#include <gui/dvonn/state.hpp>

// gipf
#include <gui/gipf/drawing-area.hpp>
#include <gui/gipf/gui.hpp>
#include <gui/gipf/state.hpp>

// invers
#include <gui/invers/drawing-area.hpp>
#include <gui/invers/gui.hpp>
#include <gui/invers/state.hpp>

// tzaar
#include <gui/tzaar/drawing-area.hpp>
#include <gui/tzaar/gui.hpp>
#include <gui/tzaar/state.hpp>

// yinsh
#include <gui/yinsh/drawing-area.hpp>
#include <gui/yinsh/gui.hpp>
#include <gui/yinsh/state.hpp>

// zertz
#include <gui/zertz/drawing-area.hpp>
#include <gui/zertz/gui.hpp>
#include <gui/zertz/state.hpp>

#include <version.hpp>

#include <sstream>

using namespace openxum::common;

namespace openxum {

Gui::Gui(BaseObjectType* cobject,
         const Glib::RefPtr < Gnome::Glade::Xml > xml) :
    Gtk::Window(cobject), mRefXML(xml), mSpecificGui(0)
{
    Settings::settings().load();

    // by default, Yinsh
    mDrawingArea = new yinsh::DrawingArea(this);
    mSpecificGui = new yinsh::Gui(this);
    mBlackRingState = new yinsh::StateView(this, BLACK);
    mWhiteRingState = new yinsh::StateView(this, WHITE);

    xml->get_widget("MenuVBox", mMenuVBox);
    mMenu = new Menu(this);
    mMenuVBox->pack_start(mMenu->getMenuBar(), false, false);

    xml->get_widget("PlayHBox", mPlayHBox);
    mPlayHBox->set_size_request(890, -1);

    xml->get_widget("StateVBox", mStateVBox);
    xml->get_widget("StatusBar", mStatusBar);

    mTimer = new Timer(this);
    mTurnWindow = new Gtk::ScrolledWindow;
    mTurnView = new TurnView(this);
    mTurnWindow->add(*mTurnView);

    build();

    set_title(OPENXUM_NAME_COMPLETE);

    Glib::signal_idle().connect_once(sigc::mem_fun(this,
                                                   &Gui::redrawDrawingArea));

    resize(640, 480);
    show_all();
}

Gui::~Gui()
{
    delete mSpecificGui;
    delete mDrawingArea;
    delete mBlackRingState;
    delete mWhiteRingState;

    delete mMenu;
    delete mTimer;
    delete mTurnWindow;
    delete mTurnView;

    Settings::settings().kill();
}

void Gui::add_turn(const std::string& message)
{
    mTurnView->add_turn(message);
}

void Gui::build()
{
    mPlayHBox->pack_start(*mDrawingArea, true, true);
    mStateVBox->pack_start(*mBlackRingState, false, false);
    mStateVBox->pack_start(*mWhiteRingState, false, false);
    mStateVBox->pack_start(*mTimer, false, false);
    mStateVBox->pack_start(*mTurnWindow, true, true);
}

void Gui::clear()
{
    mTurnView->clear();
    mPlayHBox->remove(*mDrawingArea);
    mStateVBox->remove(*mBlackRingState);
    mStateVBox->remove(*mWhiteRingState);
    mStateVBox->remove(*mTimer);
    mStateVBox->remove(*mTurnWindow);
}

void Gui::onAbout()
{
    About box(mRefXML);

    box.run();
}

void Gui::onNew()
{
    NewGameBox box(mRefXML);

    if (box.run() == Gtk::RESPONSE_OK) {
        if (box.game() == NewGameBox::PUNCT) {
            Gtk::MessageDialog dialog(*this, "This game is not available!");

            dialog.run();
        } else {
            clear();
            delete mSpecificGui;
            delete mDrawingArea;
            delete mBlackRingState;
            delete mWhiteRingState;
            delete mTimer;
            if (box.game() == NewGameBox::DVONN) {
                mDrawingArea = new dvonn::DrawingArea(this);
                mSpecificGui = new dvonn::Gui(this);
                mBlackRingState = new dvonn::StateView(this, BLACK);
                mWhiteRingState = new dvonn::StateView(this, WHITE);
            } else if (box.game() == NewGameBox::GIPF) {
                mDrawingArea = new gipf::DrawingArea(this);
                mSpecificGui = new gipf::Gui(this);
                mBlackRingState = new gipf::StateView(this, BLACK);
                mWhiteRingState = new gipf::StateView(this, WHITE);
            } else if (box.game() == NewGameBox::INVERS) {
                mDrawingArea = new invers::DrawingArea(this);
                mSpecificGui = new invers::Gui(this);
                mBlackRingState = new invers::StateView(this, RED);
                mWhiteRingState = new invers::StateView(this, YELLOW);
            } else if (box.game() == NewGameBox::TZAAR) {
                mDrawingArea = new tzaar::DrawingArea(this);
                mSpecificGui = new tzaar::Gui(this);
                mBlackRingState = new tzaar::StateView(this, BLACK);
                mWhiteRingState = new tzaar::StateView(this, WHITE);
            } else if (box.game() == NewGameBox::YINSH) {
                mDrawingArea = new yinsh::DrawingArea(this);
                mSpecificGui = new yinsh::Gui(this);
                mBlackRingState = new yinsh::StateView(this, BLACK);
                mWhiteRingState = new yinsh::StateView(this, WHITE);
            } else if (box.game() == NewGameBox::ZERTZ) {
                mDrawingArea = new zertz::DrawingArea(this);
                mSpecificGui = new zertz::Gui(this);
                mBlackRingState = new zertz::StateView(this, zertz::ONE);
                mWhiteRingState = new zertz::StateView(this, zertz::TWO);
            }
            mTimer = new Timer(this);
            build();
            show_all();
            mSpecificGui->new_game();
            mDrawingArea->queueRedraw();
        }
    }
}

void Gui::onPreferences()
{
    PreferencesBox box(mRefXML);

    box.run();
}

bool Gui::on_delete_event(GdkEventAny* event)
{
    if (event->type == GDK_DELETE) {
        onQuit();
        return true;
    }
    return false;
}

void Gui::redrawDrawingArea()
{
    mDrawingArea->queueRedraw();
}

void Gui::redrawState()
{
    mBlackRingState->queueRedraw();
    mWhiteRingState->queueRedraw();
}

} // namespace openxum
