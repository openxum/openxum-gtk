/*
 * @file gui/gui.hpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _GUI_GUI_HPP
#define _GUI_GUI_HPP 1

#include <gui/drawing-area.hpp>
#include <gui/menu.hpp>
#include <gui/settings.hpp>
#include <gui/specific-gui.hpp>
#include <gui/state.hpp>
#include <gui/timer.hpp>
#include <gui/turn-view.hpp>

#include <gtkmm/window.h>
#include <libglademm/xml.h>

namespace openxum {

class SpecificGui;
class DrawingArea;
class StateView;
class Timer;
class TurnView;

class Gui : public Gtk::Window
{
public:
    Gui(BaseObjectType* cobject, const Glib::RefPtr < Gnome::Glade::Xml > xml);
    virtual ~Gui();

    int phase() const;

    void onAbout();

    void onPreferences();

    void onNew();

    void onQuit()
    { hide(); }

    bool on_delete_event(GdkEventAny* event);

    void redrawDrawingArea();
    void redrawState();

    SpecificGui* specific() const
    { return mSpecificGui; }

private:
    void add_turn(const std::string& message);

    void build();

    void clear();

    DrawingArea* drawing_area() const
    { return mDrawingArea; }

    void push_message(const std::string& message)
    { mStatusBar->push(message); }

    const Glib::RefPtr < Gnome::Glade::Xml > mRefXML;
    Gtk::HBox* mPlayHBox;
    Gtk::VBox* mMenuVBox;
    Gtk::VBox* mStateVBox;
    Menu* mMenu;
    DrawingArea* mDrawingArea;
    StateView* mBlackRingState;
    StateView* mWhiteRingState;
    Timer* mTimer;
    Gtk::ScrolledWindow* mTurnWindow;
    TurnView* mTurnView;
    Gtk::Statusbar* mStatusBar;

    // specific game
    SpecificGui* mSpecificGui;

    friend class SpecificGui;
};

} // namespace openxum

#endif
