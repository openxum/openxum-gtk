/*
 * @file gui/invers/drawing-area.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gui/invers/drawing-area.hpp>
#include <invers/board.hpp>
#include <invers/coordinates.hpp>
#include <invers/intersection.hpp>

namespace openxum { namespace invers {

DrawingArea::DrawingArea(openxum::Gui* parent) :
    openxum::DrawingArea(parent), mSelectedCoordinates('X', 0)
{
}

void DrawingArea::computeCoordinates(char letter, int number,
                                     int& x, int& y)
{
    x = mOffset + (letter - 'A') * mDelta_x + mDelta_x / 2;
    y = mOffset + (number - 1) * mDelta_y + mDelta_y / 2;
}

void DrawingArea::compute_deltas()
{
    mOffset = 30;
    mDelta_x = (mHeight - 2 * mOffset) / 6.;
    mDelta_y = mDelta_x;
}

void DrawingArea::draw()
{
    mContext->set_line_width(1.);

    // background
    mContext->set_source_rgb(1., 1., 1.);
    mContext->rectangle(0, 0, mWidth, mHeight);
    mContext->fill();
    mContext->stroke();

    draw_grid();
    draw_coordinates();
}

void DrawingArea::draw_coordinates()
{
    char text[3];

    text[1] = 0;
    text[2] = 0;

    mContext->set_source_rgb(0., 0., 0.);
    mContext->set_font_size(16);

    // letters
    for (char l = 'A'; l <= 'F'; ++l) {
        int _x, _y;

        text[0] = l;
        computeCoordinates(l, 6, _x, _y);
        _x -= 5;
        _y += mDelta_y / 2 + 20;

        mContext->move_to(_x, _y);
        mContext->show_text(text);
    }

    // numbers
    for (int n = 1; n <= 6; ++n) {
        int _x, _y;

        if (n < 10) {
            text[0] = '0' + n;
        } else {
            text[0] = '1';
            text[1] = '0' + (n - 10);
        }
        computeCoordinates('F', n, _x, _y);
        _x += mDelta_x / 2 + 10;

        mContext->move_to(_x, _y);
        mContext->show_text(text);
    }
    mContext->stroke();
}

void DrawingArea::draw_grid()
{
    CoordinatesList top;
    CoordinatesList bottom;
    CoordinatesList left;
    CoordinatesList right;

    parent()->get_possible_position(top, bottom, left, right);
    for (int n = 1; n <= 6; ++n) {
        for (char l = 'A'; l <= 'F'; ++l) {
            int x;
            int y;
            bool selected =
                (mSelectedCoordinates.letter() == l and n == 1 and
                 mSelectedPosition == TOP) or
                (mSelectedCoordinates.letter() == l and n == 6 and
                 mSelectedPosition == BOTTOM) or
                (mSelectedCoordinates.number() == n and l == 'A' and
                 mSelectedPosition == LEFT) or
                (mSelectedCoordinates.number() == n and l == 'F' and
                 mSelectedPosition == RIGHT);
            bool forbidden = false;

            if (parent()->phase() == Gui::PUT_TILE) {
                if (n == 1) {
                    forbidden = std::find(top.begin(), top.end(),
                                          Coordinates(l, 0)) == top.end();
                }
                if (n == 6) {
                    forbidden = std::find(bottom.begin(), bottom.end(),
                                          Coordinates(l, 0)) == bottom.end();
                }
                if (l == 'A') {
                    forbidden = std::find(left.begin(), left.end(),
                                          Coordinates('X', n)) == left.end();
                }
                if (l == 'F') {
                    forbidden = std::find(right.begin(), right.end(),
                                          Coordinates('X', n)) == right.end();
                }
            }

            computeCoordinates(l, n, x, y);
            draw_tile(x, y, parent()->intersection_state(l, n), selected,
                      forbidden);
        }
    }
}

void DrawingArea::draw_tile(int x, int y, State state, bool selected,
    bool forbidden)
{
    if (state == RED_FULL) {
        mContext->set_source_rgb(1., 0., 0.);
        mContext->rectangle(x - mDelta_x / 2 + 2, y - mDelta_y / 2 + 2,
                            mDelta_x - 4, mDelta_y - 4);
        mContext->fill();
    } else if (state == YELLOW_FULL) {
        mContext->set_source_rgb(1., 1., 0.);
        mContext->rectangle(x - mDelta_x / 2 + 2, y - mDelta_y / 2 + 2,
                            mDelta_x - 4, mDelta_y - 4);
        mContext->fill();
    } else if (state == RED_REVERSE) {
        mContext->set_source_rgb(0., 0., 0.);
        mContext->rectangle(x - mDelta_x / 2 + 2, y - mDelta_y / 2 + 2,
                            mDelta_x - 4, mDelta_y - 4);
        mContext->fill();
        mContext->set_source_rgb(1., 0., 0.);
        mContext->rectangle(x - 5, y - 5, 10, 10);
        mContext->fill();
    } else if (state == YELLOW_REVERSE) {
        mContext->set_source_rgb(0., 0., 0.);
        mContext->rectangle(x - mDelta_x / 2 + 2, y - mDelta_y / 2 + 2,
                            mDelta_x - 4, mDelta_y - 4);
        mContext->fill();
        mContext->set_source_rgb(1., 1., 0.);
        mContext->rectangle(x - 5, y - 5, 10, 10);
        mContext->fill();
    }

    mContext->set_line_width(1.);
    mContext->set_source_rgb(0., 0., 0.);
    mContext->rectangle(x - mDelta_x / 2 + 2, y - mDelta_y / 2 + 2,
                        mDelta_x - 4, mDelta_y - 4);
    mContext->stroke();

    if (forbidden) {
        mContext->set_line_width(5.);
        mContext->set_source_rgb(0., 0., 1.);
        mContext->move_to(x - mDelta_x / 2 + 2, y - mDelta_y / 2 + 2);
        mContext->line_to(x + mDelta_x / 2 - 4, y + mDelta_y / 2 - 4);
        mContext->move_to(x + mDelta_x / 2 - 4, y - mDelta_y / 2 + 2);
        mContext->line_to(x - mDelta_x / 2 + 2, y + mDelta_y / 2 - 4);
        mContext->stroke();
    }

    if (selected) {
        mContext->set_source_rgb(0., 0., 1.);
        if (mSelectedPosition == TOP) {
            mContext->move_to(x, y - mDelta_y / 2 + 10);
            mContext->line_to(x - 10, y - mDelta_y / 2 - 10);
            mContext->line_to(x + 10, y - mDelta_y / 2 - 10);
            mContext->line_to(x, y - mDelta_y / 2 + 10);
        } else if (mSelectedPosition == BOTTOM) {
            mContext->move_to(x, y + mDelta_y / 2 - 10);
            mContext->line_to(x - 10, y + mDelta_y / 2 + 10);
            mContext->line_to(x + 10, y + mDelta_y / 2 + 10);
            mContext->line_to(x, y + mDelta_y / 2 - 10);
        } else if (mSelectedPosition == LEFT) {
            mContext->move_to(x - mDelta_x / 2 + 10, y);
            mContext->line_to(x - mDelta_x / 2 - 10, y - 10);
            mContext->line_to(x - mDelta_x / 2 - 10, y + 10);
            mContext->line_to(x - mDelta_x / 2 + 10, y);
        } else if (mSelectedPosition == RIGHT) {
            mContext->move_to(x + mDelta_x / 2 - 10, y);
            mContext->line_to(x + mDelta_x / 2 + 10, y - 10);
            mContext->line_to(x + mDelta_x / 2 + 10, y + 10);
            mContext->line_to(x + mDelta_x / 2 - 10, y);
        }
        mContext->fill();
        mContext->stroke();
    }
}

bool DrawingArea::on_button_release_event(GdkEventButton* event)
{
    if (parent()->phase() == Gui::PUT_TILE and
        (mSelectedCoordinates.letter() != 'X' or
         mSelectedCoordinates.number() != 0)) {
        CoordinatesList top;
        CoordinatesList bottom;
        CoordinatesList left;
        CoordinatesList right;
        bool forbidden = true;

        parent()->get_possible_position(top, bottom, left, right);
        if (mSelectedPosition == TOP and
            std::find(top.begin(), top.end(),
                      mSelectedCoordinates) != top.end()) {
            forbidden = false;
        }
        if (mSelectedPosition == BOTTOM and
            std::find(bottom.begin(), bottom.end(),
                      mSelectedCoordinates) != bottom.end()) {
            forbidden = false;
        }
        if (mSelectedPosition == LEFT and
            std::find(left.begin(), left.end(),
                      mSelectedCoordinates) != left.end()) {
            forbidden = false;
        }
        if (mSelectedPosition == RIGHT and
            std::find(right.begin(), right.end(),
                      mSelectedCoordinates) != right.end()) {
            forbidden = false;
        }

        if (not forbidden) {
            parent()->put_tile();
        }
        mSelectedCoordinates = Coordinates('X', 0);
        queueRedraw();
    }
    return true;
}

bool DrawingArea::on_motion_notify_event(GdkEventMotion* event)
{
    if (event->state & GDK_BUTTON1_MASK) {
    } else if (event->state & GDK_BUTTON2_MASK) {
    } else if (event->state & GDK_BUTTON3_MASK) {
    } else {
        if (parent()->phase() == Gui::PUT_TILE) {
            int x = ((event-> x - mOffset) / mDelta_x);
            int y = ((event-> y - mOffset) / mDelta_y);

            if (y == 0) {
                if (x == 0 and event->x - mOffset < event->y - mOffset) {
                    mSelectedCoordinates = Coordinates('X', y + 1);
                    mSelectedPosition = LEFT;
                } else if (x == 5 and
                           mDelta_y - (event->x - mOffset - 5 * mDelta_x) <
                           event->y - mOffset) {
                    mSelectedCoordinates = Coordinates('X', y + 1);
                    mSelectedPosition = RIGHT;
                } else {
                    mSelectedCoordinates = Coordinates('A' + x, 0);
                    mSelectedPosition = TOP;
                }
            } else if (y == 5) {
                if (x == 5 and event->x - mOffset > event->y - mOffset) {
                    mSelectedCoordinates = Coordinates('X', y + 1);
                    mSelectedPosition = RIGHT;
                } else if (x == 0 and
                           mDelta_y - (event->x - mOffset - 5 * mDelta_x) >
                           event->y - mOffset) {
                    mSelectedCoordinates = Coordinates('X', y + 1);
                    mSelectedPosition = LEFT;
                } else {
                    mSelectedCoordinates = Coordinates('A' + x, 0);
                    mSelectedPosition = BOTTOM;
                }
            } else if (x == 0) {
                mSelectedCoordinates = Coordinates('X', y + 1);
                mSelectedPosition = LEFT;
            } else if (x == 5) {
                mSelectedCoordinates = Coordinates('X', y + 1);
                mSelectedPosition = RIGHT;
            } else {
                mSelectedCoordinates = Coordinates('X', 0);
            }
            queueRedraw();
        }
    }
}

}} // namespace openxum invers
