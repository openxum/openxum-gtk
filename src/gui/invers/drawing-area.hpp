/*
 * @file gui/invers/drawing-area.hpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _GUI_INVERS_DRAWING_AREA_HPP
#define _GUI_INVERS_DRAWING_AREA_HPP 1

#include <gtkmm.h>

#include <gui/gui.hpp>
#include <gui/invers/gui.hpp>
#include <invers/intersection.hpp>

namespace openxum { namespace invers {

class DrawingArea : public openxum::DrawingArea
{
public:
    DrawingArea(openxum::Gui* parent);
    virtual ~DrawingArea()
    { }

    virtual void compute_deltas();
    virtual void draw();

    bool on_button_release_event(GdkEventButton* event);
    bool on_motion_notify_event(GdkEventMotion* event);

    void select_tile(common::Color color, int index)
    {
        mSelectedColor = color;
        mSelectedIndex = index;
    }

    const Coordinates& selectedCoordinates() const
    { return mSelectedCoordinates; }

    const Position& selectedPosition() const
    { return mSelectedPosition; }

    common::Color selected_tile_color() const
    { return mSelectedColor; }

    int selected_tile_index() const
    { return mSelectedIndex; }

private:
    virtual void computeCoordinates(char letter, int number, int& x, int& y);

    void draw_coordinates();
    void draw_grid();
    void draw_tile(int x, int y, State state, bool selected, bool forbidden);

    invers::Gui* parent() const
    { return dynamic_cast < invers::Gui* >(mParent->specific()); }

    int mPointerTileLetter;
    int mPointerTileNumber;

    Coordinates mSelectedCoordinates;
    Position mSelectedPosition;
    common::Color mSelectedColor;
    int mSelectedIndex;
};

}} // namespace openxum invers

#endif
