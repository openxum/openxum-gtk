/*
 * @file gui/invers/gui.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gui/invers/drawing-area.hpp>
#include <gui/invers/gtk-player.hpp>
#include <gui/invers/gui.hpp>

#include <game/invers/players/computer.hpp>

using namespace openxum::common;

namespace openxum { namespace invers {

Gui::Gui(openxum::Gui* gui) : SpecificGui(gui),
                              mPhase(SELECT_TILE),
                              mPlayer(0), mOtherPlayer(0), mJudge(0)
{
    mJudge = new invers::Judge(invers::REGULAR, RED);
    mPlayer = new invers::GtkPlayer(
        dynamic_cast < invers::DrawingArea* >(drawing_area()),
        invers::REGULAR, RED, Settings::settings().getInversColor());
    mOtherPlayer = new invers::Computer(
        invers::REGULAR, RED,
        Settings::settings().getInversColor() == RED ? YELLOW : RED);
}

Gui::~Gui()
{
    delete mJudge;
    delete mOtherPlayer;
    delete mPlayer;
}

void Gui::finish()
{
    if (mJudge->winner_is() == RED) {
        push_message("Player red is winner");
    } else {
        push_message("Player yellow is winner");
    }
    mPhase = IDLE;
}

void Gui::new_game()
{
    if (mPlayer->color() == mJudge->current_player()) {
        mPhase = SELECT_TILE;
        push_message("Select tile");
    } else {
        play_other();
    }
}

void Gui::play_other()
{
    Coordinates coordinates;
    Position position;
    Color color;
    Color player_color = mJudge->current_player();

    coordinates = mOtherPlayer->put_tile(position, color);

    {
        std::stringstream message;

        message << "[" << (player_color == RED ? "R" : "Y")
                << "] Put " << ((color == RED) ? "red":"yellow")
                << " tile at ";
        if (position == TOP or position == BOTTOM) {
            message << coordinates.letter() << " by "
                    << ((position == TOP) ? "top" : "bottom");
        } else {
            message << coordinates.number() << " by "
                    << ((position == LEFT) ? "left" : "right");
        }
        message << std::endl;
        add_turn(message.str());
    }

    board().put_tile(coordinates, position, color, player_color);
    mPlayer->put_tile(coordinates, position, color, player_color);
    gui()->redrawDrawingArea();
    if (mJudge->is_finished()) {
        finish();
    } else {
        mPhase = SELECT_TILE;
    }
}

void Gui::put_tile()
{
    Color player_color = mJudge->current_player();
    Coordinates coordinates;
    Position position;
    common::Color color;

    coordinates = mPlayer->put_tile(position, color);

    {
        std::stringstream message;

        message << "[" << (player_color == RED ? "R" : "Y")
                << "] Put " << ((color == RED) ? "red":"yellow")
                << " tile at ";
        if (position == TOP or position == BOTTOM) {
            message << coordinates.letter() << " by "
                    << ((position == TOP) ? "top" : "bottom");
        } else {
            message << coordinates.number() << " by "
                    << ((position == LEFT) ? "left" : "right");
        }
        message << std::endl;
        add_turn(message.str());
    }

    mOtherPlayer->put_tile(coordinates, position, color, player_color);
    board().put_tile(coordinates, position, color, player_color);
    push_message("Select tile");
    (dynamic_cast < invers::DrawingArea* >(drawing_area()))->
        select_tile(NONE, -1);
    gui()->redrawState();
    if (mJudge->is_finished()) {
        finish();
    } else {
        play_other();
    }
}

void Gui::select_tile(common::Color color, int index)
{
    (dynamic_cast < invers::DrawingArea* >(drawing_area()))->
        select_tile(color, index);
    mPhase = PUT_TILE;
}

common::Color Gui::selected_tile_color() const
{
    return (dynamic_cast < invers::DrawingArea* >(drawing_area()))->
        selected_tile_color();
}

int Gui::selected_tile_index() const
{
    return (dynamic_cast < invers::DrawingArea* >(drawing_area()))->
        selected_tile_index();
}

}} // namespace openxum yinsh
