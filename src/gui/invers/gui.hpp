/*
 * @file gui/invers/gui.hpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _GUI_INVERS_GUI_HPP
#define _GUI_INVERS_GUI_HPP 1

#include <gui/gui.hpp>
#include <invers/coordinates.hpp>
#include <game/invers/judge/judge.hpp>

namespace openxum { namespace invers {

class Gui : public SpecificGui
{
public:
    enum phase_t { IDLE, SELECT_TILE, PUT_TILE };

    Gui(openxum::Gui* gui);
    virtual ~Gui();

    int available_red_tile_number() const
    { return board().red_tile_number(); }

    int available_yellow_tile_number() const
    { return board().yellow_tile_number(); }

    virtual bool exist_intersection(char letter, int number)
    { return board().exist_intersection(letter, number); }

    virtual void finish();

    void get_possible_position(CoordinatesList& top, CoordinatesList& bottom,
                               CoordinatesList& left, CoordinatesList& right)
    { return board().get_possible_position(top, bottom, left, right,
                                           mJudge->current_player()); }

    invers::State intersection_state(char letter, int number) const
    { return board().intersection_state(letter, number); }

    const invers::Board::Intersections& intersections() const
    { return board().intersections(); }

    virtual void new_game();

    virtual int phase() const
    { return mPhase; }

    void put_tile();

    void redrawState()
    { gui()->redrawState(); }

    void select_tile(common::Color color, int index);

    common::Color selected_tile_color() const;

    int selected_tile_index() const;

private:
    const invers::Board& board() const
    { return mJudge->board(); }

    invers::Board& board()
    { return mJudge->board(); }

    void play_other();

    phase_t mPhase;

    invers::Player* mPlayer;
    invers::Player* mOtherPlayer;
    invers::Judge* mJudge;
};

}} // namespace openxum invers

#endif
