/*
 * @file gui/invers/state.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gui/invers/gui.hpp>
#include <gui/invers/state.hpp>

using namespace openxum::common;

namespace openxum { namespace invers {

StateView::StateView(openxum::Gui* parent, Color player) :
    openxum::StateView(parent), mPlayer(player)
{
    mDelta_x = height() - 10;
    mDelta_y = mDelta_x;
}

void StateView::draw()
{
    context()->set_line_width(1.);

    // background
    context()->set_source_rgb(1., 1., 1.);
    context()->rectangle(0, 0, width(), height());
    context()->fill();
    context()->stroke();

    draw_tiles();
}

void StateView::draw_tiles()
{
    int x = mDelta_x / 2 + 5;
    int y = mDelta_y / 2 + 5;

    if (mPlayer == RED) {
        for (int i = 0;
             i < gui()->available_red_tile_number(); ++i) {
            context()->set_line_width(1.);
            context()->set_source_rgb(0., 0., 0.);
            context()->rectangle(x - mDelta_x / 2, y - mDelta_y / 2,
                                 mDelta_x, mDelta_y);
            context()->fill();

            context()->set_source_rgb(1., 0., 0.);
            context()->rectangle(x - 5, y - 5, 10, 10);
            context()->fill();

            if (gui()->selected_tile_color() == RED and
                gui()->selected_tile_index() == i) {
                context()->set_line_width(5.);
                context()->set_source_rgb(1., 1., 0.);
                context()->rectangle(x - mDelta_x / 2, y - mDelta_y / 2,
                                     mDelta_x, mDelta_y);
            }
            context()->stroke();
            x += mDelta_x + 5;
        }
    } else {
        for (int i = 0;
             i < gui()->available_yellow_tile_number(); ++i) {
            context()->set_line_width(1.);
            context()->set_source_rgb(0., 0., 0.);
            context()->rectangle(x - mDelta_x / 2, y - mDelta_y / 2,
                                mDelta_x, mDelta_y);
            context()->fill();

            context()->set_source_rgb(1., 1., 0.);
            context()->rectangle(x - 5, y - 5, 10, 10);
            context()->fill();

            if (gui()->selected_tile_color() == YELLOW and
                gui()->selected_tile_index() == i) {
                context()->set_line_width(5.);
                context()->set_source_rgb(1., 1., 0.);
                context()->rectangle(x - mDelta_x / 2, y - mDelta_y / 2,
                                     mDelta_x, mDelta_y);
            }
            context()->stroke();
            x += mDelta_x + 5;
        }
    }
}

bool StateView::on_button_release_event(GdkEventButton* event)
{
    if (mPlayer == RED) {
        int red = gui()->available_red_tile_number();

        if (red > 0) {
            int x = event->x - 5;
            int y = event->y - 5;

            if (y >= 0 and y <= mDelta_y and
                x >= 0 and x <= mDelta_x) {
                gui()->select_tile(RED, 0);
            } else if (red == 2 and y >= 0 and y <= mDelta_y and
                       x >= mDelta_x + 5 and x <= 2 * mDelta_x + 5) {
                gui()->select_tile(RED, 1);
            }
        }
    } else {
        int yellow = gui()->available_yellow_tile_number();

        if (yellow > 0) {
            int x = event->x - 5;
            int y = event->y - 5;

            if (y >= 0 and y <= mDelta_y and
                x >= 0 and x <= mDelta_x) {
                gui()->select_tile(YELLOW, 0);
            } else if (yellow == 2 and y >= 0 and y <= mDelta_y and
                       x >= mDelta_x + 5 and x <= 2 * mDelta_x + 5) {
                gui()->select_tile(YELLOW, 1);
            }
        }
    }
    gui()->redrawState();
    gui()->redrawDrawingArea();
    return true;
}

}} // namespace openxum invers
