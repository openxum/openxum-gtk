/*
 * @file gui/invers/state.hpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _GUI_INVERS_STATE_HPP
#define _GUI_INVERS_STATE_HPP 1

#include <gui/state.hpp>
#include <invers/intersection.hpp>

namespace openxum { namespace invers {

class StateView : public openxum::StateView
{
public:
    StateView(openxum::Gui* parent, common::Color player);
    virtual ~StateView()
    { }

    bool on_button_release_event(GdkEventButton* event);

protected:
    virtual void draw();

private:
    void draw_tiles();

    invers::Gui* gui() const
    { return (dynamic_cast < invers::Gui* >(parent()->specific())); }

    common::Color mPlayer;
    double mDelta_x;
    double mDelta_y;
};

}} // namespace openxum invers

#endif
