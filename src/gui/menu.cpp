/*
 * @file gui/menu.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gui/menu.hpp>
#include <gui/gui.hpp>

#include <iostream>

namespace openxum {

const Glib::ustring Menu::UI_DEFINITION =
    "<ui>"
    "    <menubar name='MenuBar'>"
    "        <menu action='MenuGame'>"
    "            <menuitem action='NewGame'/>"
    "            <menuitem action='Quit'/>"
    "        </menu>"
    "        <menu action='MenuOptions'>"
    "            <menuitem action='Preferences' />"
    "        </menu>"
    "        <menu action='MenuHelp'>"
    "            <menuitem action='About' />"
    "        </menu>"
    "    </menubar>"
    "</ui>";

Menu::Menu(Gui* parent) :
    mParent(parent)
{
    mActionGroup = Gtk::ActionGroup::create();
    mUIManager = Gtk::UIManager::create();
    init();
}

void Menu::createGameActions()
{
    mActionGroup->add(Gtk::Action::create("MenuGame", "_Game"));

    mActionGroup->add(
        Gtk::Action::create("NewGame", Gtk::Stock::DIRECTORY,
                            "New Game", "Create a new game"),
        Gtk::AccelKey("<control>n"),
        sigc::mem_fun(mParent, &Gui::onNew));
    mActionGroup->add(
        Gtk::Action::create("Quit", Gtk::Stock::QUIT,
                            "_Quit", "Quit OpenXum"),
        sigc::mem_fun(mParent, &Gui::onQuit));
}

void Menu::createHelpActions()
{
    mActionGroup->add(Gtk::Action::create("MenuHelp", "_Help"));

    mActionGroup->add(
        Gtk::Action::create("About", Gtk::Stock::ABOUT, "About", "About"),
        sigc::mem_fun(mParent, &Gui::onAbout));
}

void Menu::createOptionsActions()
{
    mActionGroup->add(Gtk::Action::create("MenuOptions", "_Options"));

    mActionGroup->add(
        Gtk::Action::create("Preferences", Gtk::Stock::PREFERENCES,
                            "Preferences", "Preferences"),
        sigc::mem_fun(mParent, &Gui::onPreferences));
}

void Menu::createUI()
{
#ifdef GLIBMM_EXCEPTIONS_ENABLED
    try {
        mUIManager->add_ui_from_string(UI_DEFINITION);
    } catch(const Glib::Error& ex) {
        std::cerr << "building menus failed: " <<  ex.what();
    }
#else
    std::auto_ptr<Glib::Error> ex;

    mUIManager->add_ui_from_string(UI_DEFINITION, ex);
    if(ex.get()) {
        std::cerr << "building menus failed: " <<  ex->what();
    }
#endif //GLIBMM_EXCEPTIONS_ENABLED
}

void Menu::init()
{
    createGameActions();
    createOptionsActions();
    createHelpActions();

    mUIManager->insert_action_group(mActionGroup);
    mParent->add_accel_group(mUIManager->get_accel_group());

    createUI();

    mMenuBar = dynamic_cast < Gtk::MenuBar* >(
        mUIManager->get_widget("/MenuBar"));
}

} // namespace openxum
