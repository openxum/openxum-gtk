/*
 * @file gui/menu.hpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _GUI_MENU_HPP
#define _GUI_MENU_HPP 1

#include <gtkmm.h>
#include <libglademm/xml.h>

namespace openxum {

class Gui;

class Menu
{
public:
    static const Glib::ustring UI_DEFINITION;

    Menu(Gui* parent);
    virtual ~Menu()
    { }

    Gtk::MenuBar& getMenuBar() const
    { return *mMenuBar; }

private:
    void createGameActions();
    void createOptionsActions();
    void createHelpActions();
    void createUI();
    void init();
    void makeButtonsTools();

    Gui* mParent;

    Gtk::MenuBar* mMenuBar;

    Glib::RefPtr < Gtk::UIManager > mUIManager;
    Glib::RefPtr < Gtk::ActionGroup > mActionGroup;
};

} // namespace openxum

#endif
