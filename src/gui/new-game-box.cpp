/*
 * @file gui/new-game-box.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gui/new-game-box.hpp>

namespace openxum {

NewGameBox::NewGameBox(Glib::RefPtr<Gnome::Glade::Xml> xml):
    mXml(xml), mGame(YINSH)
{
    xml->get_widget("DialogNewGame", mDialog);

    xml->get_widget("DvonnRadioButton", mDvonnRadioButton);
    xml->get_widget("GipfRadioButton", mGipfRadioButton);
    xml->get_widget("InversRadioButton", mInversRadioButton);
    xml->get_widget("PunctRadioButton", mPunctRadioButton);
    xml->get_widget("TzaarRadioButton", mTzaarRadioButton);
    xml->get_widget("YinshRadioButton", mYinshRadioButton);
    xml->get_widget("ZertzRadioButton", mZertzRadioButton);

    // Action area
    xml->get_widget("ButtonNewGameOk", mButtonOk);
    mList.push_back(mButtonOk->signal_clicked().connect(
                        sigc::mem_fun(*this, &NewGameBox::onOk)));
    xml->get_widget("ButtonNewGameCancel", mButtonCancel);
    mList.push_back(mButtonCancel->signal_clicked().connect(
                        sigc::mem_fun(*this, &NewGameBox::onCancel)));
}

NewGameBox::~NewGameBox()
{
    for (std::list < sigc::connection >::iterator it = mList.begin();
         it != mList.end(); ++it) {
        it->disconnect();
    }
    mDialog->hide_all();
}

int NewGameBox::run()
{
    mDialog->show_all();
    return mDialog->run();
}

// Actions
void NewGameBox::onOk()
{
    if (mDvonnRadioButton->get_active()) {
        mGame = DVONN;
    } else if (mGipfRadioButton->get_active()) {
        mGame = GIPF;
    } else if (mInversRadioButton->get_active()) {
        mGame = INVERS;
    } else if (mPunctRadioButton->get_active()) {
        mGame = PUNCT;
    } else if (mTzaarRadioButton->get_active()) {
        mGame = TZAAR;
    } else if (mYinshRadioButton->get_active()) {
        mGame = YINSH;
    } else if (mZertzRadioButton->get_active()) {
        mGame = ZERTZ;
    }
    mDialog->hide_all();
    mDialog->response(Gtk::RESPONSE_OK);
}

void NewGameBox::onCancel()
{
    mDialog->hide_all();
    mDialog->response(Gtk::RESPONSE_CANCEL);
}

} //namespace openxum
