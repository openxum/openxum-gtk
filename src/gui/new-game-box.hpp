/*
 * @file gui/new-game-box.hpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _GUI_NEW_GAME_BOX_HPP
#define _GUI_NEW_GAME_BOX_HPP 1

#include <gtkmm.h>
#include <libglademm.h>
#include <gui/settings.hpp>

namespace openxum {

class NewGameBox
{
public:
    enum Game { DVONN, GIPF, INVERS, PUNCT, TZAAR, YINSH, ZERTZ };

    NewGameBox(Glib::RefPtr<Gnome::Glade::Xml> xml);
    ~NewGameBox();

    Game game() const
    { return mGame; }

    int run();

protected:
    // Actions
    void onOk();
    void onCancel();

private:
    Glib::RefPtr<Gnome::Glade::Xml> mXml;
    Gtk::Dialog* mDialog;
    std::list < sigc::connection > mList;

    //Dialog widgets - Action
    Gtk::Button* mButtonOk;
    Gtk::Button* mButtonCancel;

    //Dialog widgets
    Gtk::RadioButton* mDvonnRadioButton;
    Gtk::RadioButton* mGipfRadioButton;
    Gtk::RadioButton* mInversRadioButton;
    Gtk::RadioButton* mPunctRadioButton;
    Gtk::RadioButton* mTzaarRadioButton;
    Gtk::RadioButton* mYinshRadioButton;
    Gtk::RadioButton* mZertzRadioButton;

    Game mGame;
};

} //namespace openxum

#endif
