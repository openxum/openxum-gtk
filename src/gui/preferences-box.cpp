/*
 * @file gui/preferences-box.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gui/preferences-box.hpp>

using namespace openxum::common;

namespace openxum {

PreferencesBox::PreferencesBox(Glib::RefPtr<Gnome::Glade::Xml> xml):
    mXml(xml)
{
    xml->get_widget("DialogPreferences", mDialog);

    // Game tab
    xml->get_widget("InversRedRadioButton", mInversRedRadioButton);
    mList.push_back(mInversRedRadioButton->signal_clicked().connect(
                        sigc::mem_fun(*this, &PreferencesBox::onInversColor)));
    xml->get_widget("InversYellowRadioButton", mInversYellowRadioButton);
    mList.push_back(mInversYellowRadioButton->signal_clicked().connect(
                        sigc::mem_fun(*this, &PreferencesBox::onInversColor)));

    xml->get_widget("YinshRegularRadioButton", mYinshRegularRadioButton);
    mList.push_back(mYinshRegularRadioButton->signal_clicked().connect(
                        sigc::mem_fun(*this, &PreferencesBox::onYinshType)));
    xml->get_widget("YinshBlitzRadioButton", mYinshBlitzRadioButton);
    mList.push_back(mYinshBlitzRadioButton->signal_clicked().connect(
                        sigc::mem_fun(*this, &PreferencesBox::onYinshType)));

    xml->get_widget("YinshBlackRadioButton", mYinshBlackRadioButton);
    mList.push_back(mYinshBlackRadioButton->signal_clicked().connect(
                        sigc::mem_fun(*this, &PreferencesBox::onYinshColor)));
    xml->get_widget("YinshWhiteRadioButton", mYinshWhiteRadioButton);
    mList.push_back(mYinshWhiteRadioButton->signal_clicked().connect(
                        sigc::mem_fun(*this, &PreferencesBox::onYinshColor)));

    xml->get_widget("YinshLevelScale", mYinshLevelScale);
    mList.push_back(mYinshLevelScale->signal_value_changed().connect(
                        sigc::mem_fun(*this, &PreferencesBox::onYinshLevel)));

    xml->get_widget("ZertzRegularRadioButton", mZertzRegularRadioButton);
    mList.push_back(mZertzRegularRadioButton->signal_clicked().connect(
                        sigc::mem_fun(*this, &PreferencesBox::onZertzType)));
    xml->get_widget("ZertzBlitzRadioButton", mZertzBlitzRadioButton);
    mList.push_back(mZertzBlitzRadioButton->signal_clicked().connect(
                        sigc::mem_fun(*this, &PreferencesBox::onZertzType)));
    xml->get_widget("ZertzBeginCheckButton", mZertzBeginCheckButton);
    mList.push_back(mZertzBeginCheckButton->signal_clicked().connect(
                        sigc::mem_fun(*this, &PreferencesBox::onZertzBegin)));

    // Network tab
    xml->get_widget("ServerAddressEntry", mServerAddressEntry);
    mList.push_back(mServerAddressEntry->signal_changed().connect(
                        sigc::mem_fun(*this,
                                      &PreferencesBox::onServerAddress)));
    xml->get_widget("LoginEntry", mLoginEntry);
    mList.push_back(mLoginEntry->signal_changed().connect(
                        sigc::mem_fun(*this,
                                      &PreferencesBox::onLogin)));
    xml->get_widget("PasswordEntry", mPasswordEntry);
    mList.push_back(mPasswordEntry->signal_changed().connect(
                        sigc::mem_fun(*this,
                                      &PreferencesBox::onPassword)));

    xml->get_widget("RemotePlayerCheckButton", mRemotePlayerCheckButton);
    mList.push_back(mRemotePlayerCheckButton->signal_clicked().connect(
                        sigc::mem_fun(*this, &PreferencesBox::onRemotePlayer)));

    // Action area
    xml->get_widget("ButtonPreferencesApply", mButtonApply);
    mList.push_back(mButtonApply->signal_clicked().connect(
                        sigc::mem_fun(*this, &PreferencesBox::onApply)));
    xml->get_widget("ButtonPreferencesCancel", mButtonCancel);
    mList.push_back(mButtonCancel->signal_clicked().connect(
                        sigc::mem_fun(*this, &PreferencesBox::onCancel)));
    xml->get_widget("ButtonPreferencesRestore", mButtonRestore);
    mList.push_back(mButtonRestore->signal_clicked().connect(
                        sigc::mem_fun(*this, &PreferencesBox::onRestore)));
}

PreferencesBox::~PreferencesBox()
{
    for (std::list < sigc::connection >::iterator it = mList.begin();
         it != mList.end(); ++it) {
        it->disconnect();
    }
    mDialog->hide_all();
}

int PreferencesBox::run()
{
    init();
    mDialog->show_all();
    return mDialog->run();
}

// Actions
void PreferencesBox::onApply()
{
    saveSettings();
    mDialog->hide_all();
    mDialog->response(Gtk::RESPONSE_OK);
}

void PreferencesBox::onCancel()
{
    mDialog->hide_all();
    mDialog->response(Gtk::RESPONSE_CANCEL);
}

void PreferencesBox::onRestore()
{
    mCurrentSettings.setDefault();
    activate();
}

// Game

void PreferencesBox::onInversColor()
{
    mCurrentSettings.setInversColor(
        mInversRedRadioButton->get_active() ? RED : YELLOW);
}

void PreferencesBox::onYinshColor()
{
    mCurrentSettings.setYinshColor(
        mYinshBlackRadioButton->get_active() ? BLACK : WHITE);
}

void PreferencesBox::onYinshLevel()
{
    mCurrentSettings.setYinshLevelNumber(
        (int)mYinshLevelScale->get_value());
}

void PreferencesBox::onYinshType()
{
    mCurrentSettings.setYinshType(
        mYinshRegularRadioButton->get_active() ? yinsh::REGULAR : yinsh::BLITZ);
}

void PreferencesBox::onZertzType()
{
    mCurrentSettings.setZertzType(
        mZertzRegularRadioButton->get_active() ? zertz::REGULAR : zertz::BLITZ);
}

void PreferencesBox::onZertzBegin()
{
    mCurrentSettings.setZertzNumber(
        mZertzBeginCheckButton->get_active() ? zertz::ONE : zertz::TWO);
}

// Network

void PreferencesBox::onServerAddress()
{
    mCurrentSettings.setServerAddress(mServerAddressEntry->get_text());
}

void PreferencesBox::onLogin()
{
    mCurrentSettings.setLogin(mLoginEntry->get_text());
}

void PreferencesBox::onPassword()
{
    mCurrentSettings.setPassword(mPasswordEntry->get_text());
}

void PreferencesBox::onRemotePlayer()
{
    mCurrentSettings.setRemotePlayer(mRemotePlayerCheckButton->get_active());
}

// private
void PreferencesBox::init()
{
    loadSettings();
    activate();
}

void PreferencesBox::activate()
{
    // Game
    if (mCurrentSettings.getInversColor() == RED) {
        mInversRedRadioButton->set_active(true);
    } else {
        mInversYellowRadioButton->set_active(true);
    }
    if (mCurrentSettings.getYinshColor() == BLACK) {
        mYinshBlackRadioButton->set_active(true);
    } else {
        mYinshWhiteRadioButton->set_active(true);
    }
    mYinshLevelScale->set_value(mCurrentSettings.getYinshLevelNumber());
    if (mCurrentSettings.getYinshType() == yinsh::REGULAR) {
        mYinshRegularRadioButton->set_active(true);
    } else {
        mYinshBlitzRadioButton->set_active(true);
    }
    if (mCurrentSettings.getZertzType() == zertz::REGULAR) {
        mZertzRegularRadioButton->set_active(true);
    } else {
        mZertzBlitzRadioButton->set_active(true);
    }
    mZertzBeginCheckButton->set_active(
        mCurrentSettings.getZertzNumber() == zertz::ONE);

    // Network
    mServerAddressEntry->set_text(mCurrentSettings.getServerAddress());
    mLoginEntry->set_text(mCurrentSettings.getLogin());
    mPasswordEntry->set_text(mCurrentSettings.getPassword());
    mRemotePlayerCheckButton->set_active(mCurrentSettings.getRemotePlayer());
}

void PreferencesBox::saveSettings()
{
    Settings::settings() = mCurrentSettings;
    Settings::settings().save();
}

void PreferencesBox::loadSettings()
{
    Settings::settings().load();
    mCurrentSettings = Settings::settings();
}

} //namespace openxum
