/*
 * @file gui/preferences-box.hpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _GUI_PREFERENCES_BOX_HPP
#define _GUI_PREFERENCES_BOX_HPP 1

#include <gtkmm.h>
#include <libglademm.h>
#include <gui/settings.hpp>

namespace openxum {

class PreferencesBox
{
public:
    PreferencesBox(Glib::RefPtr < Gnome::Glade::Xml > xml);
    virtual ~PreferencesBox();

    int run();

protected:
    // Actions
    void onApply();
    void onCancel();
    void onRestore();

    void onInversColor();
    void onYinshColor();
    void onYinshLevel();
    void onYinshType();
    void onZertzType();
    void onZertzBegin();

    // Game

    // Network
    void onServerAddress();
    void onLogin();
    void onPassword();
    void onRemotePlayer();

private:
    void init();
    void activate();
    void saveSettings();
    void loadSettings();

    Glib::RefPtr < Gnome::Glade::Xml > mXml;
    Gtk::Dialog* mDialog;
    Settings mCurrentSettings;

    std::list < sigc::connection > mList;

    //Dialog widgets - Action
    Gtk::Button* mButtonApply;
    Gtk::Button* mButtonCancel;
    Gtk::Button* mButtonRestore;

    //Dialog widgets - Game
    // invers
    Gtk::RadioButton* mInversRedRadioButton;
    Gtk::RadioButton* mInversYellowRadioButton;

    // yinsh
    Gtk::RadioButton* mYinshBlackRadioButton;
    Gtk::RadioButton* mYinshWhiteRadioButton;
    Gtk::RadioButton* mYinshRegularRadioButton;
    Gtk::RadioButton* mYinshBlitzRadioButton;
    Gtk::HScale* mYinshLevelScale;

    // zertz
    Gtk::RadioButton* mZertzRegularRadioButton;
    Gtk::RadioButton* mZertzBlitzRadioButton;
    Gtk::CheckButton* mZertzBeginCheckButton;

    //Dialog widgets - Network
    Gtk::Entry* mServerAddressEntry;
    Gtk::Entry* mLoginEntry;
    Gtk::Entry* mPasswordEntry;
    Gtk::CheckButton* mRemotePlayerCheckButton;
};

} //namespace openxum

#endif
