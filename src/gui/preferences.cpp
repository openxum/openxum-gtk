/*
 * @file gui/preferences.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glibmm.h>
#include <giomm.h>
#include <gui/exception.hpp>
#include <gui/preferences.hpp>

namespace openxum {

Preferences::Preferences(const std::string& file)
{
    std::stringstream path;

    path << Glib::get_home_dir() << "/.openxum/" << file;
    m_filepath = path.str();
}

Preferences::Preferences(const Preferences& pref)
    : m_filepath(pref.m_filepath)
{
}

void Preferences::load()
{
    std::string line, section, key, value;

    try {
        m_file.open(m_filepath.c_str(), std::ios::in);
    } catch (const std::exception& e) {
        throw FileError("Preferences: cannot open file for reading.");
    }

    while (getline(m_file, line)) {
        if (line[0] != '#') {
            if (line[0] == '[') {
                section = line.substr(1, line.find("]") -1);
            } else {
                key = line.substr(0, line.find("="));
                value = line.substr(line.find("=") + 1, line.length() -1);
                if (!section.empty() and !key.empty() and !value.empty())
                    setAttributes(section, key, value);
            }
        }
    }
    m_file.close();
}

void Preferences::save()
{
    std::stringstream path;

    path << Glib::get_home_dir() << "/.openxum";

    Glib::RefPtr < Gio::File > dir = Gio::File::create_for_path(path.str());
    if (not dir->query_exists ()) {
        dir->make_directory();
    }

    try {
        m_file.open(m_filepath.c_str(), std::ios::out | std::ios::trunc);
    } catch (const std::exception& e) {
        throw FileError("Preferences: cannot open file for writing.");
    }

    m_file << "# OpenXum config file" << std::endl << std::endl;
    for (Settings::const_iterator it = m_settings.begin();
         it != m_settings.end(); ++it) {
        m_file << "\n[" << it->first << "]" << std::endl;
        for (KeyValue::const_iterator jt = it->second.begin();
             jt != it->second.end(); ++jt) {
            m_file << jt->first << "=" << jt->second << std::endl;
        }
    }
    m_file.close();
}

void Preferences::assign(const std::string& section, const std::string& key,
                         std::string& value) const
{
    Settings::const_iterator it = m_settings.find(section);
    if (it != m_settings.end()) {
        KeyValue::const_iterator jt = it->second.find(key);
        if (jt != it->second.end()) {
            value = jt->second;
        }
    }
}

const KeyValue& Preferences::getKeyValues(const std::string& section) const
{
    Settings::const_iterator it = m_settings.find(section);

    if (it == m_settings.end()) {
        throw ArgError("Preferences: section is empty.");
    }

    return it->second;
}

std::string Preferences::getAttributes(const std::string& section,
                                       const std::string& key)
{
    return m_settings[section][key];
}

void Preferences::setAttributes(const std::string& section,
                                const std::string& key,
                                const std::string& value)
{
    m_settings[section][key] = value;
}

} //namespace openxum
