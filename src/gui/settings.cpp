/*
 * @file gui/settings.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gui/settings.hpp>
#include <stdlib.h>
#include <sstream>

using namespace openxum::common;

namespace openxum {

Settings* Settings::mSettings = 0;

void GameSettings::setDefault()
{
    mInversColor = RED;
    mYinshType = yinsh::REGULAR;
    mYinshLevelNumber = 3;
    mYinshColor = BLACK;
    mZertzType = zertz::REGULAR;
    mZertzNumber = zertz::ONE;
}

void GameSettings::load(Preferences& prefs)
{
    std::string value;

    setDefault();

    value = prefs.getAttributes("invers", "color");
    if (not value.empty()) {
        mInversColor = (Color)atoi(value.c_str());
    }

    value = prefs.getAttributes("yinsh", "type");
    if (not value.empty()) {
        mYinshType = (yinsh::Type)atoi(value.c_str());
    }
    value = prefs.getAttributes("yinsh", "level");
    if (not value.empty()) {
        mYinshLevelNumber = atoi(value.c_str());
    }
    value = prefs.getAttributes("yinsh", "color");
    if (not value.empty()) {
        mYinshColor = (Color)atoi(value.c_str());
    }

    value = prefs.getAttributes("zertz", "type");
    if (not value.empty()) {
        mZertzType = (zertz::Type)atoi(value.c_str());
    }
    value = prefs.getAttributes("zertz", "number");
    if (not value.empty()) {
        mZertzNumber = (zertz::Number)atoi(value.c_str());
    }
}

void GameSettings::operator=(const GameSettings& src)
{
    mInversColor = src.mInversColor;
    mYinshType = src.mYinshType;
    mYinshLevelNumber = src.mYinshLevelNumber;
    mYinshColor = src.mYinshColor;
    mZertzType = src.mZertzType;
    mZertzNumber = src.mZertzNumber;
}

void GameSettings::save(Preferences& prefs)
{
    prefs.setAttributes("invers", "color",
                        mInversColor == RED ? "3" : "4");

    char level[2];
    sprintf(level, "%d", mYinshLevelNumber);

    prefs.setAttributes("yinsh", "type",
                        mYinshType == yinsh::REGULAR ? "0" : "1");
    prefs.setAttributes("yinsh", "level", level);
    prefs.setAttributes("yinsh", "color",
                        mYinshColor == BLACK ? "0" : "1");

    prefs.setAttributes("zertz", "type",
                        mZertzType == zertz::REGULAR ? "0" : "1");
    prefs.setAttributes("zertz", "number",
                        mZertzNumber == zertz::ONE ? "0" : "1");
}

void NetworkSettings::setDefault()
{
    mServerAddress = "http://127.0.0.1";
    mLogin = "";
    mPassword ="";
    mRemotePlayer = false;
}

void NetworkSettings::load(Preferences& prefs)
{
    std::string value;

    setDefault();

    value = prefs.getAttributes("network", "server_address");
    if (not value.empty()) {
        mServerAddress = value;
    }

    value = prefs.getAttributes("network", "login");
    if (not value.empty()) {
        mLogin = value;
    }

    value = prefs.getAttributes("network", "password");
    if (not value.empty()) {
        mPassword = value;
    }

    value = prefs.getAttributes("network", "remote_player");
    if (not value.empty()) {
        mRemotePlayer = (bool)atoi(value.c_str());
    }
}

void NetworkSettings::operator=(const NetworkSettings& src)
{
    mServerAddress = src.mServerAddress;
    mLogin = src.mLogin;
    mPassword = src.mPassword;
    mRemotePlayer = src.mRemotePlayer;
}

void NetworkSettings::save(Preferences& prefs)
{
    prefs.setAttributes("network", "server_address", mServerAddress);
    prefs.setAttributes("network", "login", mLogin);
    prefs.setAttributes("network", "password", mPassword);
    prefs.setAttributes("network", "remote_player",
                        mRemotePlayer ? "1" : "0");
}

void Settings::load()
{
    Preferences prefs;
    prefs.load();

    GameSettings::load(prefs);
    NetworkSettings::load(prefs);
}

void Settings::operator=(const Settings& src)
{
    GameSettings::operator=(src);
    NetworkSettings::operator=(src);
}

void Settings::save()
{
    Preferences prefs;
    prefs.load();

    GameSettings::save(prefs);
    NetworkSettings::save(prefs);
    prefs.save();
}

void Settings::setDefault()
{
    GameSettings::setDefault();
    NetworkSettings::setDefault();
}

} // namespace openxum
