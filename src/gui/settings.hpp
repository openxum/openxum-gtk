/*
 * @file gui/settings.hpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _GUI_SETTINGS_HPP
#define _GUI_SETTINGS_HPP 1

#include <gui/preferences.hpp>
#include <dvonn/board.hpp>
#include <gipf/board.hpp>
#include <invers/board.hpp>
#include <tzaar/board.hpp>
#include <yinsh/board.hpp>
#include <zertz/board.hpp>

namespace openxum {

class Preferences;

class GameSettings
{
public:
    GameSettings()
    {}

    void setDefault();
    void load(Preferences& prefs);
    virtual void operator=(const GameSettings& src);
    void save(Preferences& prefs);

    common::Color getInversColor() const
    { return mInversColor; }

    common::Color getYinshColor() const
    { return mYinshColor; }

    int getYinshLevelNumber() const
    { return mYinshLevelNumber; }

    yinsh::Type getYinshType() const
    { return mYinshType; }

    zertz::Number getZertzNumber() const
    { return mZertzNumber; }

    zertz::Type getZertzType() const
    { return mZertzType; }

    void setInversColor(common::Color color)
    { mInversColor = color; }

    void setYinshColor(common::Color color)
    { mYinshColor = color; }

    void setYinshLevelNumber(int number)
    { mYinshLevelNumber = number; }

    void setYinshType(yinsh::Type type)
    { mYinshType = type; }

    void setZertzNumber(zertz::Number number)
    { mZertzNumber = number; }

    void setZertzType(zertz::Type type)
    { mZertzType = type; }

private:
    common::Color mInversColor;
    yinsh::Type mYinshType;
    int mYinshLevelNumber;
    common::Color mYinshColor;
    zertz::Number mZertzNumber;
    zertz::Type mZertzType;
};

class NetworkSettings
{
public:
    NetworkSettings()
    {}

    void setDefault();
    void load(Preferences& prefs);
    virtual void operator=(const NetworkSettings& src);
    void save(Preferences& prefs);

    std::string getServerAddress() const
    { return mServerAddress; }

    std::string getLogin() const
    { return mLogin; }

    std::string getPassword() const
    { return mPassword; }

    bool getRemotePlayer() const
    { return mRemotePlayer; }

    void setServerAddress(const std::string& serverAddress)
    { mServerAddress = serverAddress; }

    void setLogin(const std::string& login)
    { mLogin = login; }

    void setPassword(const std::string& password)
    { mPassword = password; }

    void setRemotePlayer(bool remotePlayer)
    { mRemotePlayer = remotePlayer; }

private:
    std::string mServerAddress;
    std::string mLogin;
    std::string mPassword;
    bool mRemotePlayer;
};

class Settings : public GameSettings, public NetworkSettings
{
public:
    Settings()
    {}

    void setDefault();

    void load();

    virtual void operator=(const Settings& src);

    void save();

    static Settings& settings()
    { if (mSettings == 0) mSettings = new Settings; return *mSettings; }

    void kill()
    { if (mSettings) delete mSettings; }

private:
    static Settings* mSettings;
};

} // namespace openxum

#endif
