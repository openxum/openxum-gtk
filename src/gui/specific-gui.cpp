/*
 * @file gui/specific-gui.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gui/specific-gui.hpp>

namespace openxum {

void SpecificGui::add_turn(const std::string& message)
{
    mGui->add_turn(message);
}

DrawingArea* SpecificGui::drawing_area() const
{
    return mGui->drawing_area();
}

void SpecificGui::push_message(const std::string& message)
{
    mGui->push_message(message);
}

void SpecificGui::redrawDrawingArea()
{
    mGui->redrawDrawingArea();
}

void SpecificGui::redrawState()
{
    mGui->redrawState();
}

} // namespace gui
