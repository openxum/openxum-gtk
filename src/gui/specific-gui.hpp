/*
 * @file gui/specific-gui.hpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _GUI_SPECIFIC_GUI_HPP
#define _GUI_SPECIFIC_GUI_HPP 1

#include <gui/gui.hpp>
#include <gui/drawing-area.hpp>

namespace openxum {

class DrawingArea;
class Gui;

class SpecificGui
{
public:
    SpecificGui(Gui* gui) : mGui(gui)
    { }

    virtual ~SpecificGui()
    { }

    void add_turn(const std::string& message);

    DrawingArea* drawing_area() const;

    virtual bool exist_intersection(char letter, int number) =0;
    virtual void finish() = 0;

    Gui* gui() const
    { return mGui; }

    virtual void new_game() =0;

    virtual int phase() const =0;

    void push_message(const std::string& message);

    void redrawDrawingArea();

    void redrawState();

private:
    Gui* mGui;
};

} // namespace gui

#endif
