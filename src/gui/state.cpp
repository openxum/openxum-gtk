/*
 * @file gui/state.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gui/state.hpp>

namespace openxum {

StateView::StateView(Gui* parent) :
    mParent(parent), mHeight(50), mWidth(250), mBuffer(0), mIsRealized(false)
{
    set_events(Gdk::POINTER_MOTION_MASK | Gdk::BUTTON_MOTION_MASK |
               Gdk::BUTTON1_MOTION_MASK | Gdk::BUTTON2_MOTION_MASK |
               Gdk::BUTTON3_MOTION_MASK | Gdk::BUTTON_PRESS_MASK |
               Gdk::BUTTON_RELEASE_MASK);
}

bool StateView::on_configure_event(GdkEventConfigure* event)
{
    mWidth = 250;
    mHeight = 50;
    if (mIsRealized) {
        set_size_request(mWidth, mHeight);
        mBuffer = Gdk::Pixmap::create(mWin, mWidth, mHeight, -1);
        queueRedraw();
    }
    return true;
}

bool StateView::on_expose_event(GdkEventExpose*)
{
    if (mIsRealized) {
        if (!mBuffer) {
            set_size_request(mWidth, mHeight);
            mBuffer = Gdk::Pixmap::create(mWin, mWidth, mHeight, -1);
        }
        if (mBuffer) {
            if (mNeedRedraw) {
                mContext = mBuffer->create_cairo_context();
                draw();
                mNeedRedraw = false;
            }
            mWin->draw_drawable(mWingc, mBuffer, 0, 0, 0, 0, -1, -1);
        }
    }
    return true;
}

void StateView::on_realize()
{
    Gtk::DrawingArea::on_realize();
    mWin = get_window();
    mWingc = Gdk::GC::create(mWin);
    mIsRealized = true;
    queueRedraw();
}

} // namespace openxum
