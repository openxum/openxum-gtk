/*
 * @file gui/timer.hpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _GUI_TIMER_HPP
#define _GUI_TIMER_HPP 1

#include <gtkmm.h>

#include <gui/gui.hpp>

namespace openxum {

class Gui;

class Timer : public Gtk::DrawingArea
{
public:
    Timer(Gui* parent);
    virtual ~Timer()
    { }

    bool on_configure_event(GdkEventConfigure* event);
    bool on_expose_event(GdkEventExpose*);
    void on_realize();
    void queueRedraw()
    { mNeedRedraw = true; queue_draw(); }

private:
    void draw();

    Gui* mParent;

    int mHeight;
    int mWidth;
    Glib::RefPtr < Gdk::Pixmap > mBuffer;
    Cairo::RefPtr < Cairo::Context > mContext;
    Glib::RefPtr < Gdk::Window > mWin;
    Glib::RefPtr < Gdk::GC > mWingc;
    bool mIsRealized;
    bool mNeedRedraw;
};

} // namespace openxum

#endif
