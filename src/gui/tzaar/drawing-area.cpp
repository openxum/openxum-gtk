/*
 * @file gui/tzaar/drawing-area.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gui/tzaar/drawing-area.hpp>
#include <gui/utils/graphics.hpp>
#include <tzaar/graphics.hpp>
#include <tzaar/board.hpp>
#include <tzaar/coordinates.hpp>
#include <tzaar/intersection.hpp>

#include <iostream>

using namespace openxum::common;

namespace openxum { namespace tzaar {

DrawingArea::DrawingArea(openxum::Gui* parent) :
    openxum::DrawingArea(parent), mSelectedPiece('X', 0),
    mSelectedCoordinates('X', 0), mSelectedCapture(false),
    mSelectedMakeStack(false), mSelectedPass(false)
{
}

void DrawingArea::computeCoordinates(char letter, int number,
                                     int& x, int& y)
{
    x = mOffset + mDelta_x * (letter - 'A');
    y = mOffset + 7 * mDelta_y + mDelta_xy * (letter - 'A') -
        (number - 1) * mDelta_y;
}

void DrawingArea::compute_deltas()
{
    mDelta_x = (mHeight - 2 * mOffset) / 9.;
    mDelta_y = mDelta_x;
    mDelta_xy = mDelta_y / 2;
    mOffset = mDelta_x / 2;
}

void DrawingArea::draw()
{
    mContext->set_line_width(1.);

    // background
    mContext->set_source_rgb(1., 1., 1.);
    mContext->rectangle(0, 0, mWidth, mHeight);
    mContext->fill();
    mContext->stroke();

    // grid
    draw_grid();
    draw_coordinates();

    //state
    draw_state();

    // show intersection
    show_intersection();

    if (parent()->phase() == Gui::CHOOSE) {
        draw_choice();
    }

    if ((parent()->phase() == Gui::FIRST_MOVE or
         parent()->phase() == Gui::CAPTURE or
         parent()->phase() == Gui::SECOND_CAPTURE) and
        mSelectedPiece.is_valid()) {
        draw_possible_capture();
    }

    if (parent()->phase() == Gui::MAKE_STRONGER and
        mSelectedPiece.is_valid()) {
        draw_possible_make_stack();
    }
}

void DrawingArea::draw_button(int x, int y, const std::string& text,
                              bool select)
{
    const int height = 25;
    const int width = 100;

    if (select) {
        mContext->set_source_rgb(1., 0., 0.);
    } else {
        mContext->set_source_rgb(0.8, 0.8, 0.8);
    }
    mContext->rectangle(x, y, width, height);
    mContext->fill();
    mContext->stroke();

    mContext->set_source_rgb(0., 0., 0.);

    Pango::FontDescription font;

    font.set_family("Times");
    font.set_weight(Pango::WEIGHT_BOLD);

    Glib::RefPtr < Pango::Layout > layout =
        create_pango_layout(text);

    layout->set_font_description(font);

    int text_width;
    int text_height;

    layout->get_pixel_size(text_width, text_height);

    mContext->set_source_rgb(0., 0., 0.);
    mContext->move_to(x + (width - text_width) / 2,
                      y + (height - text_height) / 2);

    layout->show_in_cairo_context(mContext);
    mContext->stroke();

    mContext->set_source_rgb(0., 0., 0.);
    mContext->rectangle(x, y, width, height);
    mContext->stroke();
}

void DrawingArea::draw_choice()
{
    draw_button(mOffset, mOffset, "capture", mSelectedCapture);
    draw_button(mOffset + 150, mOffset, "make stack", mSelectedMakeStack);
    draw_button(mOffset + 300, mOffset, "pass", mSelectedPass);
}

void DrawingArea::draw_coordinates()
{
    char text[2];

    text[1] = 0;

    mContext->set_font_size(16);

    // letters
    for (char l = 'A'; l < 'J'; ++l) {
        int _x, _y;

        text[0] = l;
        computeCoordinates(l, tzaar::Board::begin_number[l - 'A'], _x, _y);
        _x -= 5;
        _y += 20;

        mContext->move_to(_x, _y);
        mContext->show_text(text);
    }

    // numbers
    for (int n = 1; n < 10; ++n) {
        int _x, _y;

        text[0] = '0' + n;
        computeCoordinates(tzaar::Board::begin_letter[n - 1], n, _x, _y);
        _x -= 15 + (n > 9 ? 5 : 0);
        _y -= 3;

        mContext->move_to(_x, _y);
        mContext->show_text(text);
    }
    mContext->stroke();
}

void DrawingArea::draw_grid()
{
    if (parent()->phase() == Gui::IDLE) {
        Gdk::Color color("grey");

        mContext->set_source_rgb(color.get_red_p(),
                                 color.get_green_p(),
                                 color.get_blue_p());
    } else {
        mContext->set_source_rgb(0.2, 0.2, 0.2);
    }

    for (char l = 'A'; l < 'J'; ++l) {
        int _x1, _x2, _y1, _y2;

        computeCoordinates(l, tzaar::Board::begin_number[l - 'A'], _x1, _y1);
        computeCoordinates(l, tzaar::Board::end_number[l - 'A'], _x2, _y2);
        if (l == 'E') {
            int _x3, _y3, _x4, _y4;

            computeCoordinates('E', 4, _x3, _y3);
            computeCoordinates('E', 6, _x4, _y4);
            mContext->move_to(_x1, _y1);
            mContext->line_to(_x3, _y3);
            mContext->move_to(_x4, _y4);
            mContext->line_to(_x2, _y2);
        } else {
            mContext->move_to(_x1, _y1);
            mContext->line_to(_x2, _y2);
        }
    }

    for (int n = 1; n < 10; ++n) {
        int _x1, _x2, _y1, _y2;

        computeCoordinates(tzaar::Board::begin_letter[n - 1],
                           n, _x1, _y1);
        computeCoordinates(tzaar::Board::end_letter[n - 1],
                           n, _x2, _y2);
        if (n == 5) {
            int _x3, _y3, _x4, _y4;

            computeCoordinates('D', 5, _x3, _y3);
            computeCoordinates('F', 5, _x4, _y4);
            mContext->move_to(_x1, _y1);
            mContext->line_to(_x3, _y3);
            mContext->move_to(_x4, _y4);
            mContext->line_to(_x2, _y2);
        } else {
            mContext->move_to(_x1, _y1);
            mContext->line_to(_x2, _y2);
        }
    }

    for (int i = 0; i < 10; ++i) {
        int _x1, _x2, _y1, _y2;

        computeCoordinates(tzaar::Board::begin_diagonal_letter[i],
                           tzaar::Board::begin_diagonal_number[i], _x1, _y1);
        computeCoordinates(tzaar::Board::end_diagonal_letter[i],
                           tzaar::Board::end_diagonal_number[i], _x2, _y2);
        if (tzaar::Board::begin_diagonal_letter[i] == 'A' and
            tzaar::Board::begin_diagonal_number[i] == 1) {
            int _x3, _y3, _x4, _y4;

            computeCoordinates('D', 4, _x3, _y3);
            computeCoordinates('F', 6, _x4, _y4);
            mContext->move_to(_x1, _y1);
            mContext->line_to(_x3, _y3);
            mContext->move_to(_x4, _y4);
            mContext->line_to(_x2, _y2);
        } else {
            mContext->move_to(_x1, _y1);
            mContext->line_to(_x2, _y2);
        }
    }
    mContext->stroke();
}

void DrawingArea::draw_piece(int x, int y, common::Color color, PieceType type,
                             bool selected)
{
    tzaar::Graphics::draw_piece(x, y, mDelta_x, color, type, selected,
                                mContext);
}

void DrawingArea::draw_possible_capture()
{
    const CoordinatesList& list = parent()->get_possible_capture(
        mSelectedPiece);
    CoordinatesList::const_iterator it = list.begin();

    while (it != list.end()) {
        int x, y;

        computeCoordinates(it->letter(), it->number(), x, y);
        mContext->set_source_rgb(0, 0, 255);
        mContext->set_line_width(1.);
        mContext->arc(x, y, 5, 0.0, 2 * M_PI);
        mContext->fill();
        mContext->stroke();
        ++it;
    }
}

void DrawingArea::draw_possible_make_stack()
{
    const CoordinatesList& list = parent()->get_possible_make_stack(
        mSelectedPiece);
    CoordinatesList::const_iterator it = list.begin();

    while (it != list.end()) {
        int x, y;

        computeCoordinates(it->letter(), it->number(), x, y);
        mContext->set_source_rgb(0, 0, 255);
        mContext->set_line_width(1.);
        mContext->arc(x, y, 5, 0.0, 2 * M_PI);
        mContext->fill();
        mContext->stroke();
        ++it;
    }
}

void DrawingArea::draw_state()
{
    const tzaar::Board::Intersections& intersections =
        parent()->intersections();
    tzaar::Board::Intersections::const_iterator it = intersections.begin();

    while (it != intersections.end()) {
        if (it->second.state() == tzaar::NO_VACANT) {
            int _x, _y;
            int n = it->second.height();

            computeCoordinates(it->second.letter(),
                               it->second.number(), _x, _y);
            draw_piece(_x, _y, it->second.color(), it->second.type(), false);
            if (n > 1) {
                char text[3];
                Pango::FontDescription font;

                text[1] = 0;
                text[2] = 0;
                font.set_family("Times");
                font.set_weight(Pango::WEIGHT_BOLD);
                if (n < 10) {
                    text[0] = '0' + n;
                } else {
                    text[0] = '1';
                    text[1] = '0' + (n - 10);
                }

                Glib::RefPtr < Pango::Layout > layout =
                    create_pango_layout(text);

                layout->set_font_description(font);

                int text_width;
                int text_height;

                layout->get_pixel_size(text_width, text_height);

                mContext->set_source_rgb(1., 0., 0.);
                mContext->move_to(_x - text_width / 2,
                                  _y - text_height / 2);

                layout->show_in_cairo_context(mContext);
                mContext->stroke();
            }
        }
        ++it;
    }
    if (mSelectedPiece.is_valid()) {
        draw_piece(mPointerPieceX, mPointerPieceY, mSelectedColor, TOTT, true);
    }
}

bool DrawingArea::on_button(int x, int y)
{
    mSelectedCapture = (x >= mOffset and x <= mOffset + 100 and
                        y >= mOffset and y <= mOffset + 25) ;
    mSelectedMakeStack = (x >= mOffset + 150 and x <= mOffset + 250 and
                          y >= mOffset and y <= mOffset + 25) ;
    mSelectedPass = (x >= mOffset + 300 and x <= mOffset + 400 and
                     y >= mOffset and y <= mOffset + 25) ;
    return mSelectedCapture or mSelectedMakeStack or mSelectedPass;
}

bool DrawingArea::on_button_press_event(GdkEventButton* event)
{
    char letter;
    int number;

    if (parent()->phase() == Gui::FIRST_MOVE or
        parent()->phase() == Gui::CAPTURE or
        parent()->phase() == Gui::MAKE_STRONGER or
        parent()->phase() == Gui::SECOND_CAPTURE) {
        computeLetter(event->x, event->y, letter);
        if (letter != 'X') {
            computeNumber(event->x, event->y, number);
            if (number != -1) {
                tzaar::State state = parent()->intersection_state(letter,
                                                                  number);

                if (state == tzaar::NO_VACANT) {
                    common::Color color = parent()->intersection_color(letter,
                                                                       number);

                    if (parent()->current_color() == color) {
                        mSelectedPiece = tzaar::Coordinates(letter, number);
                        mSelectedColor = color;
                    } else {
                        mSelectedPiece = tzaar::Coordinates('X', 0);
                    }
                } else {
                    mSelectedPiece = tzaar::Coordinates('X', 0);
                }
            }
        }
    }
    return true;
}

bool DrawingArea::on_button_release_event(GdkEventButton* event)
{
    char letter;
    int number;

    computeLetter(event->x, event->y, letter);
    computeNumber(event->x, event->y, number);
    if (letter != 'X' and number != -1 and
        parent()->exist_intersection(letter, number)) {
        if (parent()->phase() == Gui::FIRST_MOVE) {
            if (mSelectedPiece.is_valid() and
                parent()->intersection_state(letter,
                                             number) == tzaar::NO_VACANT and
                parent()->intersection_color(letter, number) !=
                parent()->current_color()) {
                mSelectedCoordinates = tzaar::Coordinates(letter, number);
                parent()->first_move();
            }
            mSelectedPiece = tzaar::Coordinates('X', 0);
        } else if (parent()->phase() == Gui::CAPTURE or
                   parent()->phase() == Gui::SECOND_CAPTURE) {
            if (mSelectedPiece.is_valid() and
                parent()->verify_capture(mSelectedPiece,
                                         Coordinates(letter, number)))
            {
                mSelectedCoordinates = tzaar::Coordinates(letter, number);
                parent()->capture();
            }
            mSelectedPiece = tzaar::Coordinates('X', 0);
        } else if (parent()->phase() == Gui::MAKE_STRONGER) {
            if (mSelectedPiece.is_valid() and
                parent()->intersection_state(letter,
                                             number) == tzaar::NO_VACANT and
                parent()->intersection_color(letter, number) ==
                parent()->current_color()) {
                mSelectedCoordinates = tzaar::Coordinates(letter, number);
                parent()->make_stack();
            }
            mSelectedPiece = tzaar::Coordinates('X', 0);
        }
        queueRedraw();
    }
    if (parent()->phase() == Gui::CHOOSE) {
        if (mSelectedCapture or mSelectedMakeStack or mSelectedPass) {
            parent()->choose();
            mSelectedCapture = false;
            mSelectedMakeStack = false;
            mSelectedPass = false;
            queueRedraw();
        }
    }
    return true;
}

bool DrawingArea::on_motion_notify_event(GdkEventMotion* event)
{
    if (event->state & GDK_BUTTON1_MASK) {
        if ((parent()->phase() == Gui::FIRST_MOVE or
             parent()->phase() == Gui::CAPTURE or
             parent()->phase() == Gui::MAKE_STRONGER or
             parent()->phase() == Gui::SECOND_CAPTURE) and
            mSelectedPiece.is_valid()) {
            mPointerPieceX = event->x;
            mPointerPieceY = event->y;
            computePointer(event->x, event->y);
            queueRedraw();
        }
    } else if (event->state & GDK_BUTTON2_MASK) {
    } else if (event->state & GDK_BUTTON3_MASK) {
    } else {
        if (parent()->phase() != Gui::IDLE and
            computePointer(event->x, event->y)) {
            queueRedraw();
        }  else if (parent()->phase() == Gui::CHOOSE) {
            if (on_button(event->x, event->y)) {
                queueRedraw();
            }
        }
    }
}

}} // namespace openxum tzaar
