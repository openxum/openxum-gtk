/*
 * @file gui/tzaar/drawing-area.hpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _GUI_TZAAR_DRAWING_AREA_HPP
#define _GUI_TZAAR_DRAWING_AREA_HPP 1

#include <gtkmm.h>

#include <gui/gui.hpp>
#include <gui/tzaar/gui.hpp>
#include <tzaar/intersection.hpp>

namespace openxum { namespace tzaar {

class DrawingArea : public openxum::DrawingArea
{
public:
    DrawingArea(openxum::Gui* parent);
    virtual ~DrawingArea()
    { }

    virtual void compute_deltas();
    virtual void draw();

    bool on_button_press_event(GdkEventButton* event);
    bool on_button_release_event(GdkEventButton* event);
    bool on_motion_notify_event(GdkEventMotion* event);

    Coordinates selectedPiece() const
    { return mSelectedPiece; }

    Coordinates selectedCoordinates() const
    { return mSelectedCoordinates; }

    bool selectedCapture() const
    { return mSelectedCapture; }

    bool selectedMakeStack() const
    { return mSelectedMakeStack; }

    bool selectedPass() const
    { return mSelectedPass; }

protected:
    virtual void computeCoordinates(char letter, int number, int& x, int& y);

private:
    void draw_button(int x, int y, const std::string& text, bool select);
    void draw_choice();
    void draw_coordinates();
    void draw_grid();
    void draw_piece(int x, int y, common::Color color, PieceType type,
                    bool selected);
    void draw_possible_capture();
    void draw_possible_make_stack();
    void draw_state();

    bool on_button(int x, int y);

    tzaar::Gui* parent() const
    { return dynamic_cast < tzaar::Gui* >(mParent->specific()); }

    Coordinates mSelectedCoordinates;
    tzaar::Coordinates mSelectedPiece;
    common::Color mSelectedColor;
    int mPointerPieceX;
    int mPointerPieceY;

    bool mSelectedCapture;
    bool mSelectedMakeStack;
    bool mSelectedPass;
};

}} // namespace openxum tzaar

#endif
