/*
 * @file gui/utils/graphics.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <tzaar/graphics.hpp>

using namespace openxum::common;

namespace openxum { namespace tzaar {

void Graphics::draw_piece(int x, int y, int width, common::Color color,
                          PieceType type, bool selected,
                          Cairo::RefPtr < Cairo::Context > context)
{
    if (selected) {
        context->set_source_rgb(0.8, 0.8, 0.8);
        context->set_line_width(width / 5);
        context->arc(x, y, width / 3, 0.0, 2 * M_PI);
        context->stroke();
    } else {
        if (type == TZAAR) {
            Gdk::Color piece_color(color == common::BLACK ? "black" : "white");
            Gdk::Color circle_color(color == common::WHITE ? "black" : "white");

            context->set_source_rgb(piece_color.get_red_p(),
                                     piece_color.get_green_p(),
                                     piece_color.get_blue_p());
            context->arc(x, y, width * (1. / 3 + 1. / 10), 0.0, 2 * M_PI);
            context->fill();

            context->set_source_rgb(0, 0, 0);
            context->set_line_width(1.);
            context->arc(x, y, width * (1. / 3 + 1. / 10), 0.0, 2 * M_PI);
            context->stroke();
            context->set_source_rgb(circle_color.get_red_p(),
                                     circle_color.get_green_p(),
                                     circle_color.get_blue_p());
            context->set_line_width(width * 1. / 10);
            context->arc(x, y, width * 2 * (1. / 3 + 1. / 10) / 3,
                          0.0, 2 * M_PI);
            context->stroke();
            context->arc(x, y, width * (1. / 3 + 1. / 10) / 3,
                          0.0, 2 * M_PI);
            context->fill();
            context->stroke();
        } else if (type == TZARRA) {
            Gdk::Color piece_color(color == common::BLACK ? "black" : "white");
            Gdk::Color circle_color(color == common::WHITE ? "black" : "white");

            context->set_source_rgb(piece_color.get_red_p(),
                                     piece_color.get_green_p(),
                                     piece_color.get_blue_p());
            context->arc(x, y, width * (1. / 3 + 1. / 10), 0.0, 2 * M_PI);
            context->fill();

            context->set_source_rgb(0, 0, 0);
            context->set_line_width(1.);
            context->arc(x, y, width * (1. / 3 + 1. / 10), 0.0, 2 * M_PI);
            context->stroke();
            context->set_source_rgb(circle_color.get_red_p(),
                                     circle_color.get_green_p(),
                                     circle_color.get_blue_p());
            context->arc(x, y, width * 2 * (1. / 3 + 1. / 10) / 3,
                          0.0, 2 * M_PI);
            context->stroke();
            context->arc(x, y, width * (1. / 3 + 1. / 10) / 3,
                          0.0, 2 * M_PI);
            context->fill();
            context->stroke();
        } else { // TOTT
            Gdk::Color piece_color(color == common::BLACK ? "black" : "white");
            Gdk::Color circle_color(color == common::WHITE ? "black" : "white");

            context->set_source_rgb(piece_color.get_red_p(),
                                     piece_color.get_green_p(),
                                     piece_color.get_blue_p());
            context->arc(x, y, width * (1. / 3 + 1. / 10), 0.0, 2 * M_PI);
            context->fill();

            context->set_source_rgb(0, 0, 0);
            context->set_line_width(1.);
            context->arc(x, y, width * (1. / 3 + 1. / 10), 0.0, 2 * M_PI);
            context->stroke();
            context->set_source_rgb(circle_color.get_red_p(),
                                     circle_color.get_green_p(),
                                     circle_color.get_blue_p());
            context->arc(x, y, width * 2 * (1. / 3 + 1. / 10) / 3,
                          0.0, 2 * M_PI);
            context->stroke();
            context->arc(x, y, width * (1. / 3 + 1. / 10) / 3,
                          0.0, 2 * M_PI);
            context->stroke();
        }
    }
}

}} // namespace openxum tzaar
