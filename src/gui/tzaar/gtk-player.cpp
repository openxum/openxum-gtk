/*
 * @file gui/tzaar/gtk-player.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gui/tzaar/gtk-player.hpp>

namespace openxum { namespace tzaar {

Coordinates GtkPlayer::capture(Coordinates& origin)
{
    Coordinates destination = mDrawingArea->selectedCoordinates();

    origin = mDrawingArea->selectedPiece();
    mBoard.first_move(origin, destination);
    return destination;
}

Board::Phase GtkPlayer::choose()
{
    Board::Phase choice;

    if (mDrawingArea->selectedCapture()) {
        choice = Board::SECOND_CAPTURE;
    } else if (mDrawingArea->selectedMakeStack()) {
        choice = Board::MAKE_STRONGER;
    } else if (mDrawingArea->selectedPass()) {
        choice = Board::PASS;
    }
    mBoard.choose(choice);
    return choice;
}

Coordinates GtkPlayer::first_move(Coordinates& origin)
{
    Coordinates destination = mDrawingArea->selectedCoordinates();

    origin = mDrawingArea->selectedPiece();
    mBoard.first_move(origin, destination);
    return destination;
}

Coordinates GtkPlayer::make_stack(Coordinates& origin)
{
    Coordinates destination = mDrawingArea->selectedCoordinates();

    origin = mDrawingArea->selectedPiece();
    mBoard.make_stack(origin, destination);
    return destination;
}

}} // namespace openxum tzaar
