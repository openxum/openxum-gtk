/*
 * @file gui/tzaar/gui.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gui/tzaar/drawing-area.hpp>
#include <gui/tzaar/gui.hpp>
#include <gui/tzaar/gtk-player.hpp>
#include <game/tzaar/players/computer.hpp>

#include <iostream>

using namespace openxum::common;

namespace openxum { namespace tzaar {

Gui::Gui(openxum::Gui* gui) : SpecificGui(gui),
                              mPhase(IDLE),
                              mPlayer(0), mOtherPlayer(0), mJudge(0)
{
    mJudge = new tzaar::Judge(REGULAR);
    // TODO
    mPlayer = new tzaar::GtkPlayer(
        dynamic_cast < tzaar::DrawingArea* >(drawing_area()),
        REGULAR, mJudge->current_color(), mJudge->current_color());
    mOtherPlayer = new tzaar::Computer(REGULAR, mJudge->current_color(),
                                       mJudge->next_color());
}

Gui::~Gui()
{
    delete mJudge;
    delete mPlayer;
    delete mOtherPlayer;
}

void Gui::capture()
{
    Color color = mJudge->current_color();
    Coordinates origin, destination;

    destination = mPlayer->capture(origin);
    if (destination.is_valid() and origin.is_valid()) {

        {
            std::stringstream message;

            message << "[" << (color == BLACK ? "B" : "W")
                    << "] Capture piece from "
                    << origin.letter() << origin.number()
                    << " to " << destination.letter()
                    << destination.number() << std::endl;
            add_turn(message.str());
        }

        mOtherPlayer->capture(origin, destination);
        board().capture(origin, destination);
    } else {
        mOtherPlayer->no_capture();
        board().no_capture();
    }
    redrawState();
    if (board().is_finished()) {
        finish();
    } else if (mPhase == SECOND_CAPTURE) {
        capture_other();
    } else {
        mPhase = CHOOSE;
    }
}

void Gui::capture_other()
{
    Color color = mJudge->current_color();
    Coordinates origin, destination;

    destination = mOtherPlayer->capture(origin);
    if (destination.is_valid() and origin.is_valid()) {

        {
            std::stringstream message;

            message << "[" << (color == BLACK ? "B" : "W")
                    << "] Capture piece from "
                    << origin.letter() << origin.number()
                    << " to " << destination.letter()
                    << destination.number() << std::endl;
            add_turn(message.str());
        }

        mPlayer->capture(origin, destination);
        board().capture(origin, destination);
    } else {
        mPlayer->no_capture();
        board().no_capture();
    }
    redrawState();
    if (board().is_finished()) {
        finish();
    } else if (board().phase() == Board::CHOOSE) {
        choose_other();
    } else {
        mPhase = CAPTURE;
    }
}

void Gui::choose()
{
    Color color = mJudge->current_color();
    Board::Phase choice = mPlayer->choose();

    {
        std::stringstream message;

        message << "[" << (color == BLACK ? "B" : "W")
                << "] Choose to "
                << (choice == Board::SECOND_CAPTURE ? "capture" :
                    (choice == Board::MAKE_STRONGER ? "make a stronger stack" :
                     "pass")) << std::endl;
        add_turn(message.str());
    }

    mOtherPlayer->choose(choice);
    board().choose(choice);

    if (choice == Board::SECOND_CAPTURE) {
        mPhase = SECOND_CAPTURE;
    } else if (choice == Board::MAKE_STRONGER) {
        mPhase = MAKE_STRONGER;
    } else if (choice == Board::PASS) {
        mPhase = PASS;
        pass();
    }
}

void Gui::choose_other()
{
    Color color = mJudge->current_color();
    Board::Phase choice = mOtherPlayer->choose();

    {
        std::stringstream message;

        message << "[" << (color == BLACK ? "B" : "W")
                << "] Choose to "
                << (choice == Board::SECOND_CAPTURE ? "capture" :
                    (choice == Board::MAKE_STRONGER ? "make a stronger stack" :
                     "pass")) << std::endl;
        add_turn(message.str());
    }

    mPlayer->choose(choice);
    board().choose(choice);

    if (choice == Board::SECOND_CAPTURE) {
        capture_other();
    } else if (choice == Board::MAKE_STRONGER) {
        make_stack_other();
    } else if (choice == Board::PASS) {
        pass_other();
    }
}

void Gui::finish()
{
    if (mJudge->winner_is() == WHITE) {
        push_message("White player is winner");
    } else {
        push_message("Black player is winner");
    }
    mPhase = IDLE;
}

void Gui::first_move()
{
    Color color = mJudge->current_color();
    Coordinates origin, destination;

    destination = mPlayer->first_move(origin);

    {
        std::stringstream message;

        message << "[" << (color == BLACK ? "B" : "W")
                << "] Move piece from "
                << origin.letter() << origin.number()
                << " to " << destination.letter()
                << destination.number() << std::endl;
        add_turn(message.str());
    }

    mOtherPlayer->first_move(origin, destination);
    board().first_move(origin, destination);
    capture_other();
}

void Gui::first_move_other()
{
    //TODO
}

void Gui::make_stack()
{
    Color color = mPlayer->color();
    Coordinates origin, destination;

    destination = mPlayer->make_stack(origin);
    if (destination.is_valid() and origin.is_valid()) {

        {
            std::stringstream message;

            message << "[" << (color == BLACK ? "B" : "W")
                    << "] Make stack from "
                    << origin.letter() << origin.number()
                    << " to " << destination.letter() << destination.number()
                    << std::endl;

            add_turn(message.str());
        }

        mOtherPlayer->make_stack(origin, destination);
        board().make_stack(origin, destination);
    } else {
        mOtherPlayer->pass();
        board().pass();
    }
    redrawState();
    if (board().is_finished()) {
        finish();
    } else {
        capture_other();
    }
}

void Gui::make_stack_other()
{
    Color color = mOtherPlayer->color();
    Coordinates origin, destination;

    destination = mOtherPlayer->make_stack(origin);
    if (destination.is_valid() and origin.is_valid()) {

        {
            std::stringstream message;

            message << "[" << (color == BLACK ? "B" : "W")
                    << "] Make stack from "
                    << origin.letter() << origin.number()
                    << " to " << destination.letter() << destination.number()
                    << std::endl;

            add_turn(message.str());
        }

        mPlayer->make_stack(origin, destination);
        board().make_stack(origin, destination);
    } else {
        mPlayer->pass();
        board().pass();
    }
    redrawState();
    mPhase = CAPTURE;
    if (board().is_finished()) {
        finish();
    }
}

void Gui::new_game()
{
    if (mPlayer->color() == mJudge->current_color()) {
        mPhase = FIRST_MOVE;
        push_message("capture black piece");
    } else {
        first_move_other();
    }
}

void Gui::pass()
{
    capture_other();
}

void Gui::pass_other()
{
    mOtherPlayer->pass();
    mPlayer->pass();
    board().pass();
    mPhase = CAPTURE;
}

bool Gui::verify_capture(const Coordinates& origin,
                         const Coordinates& destination) const
{
    return board().verify_capture(origin, destination);
}

}} // namespace openxum tzaar
