/*
 * @file gui/tzaar/gui.hpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _GUI_TZAAR_GUI_HPP
#define _GUI_TZAAR_GUI_HPP 1

#include <gui/gui.hpp>
#include <tzaar/coordinates.hpp>
#include <game/tzaar/judge/judge.hpp>

namespace openxum { namespace tzaar {

class Gui : public SpecificGui
{
public:
    enum phase_t { IDLE, FIRST_MOVE, CAPTURE, CHOOSE, SECOND_CAPTURE,
                   MAKE_STRONGER, PASS };

    Gui(openxum::Gui* gui);
    virtual ~Gui();

    common::Color current_color() const
    { return mJudge->current_color(); }

    virtual bool exist_intersection(char letter, int number)
    { return board().exist_intersection(letter, number); }

    virtual void finish();

    void capture();
    void choose();
    void first_move();

    void get_piece_number(common::Color color, int& tzaarNumber,
                          int& tzarraNumber, int& tottNumber) const
    { board().get_piece_number(color, tzaarNumber, tzarraNumber, tottNumber); }

    CoordinatesList get_possible_capture(const Coordinates& coordinates) const
    { return board().get_possible_capture(coordinates); }

    CoordinatesList get_possible_make_stack(
        const Coordinates& coordinates) const
    { return board().get_possible_make_stack(coordinates); }

    tzaar::State intersection_state(char letter, int number) const
    { return board().intersection_state(letter, number); }

    common::Color intersection_color(char letter, int number) const
    { return board().intersection_color(letter, number); }

    const tzaar::Board::Intersections& intersections() const
    { return board().intersections(); }

    virtual int phase() const
    { return mPhase; }

    void make_stack();

    virtual void new_game();

    void pass();

    bool verify_capture(const Coordinates& origin,
                        const Coordinates& destination) const;

private:
    const tzaar::Board& board() const
    { return mJudge->board(); }

    tzaar::Board& board()
    { return mJudge->board(); }

    void capture_other();
    void choose_other();
    void first_move_other();
    void make_stack_other();
    void pass_other();

    phase_t mPhase;

    tzaar::Player* mPlayer;
    tzaar::Player* mOtherPlayer;
    tzaar::Judge* mJudge;
};

}} // namespace openxum tzaar

#endif
