/*
 * @file gui/tzaar/state.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gui/utils/graphics.hpp>
#include <gui/tzaar/graphics.hpp>
#include <gui/tzaar/gui.hpp>
#include <gui/tzaar/state.hpp>

using namespace openxum::common;

namespace openxum { namespace tzaar {

StateView::StateView(openxum::Gui* parent, Color color) :
    openxum::StateView(parent), mColor(color)
{
}

void StateView::draw()
{
    context()->set_line_width(1.);

    // background
    context()->set_source_rgb(1., 1., 1.);
    context()->rectangle(0, 0, width(), height());
    context()->fill();
    context()->stroke();

    draw_state();
}

void StateView::draw_number(int x, int y, int n)
{
    char text[3];

    text[1] = 0;
    text[2] = 0;
    if (n < 10) {
        text[0] = '0' + n;
    } else {
        text[0] = '1';
        text[1] = '0' + (n - 10);
    }

    Pango::FontDescription font;

    font.set_family("Times");
    font.set_weight(Pango::WEIGHT_BOLD);

    Glib::RefPtr < Pango::Layout > layout =
        create_pango_layout(text);

    layout->set_font_description(font);

    context()->set_source_rgb(0., 0., 0.);
    context()->move_to(x, y);
    layout->show_in_cairo_context(context());
    context()->stroke();
}

void StateView::draw_state()
{
    int x = height() / 3 + 2;
    int y = height() / 2;
    int tzaarNumber, tzarraNumber, tottNumber;

    dynamic_cast < tzaar::Gui* >(parent()->specific())->get_piece_number(
        mColor, tzaarNumber, tzarraNumber, tottNumber);
    tzaar::Graphics::draw_piece(x, y, 2 * height() / 3, mColor, TZAAR, false,
                                context());
    draw_number(x + height() / 3, y, tzaarNumber);
    tzaar::Graphics::draw_piece(x + height(), y, 2 * height() / 3, mColor,
                                TZARRA, false, context());
    draw_number(x + height() + height() / 3, y, tzarraNumber);
    tzaar::Graphics::draw_piece(x + 2 * height(), y, 2 * height() / 3, mColor,
                                TOTT, false, context());
    draw_number(x + 2 * height() + height() / 3, y, tottNumber);
}

}} // namespace openxum tzaar
