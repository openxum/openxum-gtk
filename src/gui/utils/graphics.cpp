/*
 * @file gui/utils/graphics.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gui/utils/graphics.hpp>

using namespace openxum::common;

namespace openxum {

void Graphics::draw_marble(int x, int y, int width, Color color, bool selected,
                           Cairo::RefPtr < Cairo::Context > context)
{
    Cairo::RefPtr < Cairo::RadialGradient > pat =
        Cairo::RadialGradient::create(
            x - width / 2, y - width / 2, width * 0.28 / 3,
            x + width / 2, y + width / 2, width * 0.28 * 2);

    if (color == WHITE){
        pat->add_color_stop_rgba (0, 1, 1, 1, 1);
        pat->add_color_stop_rgba (1, .8, .8, .8, 1);
    } else if (color == GREY){
        pat->add_color_stop_rgba (0, .9, .9, .9, 1);
        pat->add_color_stop_rgba (1, .4, .4, .4, 1);
    } else if (color == BLACK){
        pat->add_color_stop_rgba (0, .3, .3, .3, 1);
        pat->add_color_stop_rgba (1, .0, .0, .0, 1);
    }

    context->move_to(x, y);
    context->set_source(pat);
    context->arc(x, y, width * 0.28, 0.0, 2 * M_PI);
    context->fill();
    context->stroke();

    if (selected) {
        context->set_line_width(5.);
        context->set_source_rgb(1., 1., 0.);
        context->arc(x, y, width * 0.28, 0.0, 2 * M_PI);
        context->stroke();
    }
}

void Graphics::draw_ring(int x, int y, int width, Color color,
                         Cairo::RefPtr < Cairo::Context > context)
{
    if (color == BLACK) {
        Gdk::Color start_color("black");
        Gdk::Color end_color("grey");
        Cairo::RefPtr < Cairo::LinearGradient > gradient =
            Cairo::LinearGradient::create(x - width / 2,
                                          y + width / 2,
                                          x + width, y + width);

        gradient->add_color_stop_rgb(0, start_color.get_red_p(),
                                     start_color.get_green_p(),
                                     start_color.get_blue_p());
        gradient->add_color_stop_rgb(1, end_color.get_red_p(),
                                     end_color.get_green_p(),
                                     end_color.get_blue_p());

        context->set_source_rgb(0, 0, 0);
        context->set_line_width(1.);
        context->arc(x, y, width * (1. / 3 + 1. / 10), 0.0, 2 * M_PI);
        context->stroke();

        context->set_source(gradient);
        context->set_line_width(width / 5);
        context->arc(x, y, width / 3, 0.0, 2 * M_PI);
        context->stroke();
    } else if (color == WHITE) {
        Gdk::Color start_color("grey");
        Gdk::Color end_color("white");
        Cairo::RefPtr < Cairo::LinearGradient > gradient =
            Cairo::LinearGradient::create(x - width / 2,
                                          y + width / 2,
                                          x + width, y + width);

        gradient->add_color_stop_rgb(0, start_color.get_red_p(),
                                     start_color.get_green_p(),
                                     start_color.get_blue_p());
        gradient->add_color_stop_rgb(1, end_color.get_red_p(),
                                     end_color.get_green_p(),
                                     end_color.get_blue_p());

        context->set_source_rgb(0, 0, 0);
        context->set_line_width(1.);
        context->arc(x, y, width * (1. / 3 + 1. / 10), 0.0, 2 * M_PI);
        context->stroke();

        context->arc(x, y, width * (1. / 3 - 1. / 10) - 1,
                      0.0, 2 * M_PI);
        context->stroke();

        context->set_source(gradient);
        context->set_line_width(width / 5);
        context->arc(x, y, width / 3, 0.0, 2 * M_PI);
        context->stroke();
    } else if (color == RED) {
        Gdk::Color start_color("#FF8888");
        Gdk::Color end_color("#FF0000");
        Cairo::RefPtr < Cairo::LinearGradient > gradient =
            Cairo::LinearGradient::create(x - width / 2,
                                          y + width / 2,
                                          x + width, y + width);

        gradient->add_color_stop_rgb(0, start_color.get_red_p(),
                                     start_color.get_green_p(),
                                     start_color.get_blue_p());
        gradient->add_color_stop_rgb(1, end_color.get_red_p(),
                                     end_color.get_green_p(),
                                     end_color.get_blue_p());

        context->set_source_rgb(0, 0, 0);
        context->set_line_width(1.);
        context->arc(x, y, width * (1. / 3 + 1. / 10), 0.0, 2 * M_PI);
        context->stroke();

        context->arc(x, y, width * (1. / 3 - 1. / 10) - 1,
                      0.0, 2 * M_PI);
        context->stroke();

        context->set_source(gradient);
        context->set_line_width(width / 5);
        context->arc(x, y, width / 3, 0.0, 2 * M_PI);
        context->stroke();
    }
}

} // namespace openxum
