/*
 * @file gui/yinsh/drawing-area.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gui/yinsh/drawing-area.hpp>
#include <gui/utils/graphics.hpp>
#include <yinsh/board.hpp>
#include <yinsh/coordinates.hpp>
#include <yinsh/intersection.hpp>

using namespace openxum::common;

namespace openxum { namespace yinsh {

DrawingArea::DrawingArea(openxum::Gui* parent) : openxum::DrawingArea(parent),
                                                 mSelectedRing('X', 0)
{
}

void DrawingArea::computeCoordinates(char letter, int number,
                                     int& x, int& y)
{
    x = mOffset + mDelta_x * (letter - 'A');
    y = mOffset + 7 * mDelta_y + mDelta_xy * (letter - 'A') -
        (number - 1) * mDelta_y;
}

void DrawingArea::compute_deltas()
{
    mDelta_x = (mHeight - 2 * mOffset) / 9.;
    mDelta_y = mDelta_x;
    mDelta_xy = mDelta_y / 2;
    mOffset = mDelta_x / 2;
}

void DrawingArea::draw()
{
    mContext->set_line_width(1.);

    // background
    mContext->set_source_rgb(1., 1., 1.);
    mContext->rectangle(0, 0, mWidth, mHeight);
    mContext->fill();
    mContext->stroke();

    // grid
    draw_grid();
    draw_coordinates();

    //state
    draw_state();

    // show intersection
    show_intersection();

    if (parent()->phase() == Gui::MOVE_RING and mSelectedRing.is_valid()) {
        draw_possible_moving();
    }

    draw_free_markers();
}

void DrawingArea::draw_coordinates()
{
    char text[3];

    text[1] = 0;
    text[2] = 0;

    mContext->set_font_size(16);

    // letters
    for (char l = 'A'; l < 'L'; ++l) {
        int _x, _y;

        text[0] = l;
        computeCoordinates(l, yinsh::Board::begin_number[l - 'A'], _x, _y);
        _x -= 5;
        _y += 20;

        mContext->move_to(_x, _y);
        mContext->show_text(text);
    }

    // numbers
    for (int n = 1; n < 12; ++n) {
        int _x, _y;

        if (n < 10) {
            text[0] = '0' + n;
        } else {
            text[0] = '1';
            text[1] = '0' + (n - 10);
        }
        computeCoordinates(yinsh::Board::begin_letter[n - 1],
                           n, _x, _y);
        _x -= 15 + (n > 9 ? 5 : 0);
        _y -= 3;

        mContext->move_to(_x, _y);
        mContext->show_text(text);
    }
    mContext->stroke();
}

void DrawingArea::draw_free_markers()
{
    char str[3];
    int n = parent()->available_marker_number();

    if (n > 9) {
        str[0] = '0' + (n / 10);
    } else {
        str[0] = ' ';
    }
    str[1] = '0' + (n % 10);
    str[2] = 0;
    mContext->set_source_rgb(0, 0, 0);
    mContext->move_to(mWidth - 50, 50);
    mContext->show_text(str);
    mContext->stroke();
}

void DrawingArea::draw_grid()
{
    if (parent()->phase() == Gui::IDLE) {
        Gdk::Color color("grey");

        mContext->set_source_rgb(color.get_red_p(),
                                 color.get_green_p(),
                                 color.get_blue_p());
    } else {
        mContext->set_source_rgb(0.2, 0.2, 0.2);
    }

    for (char l = 'A'; l < 'L'; ++l) {
        int _x1, _x2, _y1, _y2;

        computeCoordinates(l, yinsh::Board::begin_number[l - 'A'], _x1, _y1);
        computeCoordinates(l, yinsh::Board::end_number[l - 'A'], _x2, _y2);

        mContext->move_to(_x1, _y1);
        mContext->line_to(_x2, _y2);
    }

    for (int n = 1; n < 12; ++n) {
        int _x1, _x2, _y1, _y2;

        computeCoordinates(yinsh::Board::begin_letter[n - 1],
                           n, _x1, _y1);
        computeCoordinates(yinsh::Board::end_letter[n - 1],
                           n, _x2, _y2);

        mContext->move_to(_x1, _y1);
        mContext->line_to(_x2, _y2);
    }

    for (int i = 0; i < 11; ++i) {
        int _x1, _x2, _y1, _y2;

        computeCoordinates(yinsh::Board::begin_diagonal_letter[i],
                           yinsh::Board::begin_diagonal_number[i], _x1, _y1);
        computeCoordinates(yinsh::Board::end_diagonal_letter[i],
                           yinsh::Board::end_diagonal_number[i], _x2, _y2);

        mContext->move_to(_x1, _y1);
        mContext->line_to(_x2, _y2);
    }
    mContext->stroke();
}

void DrawingArea::draw_marker(int x, int y, Color color)
{
    if (color == BLACK) {
        Gdk::Color start_color("black");
        Gdk::Color end_color("grey");
        Cairo::RefPtr < Cairo::LinearGradient > gradient =
            Cairo::LinearGradient::create(x - mDelta_x / 2, y + mDelta_x / 2,
                                          x + mDelta_x, y + mDelta_x);

        gradient->add_color_stop_rgb(0, start_color.get_red_p(),
                                     start_color.get_green_p(),
                                     start_color.get_blue_p());
        gradient->add_color_stop_rgb(1, end_color.get_red_p(),
                                     end_color.get_green_p(),
                                     end_color.get_blue_p());
        mContext->set_source(gradient);

        mContext->set_line_width(1.);
        mContext->arc(x, y, mDelta_x * (1. / 3 - 1. / 10) - 1, 0.0, 2 * M_PI);
        mContext->fill();
        mContext->stroke();
    } else if (color == WHITE) {
        Gdk::Color start_color("grey");
        Gdk::Color end_color("white");
        Cairo::RefPtr < Cairo::LinearGradient > gradient =
            Cairo::LinearGradient::create(x - mDelta_x / 2, y + mDelta_x / 2,
                                          x + mDelta_x, y + mDelta_x);

        gradient->add_color_stop_rgb(0, start_color.get_red_p(),
                                     start_color.get_green_p(),
                                     start_color.get_blue_p());
        gradient->add_color_stop_rgb(1, end_color.get_red_p(),
                                     end_color.get_green_p(),
                                     end_color.get_blue_p());

        mContext->set_source_rgb(0, 0, 0);
        mContext->set_line_width(1.);
        mContext->arc(x, y, mDelta_x * (1. / 3 - 1. / 10) - 1, 0.0, 2 * M_PI);
        mContext->stroke();
        mContext->set_source(gradient);
        mContext->set_line_width(1.);
        mContext->arc(x, y, mDelta_x * (1. / 3 - 1. / 10) - 2, 0.0, 2 * M_PI);
        mContext->fill();
        mContext->stroke();
    }
}

void DrawingArea::draw_possible_moving()
{
    const yinsh::PossibleMovingList& list = parent()->get_possible_moving_list(
        mSelectedRing, mSelectedColor);
    yinsh::PossibleMovingList::const_iterator it = list.begin();

    while (it != list.end()) {
        int x, y;

        computeCoordinates((*it)->letter(), (*it)->number(), x, y);
        mContext->set_source_rgb(0, 0, 255);
        mContext->set_line_width(1.);
        mContext->arc(x, y, 5, 0.0, 2 * M_PI);
        mContext->fill();
        mContext->stroke();
        ++it;
    }
}

void DrawingArea::draw_ring(int x, int y, Color color, bool selected)
{
    if (selected) {
        mContext->set_source_rgb(255, 0, 0);
        mContext->set_line_width(mDelta_x / 5);
        mContext->arc(x, y, mDelta_x / 3, 0.0, 2 * M_PI);
        mContext->stroke();
    } else {
        Graphics::draw_ring(x, y, mDelta_x, color, mContext);
    }
}

void DrawingArea::draw_rows()
{
    if (parent()->phase() == Gui::REMOVE_ROW) {
        const yinsh::SeparatedRows& srows = parent()->get_rows(mSelectedColor);
        yinsh::SeparatedRows::const_iterator its = srows.begin();

        while (its != srows.end()) {
            yinsh::Rows::const_iterator itr = its->begin();

            while (itr != its->end()) {
                const yinsh::Coordinates& begin = itr->front();
                const yinsh::Coordinates& end = itr->back();
                int _x1, _y1, _x2, _y2;
                double alpha_1, beta_1;
                double alpha_2, beta_2;

                computeCoordinates(begin.letter(), begin.number(), _x1, _y1);
                computeCoordinates(end.letter(), end.number(), _x2, _y2);

                if (_x1 == _x2) {
                    if (_y1 < _y2) {
                        alpha_1 = M_PI;
                        beta_1 = 0;
                        alpha_2 = 0;
                        beta_2 = M_PI;
                    } else {
                        alpha_1 = 0;
                        beta_1 = M_PI;
                        alpha_2 = M_PI;
                        beta_2 = 0;
                    }
                } else {
                    double omega_1 = std::acos(1. / std::sqrt(5));

                    if (_x1 < _x2) {
                        if (_y1 < _y2) {
                            alpha_1 = M_PI - omega_1;
                            beta_1 = 3 * M_PI / 2 + omega_1 / 2;
                            alpha_2 = 3 * M_PI / 2 + omega_1 / 2;
                            beta_2 = M_PI - omega_1;
                        } else {
                            alpha_1 = omega_1;
                            beta_1 = M_PI + omega_1;
                            alpha_2 = M_PI + omega_1;
                            beta_2 = omega_1;
                        }
                    }
                }

                std::valarray < double > dashes(2);
                dashes[0] = 2.0;
                dashes[1] = 2.0;

                mContext->set_dash (dashes, 0.0);
                mContext->set_source_rgb(0, 255, 0);
                mContext->set_line_width(4.);
                mContext->arc(_x1, _y1, mDelta_x / 3 + 5, alpha_1, beta_1);
                mContext->line_to(_x2 + (mDelta_x / 3 + 5) * std::cos(alpha_2),
                    _y2 + (mDelta_x / 3 + 5) * std::sin(alpha_2));
                mContext->arc(_x2, _y2, mDelta_x / 3 + 5, alpha_2, beta_2);
                mContext->line_to(_x1 + (mDelta_x / 3 + 5) * std::cos(alpha_1),
                    _y1 + (mDelta_x / 3 + 5) * std::sin(alpha_1));
                mContext->stroke();

                ++itr;
            }
            ++its;
        }

        yinsh::Row::const_iterator it = mSelectedRow.begin();

        while (it != mSelectedRow.end()) {
            int x,y;

            computeCoordinates(it->letter(), it->number(), x, y);
            mContext->set_source_rgb(0, 0, 255);
            mContext->set_line_width(1.);
            mContext->arc(x, y, mDelta_x * (1. / 3 - 1. / 10) - 1,
                          0.0, 2 * M_PI);
            mContext->fill();
            mContext->stroke();
            ++it;
        }
    }
}

void DrawingArea::draw_state()
{
    const yinsh::Board::Intersections& intersections =
        parent()->intersections();
    yinsh::Board::Intersections::const_iterator it = intersections.begin();

    while (it != intersections.end()) {
        int _x, _y;

        computeCoordinates(it->second.letter(), it->second.number(), _x, _y);
        switch (it->second.state()) {
        case yinsh::VACANT: break;
        case yinsh::BLACK_MARKER:
            draw_marker(_x, _y, BLACK);
            break;
        case yinsh::WHITE_MARKER:
            draw_marker(_x, _y, WHITE);
            break;
        case yinsh::BLACK_MARKER_RING:
            draw_marker(_x, _y, BLACK);
        case yinsh::BLACK_RING:
            if (not mSelectedRing.is_valid() or
                (mSelectedRing.is_valid() and it->first != mSelectedRing)) {
                draw_ring(_x, _y, BLACK);
            }
            break;
        case yinsh::WHITE_MARKER_RING:
            draw_marker(_x, _y, WHITE);
        case yinsh::WHITE_RING:
            if (not mSelectedRing.is_valid() or
                (mSelectedRing.is_valid() and it->first != mSelectedRing)) {
                draw_ring(_x, _y, WHITE);
            }
            break;
        }
        ++it;
    }

    if (parent()->phase() == Gui::MOVE_RING and mSelectedRing.is_valid()) {
        draw_ring(mPointerRingX, mPointerRingY, mSelectedColor, true);
    }

    draw_rows();
}

bool DrawingArea::on_button_press_event(GdkEventButton* event)
{
    char letter;
    int number;

    if (parent()->phase() == Gui::MOVE_RING) {
        computeLetter(event->x, event->y, letter);
        if (letter != 'X') {
            computeNumber(event->x, event->y, number);
            if (number != -1) {
                yinsh::State state = parent()->intersection_state(letter,
                                                                 number);

                if (state == yinsh::BLACK_MARKER_RING or
                    state == yinsh::WHITE_MARKER_RING) {
                    mSelectedRing = yinsh::Coordinates(letter, number);
                    mSelectedColor =
                        (state == yinsh::BLACK_MARKER_RING) ? BLACK : WHITE;
                }
            }
        }
    }
    return true;
}

bool DrawingArea::on_button_release_event(GdkEventButton* event)
{
    char letter;
    int number;

    computeLetter(event->x, event->y, letter);
    computeNumber(event->x, event->y, number);
    if (letter != 'X' and number != -1 and
        parent()->exist_intersection(letter, number)) {
        if (parent()->phase() == Gui::PUT_RING and
            parent()->intersection_state(letter, number) == yinsh::VACANT) {
            mSelectedCoordinates = yinsh::Coordinates(letter, number);
            parent()->put_ring();
        } else if (parent()->phase() == Gui::PUT_MARKER and
                   ((parent()->intersection_state(letter,
                                                  number) == yinsh::BLACK_RING
                     and parent()->current_color() == BLACK) or
                    (parent()->intersection_state(letter,
                                                  number) == yinsh::WHITE_RING
                     and parent()->current_color() == WHITE))) {
            mSelectedCoordinates = yinsh::Coordinates(letter, number);
            parent()->put_marker();
        } else if (parent()->phase() == Gui::MOVE_RING) {
            if (mSelectedRing.is_valid()) {
                if (parent()->verify_moving(mSelectedRing,
                                           yinsh::Coordinates(letter,
                                                              number))) {
                    mSelectedCoordinates = yinsh::Coordinates(letter, number);
                    parent()->move_ring();
                }
            }
            mSelectedRing = yinsh::Coordinates('X', 0);
        } else if (parent()->phase() == Gui::REMOVE_ROW) {
            mSelectedCoordinates = yinsh::Coordinates(letter, number);
            parent()->remove_row();
            mSelectedRow.clear();
        } else if (parent()->phase() == Gui::REMOVE_RING and
                   ((parent()->intersection_state(letter,
                                                  number) == yinsh::BLACK_RING
                     and parent()->player_color() == BLACK) or
                    (parent()->intersection_state(letter,
                                                  number) == yinsh::WHITE_RING
                     and parent()->player_color() == WHITE))) {
            mSelectedCoordinates = yinsh::Coordinates(letter, number);
            parent()->remove_ring();
        }
        queueRedraw();
    } else {
        if (parent()->phase() == Gui::MOVE_RING) {
            mSelectedRing = yinsh::Coordinates('X', 0);
            queueRedraw();
        }
    }
    return true;
}

bool DrawingArea::on_motion_notify_event(GdkEventMotion* event)
{
    if (event->state & GDK_BUTTON1_MASK) {
        if (parent()->phase() == Gui::MOVE_RING and
            mSelectedRing.is_valid()) {
            mPointerRingX = event->x;
            mPointerRingY = event->y;
            computePointer(event->x, event->y);
            queueRedraw();
        } else if (parent()->phase() == Gui::REMOVE_ROW) {
            char letter;
            int number;

            computeLetter(event->x, event->y, letter);
            computeNumber(event->x, event->y, number);
            if (letter != 'X' and number != -1 and
                std::find(mSelectedRow.begin(), mSelectedRow.end(),
                          yinsh::Coordinates(letter,
                                             number)) == mSelectedRow.end()) {
                mSelectedRow.push_back(yinsh::Coordinates(letter, number));
            }
            queueRedraw();
        }
    } else if (event->state & GDK_BUTTON2_MASK) {
    } else if (event->state & GDK_BUTTON3_MASK) {
    } else {
        if (parent()->phase() != Gui::IDLE and
            computePointer(event->x, event->y)) {
            queueRedraw();
        }
    }
}

}} // namespace openxum yinsh
