/*
 * @file gui/yinsh/drawing-area.hpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _GUI_YINSH_DRAWING_AREA_HPP
#define _GUI_YINSH_DRAWING_AREA_HPP 1

#include <gtkmm.h>
#include <libglademm/xml.h>

#include <gui/gui.hpp>
#include <gui/yinsh/gui.hpp>
#include <yinsh/intersection.hpp>
#include <yinsh/row.hpp>

namespace openxum { namespace yinsh {

class DrawingArea : public openxum::DrawingArea
{
public:
    DrawingArea(openxum::Gui* parent);
    virtual ~DrawingArea()
    { }

    virtual void compute_deltas();
    virtual void draw();

    bool on_button_press_event(GdkEventButton* event);
    bool on_button_release_event(GdkEventButton* event);
    bool on_motion_notify_event(GdkEventMotion* event);

    Coordinates selectedCoordinates() const
    { return mSelectedCoordinates; }

    yinsh::Row selectedRow() const
    { return mSelectedRow; }

private:
    virtual void computeCoordinates(char letter, int number, int& x, int& y);
    void draw_coordinates();
    void draw_free_markers();
    void draw_grid();
    void draw_marker(int x, int y, common::Color color);
    void draw_possible_moving();
    void draw_ring(int x, int y, common::Color color, bool selected = false);
    void draw_rows();
    void draw_state();

    yinsh::Gui* parent() const
    { return dynamic_cast < yinsh::Gui* >(mParent->specific()); }

    yinsh::Coordinates mSelectedCoordinates;
    yinsh::Coordinates mSelectedRing;
    common::Color mSelectedColor;
    yinsh::Row mSelectedRow;

    int mPointerRingX;
    int mPointerRingY;
};

}} // namespace openxum yinsh

#endif
