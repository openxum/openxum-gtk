/*
 * @file gui/yinsh/gtk-player.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gui/yinsh/gtk-player.hpp>

namespace openxum { namespace yinsh {

Coordinates GtkPlayer::move_ring(const Coordinates& origin)
{
    mBoard.move_ring(origin, mDrawingArea->selectedCoordinates());
    return mDrawingArea->selectedCoordinates();
}

Coordinates GtkPlayer::put_marker()
{
    mBoard.put_marker(mDrawingArea->selectedCoordinates(), mColor);
    return mDrawingArea->selectedCoordinates();
}

Coordinates GtkPlayer::put_ring()
{
    mBoard.put_ring(mDrawingArea->selectedCoordinates(), mColor);
    return mDrawingArea->selectedCoordinates();
}

Coordinates GtkPlayer::remove_ring()
{
    mBoard.remove_ring(mDrawingArea->selectedCoordinates(), mColor);
    return mDrawingArea->selectedCoordinates();
}

bool GtkPlayer::remove_rows(Rows& rows)
{
    const yinsh::SeparatedRows& srows = mBoard.get_rows(mColor);
    yinsh::SeparatedRows::const_iterator its = srows.begin();
    bool found = false;

    while (not found and its != srows.end()) {
        yinsh::Rows::const_iterator itr = its->begin();

        while (not found and itr != its->end()) {
            if (itr->belong(mDrawingArea->selectedCoordinates())) {
                found = true;
                if (itr->size() == 5) {
                    rows.push_back(*itr);
                } else if (mDrawingArea->selectedRow().size() == 5) {
                    rows.push_back(mDrawingArea->selectedRow());
                }
            } else {
                ++itr;
            }
        }
        ++its;
    }
    mBoard.remove_rows(rows, mColor);
}

}} // namespace openxum yinsh
