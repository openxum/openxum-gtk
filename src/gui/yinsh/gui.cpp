/*
 * @file gui/yinsh/gui.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gui/yinsh/drawing-area.hpp>
#include <gui/yinsh/gtk-player.hpp>
#include <gui/yinsh/gui.hpp>

#include <game/yinsh/players/MCTSPlayer.hpp>
#include <game/yinsh/players/clientPlayer.hpp>

#include <glibmm/thread.h>

using namespace openxum::common;

namespace openxum { namespace yinsh {

Gui::Gui(openxum::Gui* gui) : SpecificGui(gui),
                              mPhase(IDLE),
                              mPlayer(0), mOtherPlayer(0), mJudge(0)
{
    mJudge = new yinsh::Judge(Settings::settings().getYinshType(), WHITE);
    mPlayer = new yinsh::GtkPlayer(
        dynamic_cast < yinsh::DrawingArea* >(drawing_area()),
        Settings::settings().getYinshType(), WHITE,
        Settings::settings().getYinshColor());
    if (Settings::settings().getRemotePlayer()) {
        mOtherPlayer = new yinsh::ClientPlayer(
            Settings::settings().getYinshType(), WHITE,
            mPlayer->color() == BLACK ? WHITE : BLACK);
        startRemoteGame();
    } else {
        if (Settings::settings().getYinshLevelNumber() > 0) {
            mOtherPlayer = new yinsh::MCTSPlayer(
                Settings::settings().getYinshType(), WHITE,
                mPlayer->color() == BLACK ? WHITE : BLACK);
        } else {
            mOtherPlayer = new yinsh::RandomPlayer(
                Settings::settings().getYinshType(), WHITE,
                mPlayer->color() == BLACK ? WHITE : BLACK);
        }
    }
}

Gui::~Gui()
{
    delete mJudge;
    delete mPlayer;
    delete mOtherPlayer;
}

void Gui::finish()
{
    if (mJudge->winner_is() == BLACK) {
        push_message("Black is winner");
    } else {
        push_message("White is winner");
    }
    mPhase = IDLE;
}

void Gui::new_game()
{
    if (mPlayer->color() != mJudge->current_color()) {
        yinsh::Coordinates coordinates = mOtherPlayer->put_ring();
        std::stringstream message;

        message << "[" << (mJudge->current_color() == BLACK ? "B" : "W")
                << "] Put ring at " << coordinates.letter()
                << coordinates.number() << std::endl;
        add_turn(message.str());

        mPlayer->put_ring(coordinates, mJudge->current_color());
        board().put_ring(coordinates, mJudge->current_color());
    }
    mPhase = PUT_RING;
    push_message("Put ring");
}

void Gui::move_ring()
{
    Color color = mJudge->current_color();
    Coordinates destination = mPlayer->move_ring(mOrigin);

    {
        std::stringstream message;

        message << "[" << (color == BLACK ? "B" : "W")
                << "] Move ring at " << mOrigin.letter() << mOrigin.number()
                << " to " << destination.letter() << destination.number()
                << std::endl;
        add_turn(message.str());
    }

    mOtherPlayer->move_ring(mOrigin, destination, color);
    board().move_ring(mOrigin, destination);
    if (board().get_rows(color).empty()) {
        mOtherPlayer->remove_no_row(color);
        board().remove_no_row();
        mPlayer->remove_no_row(color);
        mPhase = PUT_MARKER;
        play_other();
        if (mJudge->is_finished()) {
            finish();
        } else {
            if (not board().get_rows(color).empty()) {
                mPhase = REMOVE_ROW;
                push_message("Remove row");
            } else {
                mOtherPlayer->remove_no_row(color);
                board().remove_no_row();
                mPlayer->remove_no_row(color);
                mPhase = PUT_MARKER;
                push_message("Put marker");
            }
        }
    } else {
        mPhase = REMOVE_ROW;
        push_message("Remove row");
    }
    redrawDrawingArea();
}

void Gui::put_marker()
{
    Color color = mJudge->current_color();

    mOrigin = mPlayer->put_marker();
    if (not get_possible_moving_list(mOrigin, color, false).empty()) {
        std::stringstream message;

        message << "[" << (color == BLACK ? "B" : "W")
                << "] Put marker at " << mOrigin.letter()
                << mOrigin.number() << std::endl;
        add_turn(message.str());

        mOtherPlayer->put_marker(mOrigin, color);
        board().put_marker(mOrigin, color);
        mPhase = MOVE_RING;
        push_message("Move ring");
    } else {
        push_message("Invalid intersection: no possible moving");
    }
}

void Gui::put_ring()
{
    Color color = mPlayer->color();
    Coordinates coordinates = mPlayer->put_ring();

    {
        std::stringstream message;

        message << "[" << (mJudge->current_color() == BLACK ? "B" : "W")
                << "] Put ring at " << coordinates.letter()
                << coordinates.number() << std::endl;
        add_turn(message.str());
    }

    mOtherPlayer->put_ring(coordinates, color);
    board().put_ring(coordinates, color);

    if (board().is_initialized()) {
        play_other();
        if (not board().get_rows(mPlayer->color()).empty()) {
            mPhase = REMOVE_ROW;
            push_message("Remove row");
        } else {
            mPhase = PUT_MARKER;
            push_message("Put marker");
        }
    } else {
        Coordinates otherCoordinates = mOtherPlayer->put_ring();

        {
            std::stringstream otherMessage;

            otherMessage << "[" << (mJudge->current_color() == BLACK ?
                                    "B" : "W")
                         << "] Put ring at " << otherCoordinates.letter()
                         << otherCoordinates.number() << std::endl;
            add_turn(otherMessage.str());
        }

        mPlayer->put_ring(otherCoordinates, mJudge->current_color());
        board().put_ring(otherCoordinates, mJudge->current_color());
        if (board().is_initialized()) {
            mPhase = PUT_MARKER;
            push_message("Put marker");
        }
    }
    redrawDrawingArea();
}

void Gui::remove_ring()
{
    Color color = mPlayer->color();
    Coordinates coordinates = mPlayer->remove_ring();

    {
        std::stringstream message;

        message << "["
                << (color == BLACK ? "B" : "W")
                << "] Remove ring at " << coordinates.letter()
                << coordinates.number() << std::endl;
        add_turn(message.str());
    }

    mOtherPlayer->remove_ring(coordinates, color);
    board().remove_ring(coordinates, color);

    if (mJudge->is_finished()) {
        finish();
    } else {
        play_other();
        if (mJudge->is_finished()) {
            finish();
        } else {
            if (not board().get_rows(mPlayer->color()).empty()) {
                mPhase = REMOVE_ROW;
                push_message("Remove row");
            } else {
                mPhase = PUT_MARKER;
                push_message("Put marker");
            }
        }
        redrawState();
    }
}

void Gui::remove_row()
{
    Color color = mPlayer->color();
    const yinsh::SeparatedRows& srows = board().get_rows(color);

    mPlayer->remove_rows(mSelectedRows);
    if (mSelectedRows.size() == srows.size()) {
        yinsh::Rows::const_iterator itr = mSelectedRows.begin();

        while (itr != mSelectedRows.end()) {
            std::stringstream message;

            message << "["
                    << (color == BLACK ? "B" : "W")
                    << "] Remove " << itr->to_string() << std::endl;
            add_turn(message.str());
            ++itr;
        }
        mOtherPlayer->remove_rows(mSelectedRows, color);
        board().remove_rows(mSelectedRows, color);
        mSelectedRows.clear();
        mPhase = REMOVE_RING;
        push_message("Remove ring");
    }
}

void Gui::remove_rows_other()
{
    Color player_color = mJudge->current_color();
    yinsh::Rows rows;

    if (mOtherPlayer->remove_rows(rows)) {
        yinsh::Rows::const_iterator itr = rows.begin();

        while (itr != rows.end()) {
            std::stringstream message;

            message << "[" << (player_color == BLACK ? "B" : "W")
                    << "] Remove " << itr->to_string() << std::endl;
            add_turn(message.str());
            ++itr;
        }

        board().remove_rows(rows, player_color);
        mPlayer->remove_rows(rows, player_color);

        {
            yinsh::Coordinates coordinates = mOtherPlayer->remove_ring();

            std::stringstream message;

            message << "[" << (player_color == BLACK ? "B" : "W")
                    << "] Remove ring at " << coordinates.letter()
                    << coordinates.number() << std::endl;
            add_turn(message.str());

            board().remove_ring(coordinates, player_color);
            mPlayer->remove_ring(coordinates, player_color);
        }

        redrawState();
    }
}

void Gui::play_other()
{
    gui()->get_window()->set_cursor(Gdk::Cursor(Gdk::WATCH));

    redrawDrawingArea();
    gdk_flush();

    Glib::Thread* thread = Glib::Thread::create(
        sigc::mem_fun(*this, &Gui::play_other_thread), true);

    thread->join();

    gui()->get_window()->set_cursor(Gdk::Cursor(Gdk::LEFT_PTR));
}

void Gui::play_other_thread()
{
    yinsh::Coordinates _origin, _destination;
    std::stringstream message;
    Color player_color = mJudge->current_color();

    if(not board().get_rows(player_color).empty()) {
        remove_rows_other();
    } else {
        mOtherPlayer->remove_no_row(player_color);
        board().remove_no_row();
        mPlayer->remove_no_row(player_color);
    }
    _origin = mOtherPlayer->put_marker();
    message << "[" << (player_color == BLACK ? "B" : "W")
            << "] Put marker at " << _origin.letter()
            << _origin.number() << std::endl;

    mPlayer->put_marker(_origin, player_color);
    board().put_marker(_origin, player_color);

    _destination = mOtherPlayer->move_ring(_origin);

    message << "[" << (player_color == BLACK ? "B" : "W")
            << "] Move ring at " << _origin.letter() << _origin.number()
            << " to " << _destination.letter() << _destination.number()
            << std::endl;
    add_turn(message.str());

    mPlayer->move_ring(_origin, _destination, player_color);
    board().move_ring(_origin, _destination);

    if(not board().get_rows(player_color).empty()) {
        remove_rows_other();
    } else {
        mOtherPlayer->remove_no_row(player_color);
        board().remove_no_row();
        mPlayer->remove_no_row(player_color);
    }
}

void Gui::startRemoteGame()
{
    ClientPlayer* player = dynamic_cast < ClientPlayer* >(mOtherPlayer);
    const RemotePlayer::waiting_list& list = player->getWaitingList();

    if (list.empty()) {
    }
}

}} // namespace openxum yinsh
