/*
 * @file gui/yinsh/gui.hpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _GUI_YINSH_GUI_HPP
#define _GUI_YINSH_GUI_HPP 1

#include <gui/gui.hpp>
#include <yinsh/coordinates.hpp>
#include <game/yinsh/judge/judge.hpp>

namespace openxum { namespace yinsh {

class Gui : public SpecificGui
{
public:
    enum phase_t { IDLE, INIT, PUT_MARKER, PUT_RING, MOVE_RING, REMOVE_ROW,
                   REMOVE_RING };

    Gui(openxum::Gui* gui);
    virtual ~Gui();

    unsigned int available_marker_number() const
    { return board().available_marker_number(); }

    common::Color current_color() const
    { return mJudge->current_color(); }

    virtual bool exist_intersection(char letter, int number)
    { return board().exist_intersection(letter, number); }

    virtual void finish();

    virtual int phase() const
    { return mPhase; }

    yinsh::PossibleMovingList get_possible_moving_list(
        const yinsh::Coordinates& origin, common::Color color,
        bool control = true) const
    { return board().get_possible_moving_list(origin, color, control); }

    yinsh::SeparatedRows get_rows(common::Color color) const
    { return board().get_rows(color); }

    yinsh::State intersection_state(char letter, int number) const
    { return board().intersection_state(letter, number); }

    const yinsh::Board::Intersections& intersections() const
    { return board().intersections(); }

    virtual void new_game();

    void move_ring();
    void put_marker();
    void put_ring();
    void remove_ring();
    void remove_row();

    common::Color player_color() const
    { return mPlayer->color(); }

    unsigned int removed_ring_number(common::Color color) const
    { return board().removed_ring_number(color); }

    bool verify_moving(const yinsh::Coordinates& origin,
                       const yinsh::Coordinates& destination) const
    { return board().verify_moving(origin, destination); }

private:
    const yinsh::Board& board() const
    { return mJudge->board(); }

    yinsh::Board& board()
    { return mJudge->board(); }

    void play_other();
    void play_other_thread();
    void remove_rows_other();

    void startRemoteGame();

    phase_t mPhase;
    yinsh::Rows mSelectedRows;
    Coordinates mOrigin;

    yinsh::Player* mPlayer;
    yinsh::Player* mOtherPlayer;
    yinsh::Judge* mJudge;
};

}} // namespace openxum yinsh

#endif
