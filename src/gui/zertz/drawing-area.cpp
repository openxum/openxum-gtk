/*
 * @file gui/zertz/drawing-area.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gui/zertz/drawing-area.hpp>
#include <gui/utils/graphics.hpp>
#include <zertz/board.hpp>
#include <zertz/coordinates.hpp>
#include <zertz/intersection.hpp>

using namespace openxum::common;

namespace openxum { namespace zertz {

DrawingArea::DrawingArea(openxum::Gui* parent) :
    openxum::DrawingArea(parent),mSelectedColor(NONE), mSelectedMarble('X', 0)
{
}

void DrawingArea::computeCoordinates(char letter, int number,
                                     int& x, int& y)
{
    x = mOffset + mDelta_x * (letter - 'A');
    y = mOffset + 9 * mDelta_xy + mDelta_xy * (letter - 'A') -
        (number - 1) * mDelta_y;
    x += mDelta_x / 2;
    y += mDelta_y / 2;
}

void DrawingArea::compute_deltas()
{
    mOffset = 10;
    mDelta_x = (mHeight - 2 * mOffset) / 7.;
    mDelta_y = mDelta_x;
    mDelta_xy = mDelta_y / 2;
}

void DrawingArea::computeLetter(int x, int y, char& letter)
{
    openxum::DrawingArea::computeLetter(x - mDelta_x / 2, y - mDelta_y / 2,
                                        letter);
}

void DrawingArea::computeNumber(int x, int y, int& number)
{
    openxum::DrawingArea::computeNumber(x - mDelta_x / 2, y + mDelta_y / 2,
                                        number);
}

void DrawingArea::draw()
{
    mContext->set_line_width(1.);

    // background
    mContext->set_source_rgb(1., 1., 1.);
    mContext->rectangle(0, 0, mWidth, mHeight);
    mContext->fill();
    mContext->stroke();

    draw_rings();
    draw_pool();
    draw_marbles();
    if (parent()->phase() != Gui::IDLE) {
        show_intersection();
    }

    if (parent()->phase() == Gui::CAPTURE and mSelectedMarble.is_valid()) {
        draw_possible_capturing_marbles();
    }
}

void DrawingArea::draw_marble(int x, int y, Color color, bool selected)
{
    Graphics::draw_marble(x, y, mDelta_x, color, selected, mContext);
}

void DrawingArea::draw_marbles()
{
    for (char l = 'A'; l < 'H'; ++l) {
        for (int n = zertz::Board::begin_number[l - 'A'];
             n <= zertz::Board::end_number[l - 'A']; ++n) {
            zertz::State state = parent()->intersection_state(l, n);

            if (state == zertz::BLACK_MARBLE or
                state == zertz::GREY_MARBLE or
                state == zertz::WHITE_MARBLE) {
                int x;
                int y;

                computeCoordinates(l, n, x, y);
                draw_marble(x, y, parent()->intersection_color(l, n),
                            mSelectedMarble.letter() == l and
                            mSelectedMarble.number() == n);
            }
        }
    }
}

void DrawingArea::draw_pool()
{
    int x = mWidth - 3 * 0.7 * mDelta_x;
    int y = mDelta_y * 0.7;

    for (int i = 0; i < parent()->available_black_marble_number(); ++i) {
        if (mSelectedColor == BLACK and mSelectedMarblePool == i) {
            draw_marble(x, y, BLACK, true);
        } else {
            draw_marble(x, y, BLACK);
        }
        y += mDelta_y * 0.7;
    }
    x += mDelta_x * 0.7;
    y = mDelta_y * 0.7;
    for (int i = 0; i < parent()->available_grey_marble_number(); ++i) {
        if (mSelectedColor == GREY and mSelectedMarblePool == i) {
            draw_marble(x, y, GREY, true);
        } else {
            draw_marble(x, y, GREY);
        }
        y += mDelta_y * 0.7;
    }
    x += mDelta_x * 0.7;
    y = mDelta_y * 0.7;
    for (int i = 0; i < parent()->available_white_marble_number(); ++i) {
        if (mSelectedColor == WHITE and mSelectedMarblePool == i) {
            draw_marble(x, y, WHITE, true);
        } else {
            draw_marble(x, y, WHITE);
        }
        y += mDelta_y * 0.7;
    }
}

void DrawingArea::draw_possible_capturing_marbles()
{
    CoordinatesList list = parent()->get_possible_capturing_marbles(
        mSelectedMarble);
    CoordinatesList::const_iterator it = list.begin();

    while (it != list.end()) {
        int x, y;

        computeCoordinates(it->letter(), it->number(), x, y);
        mContext->set_source_rgb(0, 0, 255);
        mContext->set_line_width(1.);
        mContext->arc(x, y, 5, 0.0, 2 * M_PI);
        mContext->fill();
        mContext->stroke();
        ++it;
    }
}

void DrawingArea::draw_ring(int x, int y, bool highlighting)
{

    Graphics::draw_ring(x, y, mDelta_x, WHITE, mContext);

    if (highlighting) {
        mContext->set_source_rgb(1., 1., 0.);
        mContext->set_line_width(5.);
        mContext->arc(x, y, mDelta_x * (1. / 3 + 1. / 10),
                      0.0, 2 * M_PI);
        mContext->stroke();
    }
}

void DrawingArea::draw_rings()
{
    CoordinatesList list;

    if (parent()->phase() == Gui::REMOVE_RING) {
        list = parent()->get_possible_removing_rings();
    }

    for (char l = 'A'; l < 'H'; ++l) {
        for (int n = zertz::Board::begin_number[l - 'A'];
             n <= zertz::Board::end_number[l - 'A']; ++n) {
            if (parent()->intersection_state(l, n) != zertz::EMPTY) {
                int x;
                int y;

                computeCoordinates(l, n, x, y);
                draw_ring(x, y, std::find(list.begin(), list.end(),
                                          Coordinates(l, n)) != list.end());
            }
        }
    }
}

bool DrawingArea::on_button_press_event(GdkEventButton* event)
{
    char letter;
    int number;

    if (parent()->phase() == Gui::CAPTURE) {
        computeLetter(event->x, event->y, letter);
        if (letter != 'X') {
            computeNumber(event->x, event->y, number);
            if (number != -1) {
            }
        }
    }
    return true;
}

bool DrawingArea::on_button_release_event(GdkEventButton* event)
{
    char letter;
    int number;

    computeLetter(event->x, event->y, letter);
    computeNumber(event->x, event->y, number);
    if (letter != 'X' and number != -1 and
        parent()->exist_intersection(letter, number)) {
        if (parent()->phase() == Gui::PUT_MARBLE and
            parent()->intersection_state(letter, number) == zertz::VACANT) {
            mSelectedCoordinates = Coordinates(letter, number);
            parent()->put_marble();
            mSelectedColor = NONE;
            mSelectedMarblePool = -1;
        } else if (parent()->phase() == Gui::REMOVE_RING) {
            CoordinatesList list = parent()->get_possible_removing_rings();
            Coordinates coordinates(letter, number);

            if (std::find(list.begin(), list.end(),
                          coordinates) != list.end()) {
                mSelectedCoordinates = coordinates;
                parent()->remove_ring();
            }
        } else if (parent()->phase() == Gui::CAPTURE) {
            if (not mSelectedMarble.is_valid()) {
                CoordinatesList list = parent()->get_can_capture_marbles();
                Coordinates coordinates(letter, number);

                if (std::find(list.begin(), list.end(),
                              coordinates) != list.end()) {
                    mSelectedMarble = zertz::Coordinates(letter, number);
                    mSelectedColor = parent()->intersection_color(letter,
                                                                  number);
                }
            } else {
                CoordinatesList list =
                    parent()->get_possible_capturing_marbles(
                        mSelectedMarble);
                Coordinates coordinates(letter, number);

                if (std::find(list.begin(), list.end(),
                              coordinates) != list.end()) {
                    mSelectedCoordinates = coordinates;
                    parent()->capture();
                } else {
                    if (mSelectedMarble == coordinates) {
                        clearSelectedMarble();
                    } else {
                        CoordinatesList list =
                            parent()->get_can_capture_marbles();

                        if (std::find(list.begin(), list.end(),
                                      coordinates) != list.end()) {
                            mSelectedMarble = zertz::Coordinates(letter,
                                                                 number);
                            mSelectedColor =
                                parent()->intersection_color(letter,
                                                             number);
                        }
                    }
                }
            }
        } else if (parent()->phase() == Gui::CAPTURE_ISOLATED_GROUP) {
            Coordinates coordinates(letter, number);
            CoordinatesList list = parent()->get_isolated_marbles();

            if (std::find(list.begin(), list.end(),
                          coordinates) != list.end()) {
                parent()->capture_isolated_group(coordinates);
            }
        }
        queueRedraw();
    } else {
        if (parent()->phase() == Gui::SELECT_MARBLE and
            select_marble_pool(event->x, event->y) != NONE) {
            parent()->select_marble_pool();
            queueRedraw();
        } else if(parent()->phase() == Gui::PUT_MARBLE and
            select_marble_pool(event->x, event->y) != NONE) {
            parent()->select_marble_pool();
            queueRedraw();
        }
    }

    return true;
}

bool DrawingArea::on_motion_notify_event(GdkEventMotion* event)
{
    if (event->state & GDK_BUTTON1_MASK) {
        if (parent()->phase() == Gui::CAPTURE and
            mSelectedMarble.is_valid()) {
            mPointerMarbleX = event->x;
            mPointerMarbleY = event->y;
            computePointer(event->x, event->y);
            queueRedraw();
        }
    } else if (event->state & GDK_BUTTON2_MASK) {
    } else if (event->state & GDK_BUTTON3_MASK) {
    } else {
        if (/*parent()->phase() != Gui::IDLE and */
            computePointer(event->x, event->y)) {
            queueRedraw();
        }
    }
}

bool DrawingArea::select_marble_pool(int x_, int y_)
{
    int x = mWidth - 3 * 0.7 * mDelta_x;
    int y = mDelta_y * 0.7;

    for (int i = 0; i < parent()->available_black_marble_number(); ++i) {
        if (x_ >= x - tolerance and x_ <= x + tolerance and
            y_ >= y - tolerance and y_ <= y + tolerance) {
            mSelectedMarblePool = i;
            mSelectedColor = BLACK;
            return true;
        }
        y += mDelta_y * 0.7;
    }
    x += mDelta_x * 0.7;
    y = mDelta_y * 0.7;
    for (int i = 0; i < parent()->available_grey_marble_number(); ++i) {
        if (x_ >= x - tolerance and x_ <= x + tolerance and
            y_ >= y - tolerance and y_ <= y + tolerance) {
            mSelectedMarblePool = i;
            mSelectedColor = GREY;
            return true;
        }
        y += mDelta_y * 0.7;
    }
    x += mDelta_x * 0.7;
    y = mDelta_y * 0.7;
    for (int i = 0; i < parent()->available_white_marble_number(); ++i) {
        if (x_ >= x - tolerance and x_ <= x + tolerance and
            y_ >= y - tolerance and y_ <= y + tolerance) {
            mSelectedMarblePool = i;
            mSelectedColor = WHITE;
            return true;
        }
        y += mDelta_y * 0.7;
    }
    return false;
}

}} // namespace openxum zertz
