/*
 * @file gui/zertz/drawing-area.hpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _GUI_ZERTZ_DRAWING_AREA_HPP
#define _GUI_ZERTZ_DRAWING_AREA_HPP 1

#include <gtkmm.h>

#include <gui/gui.hpp>
#include <gui/zertz/gui.hpp>
#include <zertz/intersection.hpp>

namespace openxum { namespace zertz {

class DrawingArea : public openxum::DrawingArea
{
public:
    DrawingArea(openxum::Gui* parent);
    virtual ~DrawingArea()
    { }

    void clearSelectedMarble()
    { mSelectedMarble = Coordinates('X', 0); }

    virtual void compute_deltas();
    virtual void draw();

    bool on_button_press_event(GdkEventButton* event);
    bool on_button_release_event(GdkEventButton* event);
    bool on_motion_notify_event(GdkEventMotion* event);

    common::Color selectedColor() const
    { return mSelectedColor; }

    Coordinates selectedCoordinates() const
    { return mSelectedCoordinates; }

    Coordinates selectedMarble() const
    { return mSelectedMarble; }

    void setSelectedMarble(const zertz::Coordinates& coordinates)
    { mSelectedMarble = coordinates; }

private:
    virtual void computeCoordinates(char letter, int number, int& x, int& y);
    virtual void computeLetter(int x, int y, char& letter);
    virtual void computeNumber(int x, int y, int& number);
    void draw_marble(int x, int y, common::Color color, bool selected = false);
    void draw_marbles();
    void draw_pool();
    void draw_possible_capturing_marbles();
    void draw_ring(int x, int y, bool highlighting = false);
    void draw_rings();

    zertz::Gui* parent() const
    { return dynamic_cast < zertz::Gui* >(mParent->specific()); }

    bool select_marble_pool(int x_, int y_);

    common::Color mSelectedColor;
    zertz::Coordinates mSelectedCoordinates;
    zertz::Coordinates mSelectedMarble;
    int mSelectedMarblePool;

    int mPointerMarbleX;
    int mPointerMarbleY;
};

}} // namespace openxum zertz

#endif
