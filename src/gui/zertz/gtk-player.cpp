/*
 * @file gui/zertz/gtk-player.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gui/zertz/gtk-player.hpp>

namespace openxum { namespace zertz {

Coordinates GtkPlayer::put_marble(common::Color& color)
{
    color = mDrawingArea->selectedColor();
    mBoard.put_marble(mDrawingArea->selectedCoordinates(), color, mNumber);
    return mDrawingArea->selectedCoordinates();
}

CoordinatesList GtkPlayer::capture(Coordinates& origin,
                                   Coordinates& destination)
{
    CoordinatesList list;

    list.push_back(mDrawingArea->selectedCoordinates());
    origin = mDrawingArea->selectedMarble();
    destination = mBoard.jump_marble(origin,
                                     mDrawingArea->selectedCoordinates(),
                                     mNumber);
    return list;
}

Coordinates GtkPlayer::remove_ring()
{
    mBoard.remove_ring(mDrawingArea->selectedCoordinates(), mNumber);
    return mDrawingArea->selectedCoordinates();
}

}} // namespace openxum zertz
