/*
 * @file gui/zertz/gtk-player.hpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _GUI_ZERTZ_GTK_PLAYER_HPP
#define _GUI_ZERTZ_GTK_PLAYER_HPP 1

#include <game/zertz/players/localPlayer.hpp>
#include <gui/zertz/drawing-area.hpp>

namespace openxum { namespace zertz {

class GtkPlayer : public LocalPlayer
{
public:
    GtkPlayer(DrawingArea* drawingarea, Type type, Number number,
              Number mynumber) :
        LocalPlayer(type, number, mynumber), mDrawingArea(drawingarea)
    { }

    virtual ~GtkPlayer()
    { }

    virtual Coordinates put_marble(common::Color& color);
    virtual CoordinatesList capture(Coordinates& origin,
                                    Coordinates& destination);
    virtual Coordinates remove_ring();

private:
    DrawingArea* mDrawingArea;
};

}} // namespace openxum zertz

#endif
