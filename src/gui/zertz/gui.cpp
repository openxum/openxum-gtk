/*
 * @file gui/zertz/gui.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gui/zertz/drawing-area.hpp>
#include <gui/zertz/gui.hpp>
#include <gui/zertz/gtk-player.hpp>
#include <game/zertz/players/computer.hpp>

using namespace openxum::common;

namespace openxum { namespace zertz {

Gui::Gui(openxum::Gui* gui) : SpecificGui(gui),
                              mPhase(SELECT_MARBLE),
                              mPlayer(0), mOtherPlayer(0), mJudge(0)
{
    mJudge = new zertz::Judge(Settings::settings().getZertzType(), zertz::ONE);
    mPlayer = new zertz::GtkPlayer(
        dynamic_cast < zertz::DrawingArea* >(drawing_area()),
        Settings::settings().getZertzType(), zertz::ONE,
        Settings::settings().getZertzNumber());
    mOtherPlayer = new zertz::Computer(
        Settings::settings().getZertzType(), zertz::ONE,
        Settings::settings().getZertzNumber() == ONE ? TWO : ONE);
}

Gui::~Gui()
{
    delete mJudge;
    delete mPlayer;
    delete mOtherPlayer;
}

void Gui::capture_isolated_group(const zertz::Coordinates& coordinates)
{
    Number current_player = mJudge->current_player();
    CoordinatesList list = board().get_isolated_marbles();

    mOtherPlayer->capture_marble_and_ring(list, current_player);
    mPlayer->capture_marble_and_ring(list, current_player);
    board().capture_marble_and_ring(list, current_player);
    redrawState();
    if (mJudge->is_finished()) {
        finish();
    } else {
        play_other();
    }
}

void Gui::finish()
{
    if (mJudge->winner_is() == zertz::ONE) {
        push_message("Player One is winner");
    } else {
        push_message("Player Two is winner");
    }
    mPhase = IDLE;
}

void Gui::capture()
{
    Number current_player = mJudge->current_player();
    Coordinates origin;
    Coordinates destination;
    CoordinatesList list = mPlayer->capture(origin, destination);
    Coordinates captured = list[0];

    board().jump_marble(origin, captured, current_player);
    mOtherPlayer->capture(origin, destination, list, current_player);

    std::stringstream message;

    message << "[" << (current_player == ONE ? "1" : "2")
            << "] Jump marble at " << origin.letter()
            << origin.number() << " to " << destination.letter()
            << destination.number() << std::endl;
    message << "[" << (current_player == ONE ? "1" : "2")
            << "] Capture marble at " << captured.letter()
            << captured.number() << std::endl;
    add_turn(message.str());

    redrawState();

    list = board().get_possible_capturing_marbles(destination);
    if (list.empty()) {
        dynamic_cast < zertz::DrawingArea* >(drawing_area())
            ->clearSelectedMarble();
        if (mJudge->is_finished()) {
            finish();
        } else {
            play_other();
        }
    } else {
        std::stringstream message;
        CoordinatesList::const_iterator it = list.begin();

        dynamic_cast < zertz::DrawingArea* >(drawing_area())
            ->setSelectedMarble(destination);
        message << "Select marble to capture with ";
        while (it != list.end()) {
            message << it->letter() << it->number() << " ";
            ++it;
        }
        message << std::endl;
        push_message(message.str());
        if (mJudge->is_finished()) {
            finish();
        } else {
            mPhase = CAPTURE;
        }
    }
}

void Gui::new_game()
{
    if (mPlayer->number() == mJudge->current_player()) {
        mPhase = SELECT_MARBLE;
        push_message("Select marble");
    } else {
        play_other();
    }
}

void Gui::play_other()
{
    Number current_player = mJudge->current_player();
    CoordinatesList list = board().get_can_capture_marbles();

    if (list.empty()) {
        std::stringstream message;
        Color color;
        Coordinates coordinates = mOtherPlayer->put_marble(color);

        board().put_marble(coordinates, color, current_player);
        mPlayer->put_marble(coordinates, color, current_player);

        message << "[" << (current_player == ONE ? "1" : "2")
                << "] Put marble at " << coordinates.letter()
                << coordinates.number() << std::endl;

        coordinates = mOtherPlayer->remove_ring();
        board().remove_ring(coordinates, current_player);
        mPlayer->remove_ring(coordinates, current_player);

        message << "[" << (current_player == ONE ? "1" : "2")
                << "] Remove ring at " << coordinates.letter()
                << coordinates.number() << std::endl;
        add_turn(message.str());

        list = board().get_isolated_marbles();
        if (not list.empty()) {
            mOtherPlayer->capture_marble_and_ring(list, current_player);
            board().capture_marble_and_ring(list, current_player);
            mPlayer->capture_marble_and_ring(list, current_player);
        }
    } else {
        std::stringstream message;
        Coordinates origin;
        Coordinates destination;
        CoordinatesList list = mOtherPlayer->capture(origin, destination);

        board().capture(origin, destination, list, current_player);
        mPlayer->capture(origin, destination, list, current_player);

        message << "[" << (current_player == ONE ? "1" : "2")
                << "] Jump marble at " << origin.letter()
                << origin.number() << " to " << destination.letter()
                << destination.number() << std::endl;

        CoordinatesList::const_iterator it = list.begin();

        message << "[" << (current_player == ONE ? "1" : "2")
                << "] Capture marble";
        if (list.size() > 1) {
            message << "s";
        }
        message << " at ";
        while (it != list.end()) {
            message << it->letter() << it->number() << " ";
            ++it;
        }
        message << std::endl;
        add_turn(message.str());
        redrawState();
    }

    if (mJudge->is_finished()) {
        finish();
    } else {
        CoordinatesList list = board().get_can_capture_marbles();

        if (list.empty()) {
            mPhase = SELECT_MARBLE;
            push_message("Select new marble");
        } else {
            std::stringstream message;
            CoordinatesList::const_iterator it = list.begin();

            message << "Select marble to capture with ";
            while (it != list.end()) {
                message << it->letter() << it->number() << " ";
                ++it;
            }
            mPhase = CAPTURE;
            push_message(message.str());
        }
    }
}

void Gui::put_marble()
{
    Number current_player = mJudge->current_player();
    common::Color color;
    Coordinates coordinates = mPlayer->put_marble(color);
    std::stringstream message;

    mOtherPlayer->put_marble(coordinates, color, current_player);
    board().put_marble(coordinates, color, current_player);

    message << "[" << (current_player == ONE ? "1" : "2")
            << "] Put marble at " << coordinates.letter()
            << coordinates.number() << std::endl;
    add_turn(message.str());

    mPhase = REMOVE_RING;
    push_message("Remove ring");
}

void Gui::remove_ring()
{
    Number current_player = mJudge->current_player();
    Coordinates coordinates = mPlayer->remove_ring();
    std::stringstream message;

    message << "[" << (current_player == ONE ? "1" : "2")
            << "] Remove ring at " << coordinates.letter()
            << coordinates.number() << std::endl;
    add_turn(message.str());

    mOtherPlayer->remove_ring(coordinates, current_player);
    board().remove_ring(coordinates, current_player);

    CoordinatesList list = board().get_isolated_marbles();

    if (not list.empty()) {
        std::stringstream message;
        CoordinatesList::const_iterator it = list.begin();

        message << "Capture marbles in isolated group: ";
        while (it != list.end()) {
            message << it->letter() << it->number() << " ";
            ++it;
        }
        mPhase = CAPTURE_ISOLATED_GROUP;
        push_message(message.str());
    } else {
        play_other();
    }
}

void Gui::select_marble_pool()
{
    mPhase = PUT_MARBLE;
    push_message("Put marble");
}

}} // namespace openxum yinsh
