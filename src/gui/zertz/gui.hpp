/*
 * @file gui/zertz/gui.hpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _GUI_ZERTZ_GUI_HPP
#define _GUI_ZERTZ_GUI_HPP 1

#include <gui/gui.hpp>
#include <zertz/coordinates.hpp>
#include <game/zertz/judge/judge.hpp>

namespace openxum { namespace zertz {

class Gui : public SpecificGui
{
public:
    enum phase_t { IDLE, INIT, SELECT_MARBLE, PUT_MARBLE, REMOVE_RING,
                   CAPTURE, CAPTURE_ISOLATED_GROUP };

    Gui(openxum::Gui* gui);
    virtual ~Gui();

    unsigned int available_black_marble_number() const
    { return board().available_black_marble_number(); }
    unsigned int available_grey_marble_number() const
    { return board().available_grey_marble_number(); }
    unsigned int available_white_marble_number() const
    { return board().available_white_marble_number(); }

    void capture_isolated_group(const zertz::Coordinates& coordinates);

    unsigned int captured_marble_number(common::Color color,
                                        Number number) const
    { board().captured_marble_number(color, number); }

    CoordinatesList get_isolated_marbles() const
    { return board().get_isolated_marbles(); }

    void capture();

    virtual bool exist_intersection(char letter, int number)
    { return board().exist_intersection(letter, number); }

    virtual void finish();

    CoordinatesList get_can_capture_marbles() const
    { return board().get_can_capture_marbles(); }

    CoordinatesList get_possible_capturing_marbles(
        const Coordinates& coordinates) const
    { return board().get_possible_capturing_marbles(coordinates); }

    CoordinatesList get_possible_removing_rings() const
    { return board().get_possible_removing_rings(); }

    common::Color intersection_color(char letter, int number) const
    { return board().intersection_color(letter, number); }

    zertz::State intersection_state(char letter, int number) const
    { return board().intersection_state(letter, number); }

    const zertz::Board::Intersections& intersections() const
    { return board().intersections(); }

    virtual int phase() const
    { return mPhase; }

    void put_marble();
    void remove_ring();
    void select_marble_pool();

    virtual void new_game();

private:
    const zertz::Board& board() const
    { return mJudge->board(); }

    zertz::Board& board()
    { return mJudge->board(); }

    void play_other();

    phase_t mPhase;

    zertz::Player* mPlayer;
    zertz::Player* mOtherPlayer;
    zertz::Judge* mJudge;
};

}} // namespace openxum yinsh

#endif
