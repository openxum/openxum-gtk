/*
 * @file gui/zertz/state.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gui/utils/graphics.hpp>
#include <gui/zertz/gui.hpp>
#include <gui/zertz/state.hpp>

using namespace openxum::common;

namespace openxum { namespace zertz {

StateView::StateView(openxum::Gui* parent, zertz::Number player) :
    openxum::StateView(parent), mPlayer(player)
{
    mDelta_x = height() / 2 - 4;
    mDelta_y = mDelta_x;
}

void StateView::draw()
{
    context()->set_line_width(1.);

    // background
    context()->set_source_rgb(1., 1., 1.);
    context()->rectangle(0, 0, width(), height());
    context()->fill();
    context()->stroke();

    unsigned int k = 0;
    int x = height() / 4 + 2;
    int y = height() / 4;

    for (unsigned int i = 0;
         i < dynamic_cast < zertz::Gui* >(parent()->specific())
             ->captured_marble_number(BLACK, mPlayer); ++i) {
        draw_marble(x, y, BLACK);
        if (++k == 8) {
            x = height() / 4 + 24;
            y += height() / 2 + 2;
        } else {
            x += (height() / 2) + 4;
        }
    }
    for (unsigned int i = 0;
         i < dynamic_cast < zertz::Gui* >(parent()->specific())
             ->captured_marble_number(GREY, mPlayer); ++i) {
        draw_marble(x, y, GREY);
        if (++k == 8) {
            x = height() / 4 + 2;
            y += height() / 2 + 2;
        } else {
            x += (height() / 2) + 4;
        }
    }
    for (unsigned int i = 0;
         i < dynamic_cast < zertz::Gui* >(parent()->specific())
             ->captured_marble_number(WHITE, mPlayer); ++i) {
        draw_marble(x, y, WHITE);
        if (++k == 8) {
            x = height() / 4 + 2;
            y += height() / 2 + 2;
        } else {
            x += (height() / 2) + 4;
        }
    }
}

void StateView::draw_marble(int x, int y, Color color)
{
    Graphics::draw_marble(x, y, mDelta_x * 2, color, false, context());
}

}} // namespace openxum zertz
