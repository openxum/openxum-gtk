/*
 * @file main-gui.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gui/gui.hpp>
#include <version.hpp>

// #include <X11/Xlib.h>

#include <gtkmm/main.h>
#include <sstream>

using namespace openxum;

int main(int argc, char* argv[])
{
    std::stringstream path;

    path << OPENXUM_PREFIX_DIR << "/" << OPENXUM_SHARE_DIRS
         << "/glade/openxum.glade";

    // if (not g_thread_supported()) {
    //     g_thread_init(NULL);
    // }

    // XInitThreads();
    // gdk_threads_init();
    // gdk_threads_enter();

    Gtk::Main application(&argc, &argv);
    Glib::RefPtr < Gnome::Glade::Xml > xml =
        Gnome::Glade::Xml::create(path.str().c_str());
    Gui* g = 0;

    xml->get_widget_derived("MainWindow", g);
    application.run(*g);

    // gdk_threads_leave();

    delete g;
    return 0;
}
