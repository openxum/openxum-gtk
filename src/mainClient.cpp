/*
 * @file mainClient.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <game/yinsh/players/clientPlayer.hpp>
#include <iostream>
#include <stdlib.h>

using namespace openxum::yinsh;

int main(int argc, char* argv[])
{
    ClientPlayer player(REGULAR, "127.0.0.1", atoi(argv[1]));

    std::cerr << "Player is: " << player.color() << std::endl;

    std::cerr << "wait the other player..." << std::endl;
    player.wait_other_player();

    player.run();
    return 0;
}
