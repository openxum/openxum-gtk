/*
 * @file mainServer.cpp
 *
 * This file is part of OpenXum games
 *
 * Copyright (c) 2011-2012 Eric Ramat <eramat@users.sourceforge.net>
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <stdlib.h>
#include <game/yinsh/judge/judge.hpp>
#include <game/yinsh/players/serverPlayer.hpp>

using namespace openxum;
using namespace openxum::common;
using namespace openxum::yinsh;

int main(int argc, char* argv[])
{

    srand(8721);

    Type type = REGULAR;
    Judge judge(type, BLACK);
    ServerPlayer* player_one = new ServerPlayer(type, judge.current_color(),
                                                judge.current_color());
    ServerPlayer* player_two = new ServerPlayer(type, judge.current_color(),
                                                judge.next_color());

    std::cout << "First player is: "
              << (player_one->color() == BLACK ? "black" : "white" )
              << std::endl;
    std::cout << "Second player is: "
              << (player_two->color() == BLACK ? "black" : "white" )
              << std::endl;

    player_one->send_start();
    player_two->send_start();

    judge.init(*player_one, *player_two);

    std::cout << judge.display_board() << std::endl;
    std::cout << "Board is initialized !" << std::endl;

    unsigned int i = 1;

    while (not judge.is_finished()) {

        std::cout << "Turn n°" << i++ << std::endl;

        judge.play(*player_one, *player_two);

        std::cout << judge.display_board() << std::endl;

    }

    std::cout << "Winner is: "
              << (judge.winner_is() == BLACK ? "BLACK" :
                  (judge.winner_is() == WHITE ? "WHITE" : "NONE"))
              << std::endl;

    delete player_one;
    delete player_two;
    return 0;
}
